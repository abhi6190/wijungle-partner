package com.httpcart.wijunglepartner.Models;

public class WijungleModel {
    String id;
    String  modelName;

    public WijungleModel(String id, String modelName){
        this.id = id;
        this.modelName = modelName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }
}
