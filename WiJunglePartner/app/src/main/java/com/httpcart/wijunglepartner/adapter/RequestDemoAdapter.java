package com.httpcart.wijunglepartner.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.httpcart.wijunglepartner.Models.RequestDemoModel;
import com.httpcart.wijunglepartner.R;
import com.httpcart.wijunglepartner.RestInteraction.OnLoadMoreListener;
import com.httpcart.wijunglepartner.activity.ViewCommentScreen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
public class RequestDemoAdapter extends RecyclerView.Adapter<RequestDemoAdapter.MyViewHolder> {

    private List<RequestDemoModel> items;
    Context context;
    public RequestDemoAdapter(Context context, ArrayList<RequestDemoModel> items){
        this.context = context;
        this.items = items;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView compname, personname, mobileno, email, tenmodel, city, time, status;
        public Button btnViewDemoComment;

        public MyViewHolder(View view) {
            super(view);
            compname = (TextView) view.findViewById(R.id.compname);
            personname = (TextView) view.findViewById(R.id.personname);
            mobileno = (TextView) view.findViewById(R.id.mobileno);
            email = (TextView) view.findViewById(R.id.email);
            tenmodel = (TextView) view.findViewById(R.id.tenmodel);
            city = (TextView) view.findViewById(R.id.city);
            time = (TextView) view.findViewById(R.id.time);
            status = (TextView) view.findViewById(R.id.status);
            btnViewDemoComment = view.findViewById(R.id.btnViewDemoComment);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.demo_request_list_items, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        RequestDemoModel requestDemoModel = items.get(position);
        final HashMap<String, String> temp = requestDemoModel.getHashMap();

        holder.compname.setText(temp.get("Company Name"));
        holder.personname.setText(temp.get("Person"));
        holder.mobileno.setText(temp.get("Number"));
        holder.email.setText(temp.get("Email"));
        holder.tenmodel.setText(temp.get("Tentative Model"));
        holder.city.setText(temp.get("City"));
        if(temp.get("Time")!=null && temp.get("Time")!="null") {
            holder.time.setText(temp.get("Time"));
        }else holder.time.setText("");

        if(temp.get("Status").equals("0")) {
            holder.status.setText("Pending");
        }else if(temp.get("Status").equals("1")) {
            holder.status.setText("Done");
        }else if(temp.get("Status").equals("2")) {
            holder.status.setText("Active");
        }

        holder.btnViewDemoComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("demo id : ", temp.get("id"));
                context.startActivity(new Intent(context, ViewCommentScreen.class).putExtra("demoid", temp.get("id")));
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}