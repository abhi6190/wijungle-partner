package com.httpcart.wijunglepartner.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.httpcart.wijunglepartner.Models.TimeSlotModel;
import com.httpcart.wijunglepartner.Models.WijungleModel;
import com.httpcart.wijunglepartner.R;
import com.httpcart.wijunglepartner.Utils.Preferences;
import com.httpcart.wijunglepartner.Utils.Utils;

import java.util.ArrayList;

public class SplashScreen extends AppCompatActivity {
    public static final int SLEEP_TIME = 4;
    IntentLauncher launcher;
    Preferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(4000);
        rotate.setInterpolator(new LinearInterpolator());

        Utils.wijungleModels = new ArrayList<>();
        Utils.wijungleModels.add(new WijungleModel("0", "Select Tetative Model"));
        Utils.wijungleModels.add(new WijungleModel("1", "WiJungle U20"));
        Utils.wijungleModels.add(new WijungleModel("2", "WiJungle U35"));
        Utils.wijungleModels.add(new WijungleModel("3", "WiJungle U50"));
        Utils.wijungleModels.add(new WijungleModel("4", "WiJungle U75"));
        Utils.wijungleModels.add(new WijungleModel("5", "WiJungle U100"));
        Utils.wijungleModels.add(new WijungleModel("6", "WiJungle U150"));
        Utils.wijungleModels.add(new WijungleModel("7", "WiJungle U250"));
        Utils.wijungleModels.add(new WijungleModel("8", "WiJungle U350"));
        Utils.wijungleModels.add(new WijungleModel("9", "WiJungle U500"));
        Utils.wijungleModels.add(new WijungleModel("10", "WiJungle U1000"));
        Utils.wijungleModels.add(new WijungleModel("11", "WiJungle U1500"));
        Utils.wijungleModels.add(new WijungleModel("12", "WiJungle U2500"));



        ImageView image= (ImageView) findViewById(R.id.splashLogo);
        preferences = new Preferences(SplashScreen.this);
        Utils.doLogin(this, preferences.getPartnerUname(), preferences.getPartnerPass());

        launcher = new IntentLauncher();
        launcher.start();
    }
    private class IntentLauncher extends Thread {
        @Override
        /**
         * Sleep for some time and than start new activity.
         */
        public void run() {
            try {
                // Sleeping
                Thread.sleep(SLEEP_TIME * 1000);

                if(preferences.getPartnerId()!="") {
                    startActivity(new Intent(SplashScreen.this, HomeScreen.class));
                }else startActivity(new Intent(SplashScreen.this, LoginActivity.class));
                finish();
            } catch (Exception e) {
                Log.e("error_msg", e.getMessage());
            }
        }
    }
}
