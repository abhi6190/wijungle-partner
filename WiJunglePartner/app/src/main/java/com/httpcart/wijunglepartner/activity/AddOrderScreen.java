package com.httpcart.wijunglepartner.activity;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.httpcart.wijunglepartner.Models.SearchByDemoModel;
import com.httpcart.wijunglepartner.R;
import com.httpcart.wijunglepartner.RestInteraction.RestInteraction;
import com.httpcart.wijunglepartner.RestInteraction.SearchDemoCallback;
import com.httpcart.wijunglepartner.RestInteraction.VolleyCallback;
import com.httpcart.wijunglepartner.Utils.ApiUrl;
import com.httpcart.wijunglepartner.Utils.Utils;
import com.httpcart.wijunglepartner.adapter.AutocompleteAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class AddOrderScreen extends AppCompatActivity implements View.OnClickListener {
    private EditText companyName, companyMob, companyContactPerson, companyShippingAddress, companyCity, companyEmail, gstNo;
    Button addOrder;
    AutoCompleteTextView searchByDemoReq;
    private Context context;
    private String TAG = getClass().getSimpleName();
    private RestInteraction restInteraction;
    private AppCompatSpinner saleType, billEndCustomer;
    private String customerBill="-1";
    private String sale="-1";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_order_screen);
        initViews();

        final AutocompleteAdapter adapter = new AutocompleteAdapter(this, android.R.layout.simple_dropdown_item_1line, new SearchDemoCallback() {
            @Override
            public void onEmptyResponse() {
                AddOrderScreen.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Handle UI here
                        companyName.setText("");
                        companyMob.setText("");
                        companyContactPerson.setText("");
                        companyEmail.setText("");
                    }
                });
            }
        });
        searchByDemoReq.setAdapter(adapter);
        searchByDemoReq.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String demoName = adapter.getItem(position).getNameId();
                searchByDemoReq.setText(demoName);
                sendRequests("type=2&id="+URLEncoder.encode(demoName));
            }
        });


        saleType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(parent.getItemAtPosition(position).toString().equalsIgnoreCase("Customer Enquired About Product")){
                    sale = "customerenquired";
                }else if(parent.getItemAtPosition(position).toString().equalsIgnoreCase("Partner Pitched Product To Customer")){
                    sale = "partnerpitched";
                }else{
                    sale = "-1";
                }
                // Toast.makeText(AddRequestDemoScreen.this, timeSlot, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        billEndCustomer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(parent.getItemAtPosition(position).toString().equalsIgnoreCase("Direct (By WiJungle)")){
                    customerBill = "direct";
                    gstNo.setVisibility(View.VISIBLE);
                }else if(parent.getItemAtPosition(position).toString().equalsIgnoreCase("Self (By Partner)")){
                    customerBill = "self";
                    gstNo.setVisibility(View.GONE);
                }else{
                    customerBill = "-1";
                    gstNo.setVisibility(View.GONE);
                }
                // Toast.makeText(AddRequestDemoScreen.this, timeSlot, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    void initViews(){
        context = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Add Order");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        restInteraction = new RestInteraction(this);
        searchByDemoReq = findViewById(R.id.searchByDemoReq);
        companyName = findViewById(R.id.companyName);
        gstNo = findViewById(R.id.gstNo);
        saleType = findViewById(R.id.saleType);
        billEndCustomer = findViewById(R.id.billEndCustomer);
        companyMob = findViewById(R.id.companyMob);
        companyContactPerson = findViewById(R.id.companyContactPerson);
        companyShippingAddress = findViewById(R.id.companyShippingAddress);
        companyCity = findViewById(R.id.companyCity);
        companyEmail = findViewById(R.id.companyEmail);
        addOrder = findViewById(R.id.addOrder);
        addOrder.setOnClickListener(this);


        List<String> saleTypeList = new ArrayList<String>();
        saleTypeList.add("Select Sale Type");
        saleTypeList.add("Customer Enquired About Product");
        saleTypeList.add("Partner Pitched Product To Customer");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, saleTypeList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        saleType.setAdapter(dataAdapter);

        List<String> billList = new ArrayList<String>();
        billList.add("Who would Bill the End Customer?");
        billList.add("Direct (By WiJungle)");
        billList.add("Self (By Partner)");
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, billList);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        billEndCustomer.setAdapter(dataAdapter1);

    }

    private void sendRequests(String url){
        restInteraction.sendJsonObjectRequests(ApiUrl.apiUrl + url, true, new VolleyCallback() {


            @Override
            public void onSuccessResponse(String result) {
                try {
                    JSONObject response = new JSONObject(result);
                    if(response.optString("status").equalsIgnoreCase("fail")){
                        Utils.alert(context, response.optString("errors"));
                        Log.d(TAG+"Error 2 : ", response.optString("errors"));
                    }else{
                        JSONArray jsonArray = response.optJSONArray("alldata");
                        if(jsonArray != null && jsonArray.length()>0){
                            companyName.setText(jsonArray.optJSONObject(0).getString("orgname"));
                            companyMob.setText(jsonArray.optJSONObject(0).getString("contactnumber"));
                            companyContactPerson.setText(jsonArray.optJSONObject(0).getString("contactperson"));
                            companyEmail.setText(jsonArray.optJSONObject(0).getString("email"));
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(String result) {

            }
        });

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.addOrder:
                /*String error = validation();
                if(error!="") Snackbar.make(coordinatorLayout, error, Snackbar.LENGTH_LONG).show();
                else sendRequests();*/
                break;
        }
    }

}
