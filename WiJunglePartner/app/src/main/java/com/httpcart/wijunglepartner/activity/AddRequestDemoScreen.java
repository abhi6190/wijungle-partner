package com.httpcart.wijunglepartner.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.httpcart.wijunglepartner.AppController;
import com.httpcart.wijunglepartner.Models.CommonModel;
import com.httpcart.wijunglepartner.Models.RequestDemoModel;
import com.httpcart.wijunglepartner.Models.TimeSlotModel;
import com.httpcart.wijunglepartner.R;
import com.httpcart.wijunglepartner.RestInteraction.RestInteraction;
import com.httpcart.wijunglepartner.RestInteraction.VolleyCallback;
import com.httpcart.wijunglepartner.Utils.ApiUrl;
import com.httpcart.wijunglepartner.Utils.Preferences;
import com.httpcart.wijunglepartner.Utils.Utils;
import com.httpcart.wijunglepartner.Utils.Validation;
import com.httpcart.wijunglepartner.adapter.RequestDemoAdapter;
import com.httpcart.wijunglepartner.adapter.TimeSlotAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class AddRequestDemoScreen extends AppCompatActivity implements View.OnClickListener {
    private Spinner models, timeSlots;
    private EditText customerCompanyName, customerPersonName, customerContactName, customerMail, scheduleDate;
    private Button addRequest;
    private String companyName, personName, contactNumber, mailAddress, modelName, demoDate, timeSlot;
    private DatePicker datePicker;
    private Calendar calendar;
    CoordinatorLayout coordinatorLayout;
    private int year, month, day;
    private String tag_json_arry = "json_array_req";
    String TAG = getClass().getSimpleName();
    ProgressDialog pDialog;
    List<String> bookedSlots = new ArrayList<>();
    RestInteraction restInteraction;
    private TimeSlotAdapter timeSlotAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_request_demo_screen);
        initViews();

        models.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                modelName = Utils.wijungleModels.get(position).getId();
                //Toast.makeText(AddRequestDemoScreen.this, modelName, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        timeSlots.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(Utils.timeSlotModels.get(position).getIsEnabled()==0) {
                    timeSlot = Utils.timeSlotModels.get(position).getTime();
                }else{
                    timeSlot = "Select Time Slot";
                }
               // Toast.makeText(AddRequestDemoScreen.this, timeSlot, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        //showDate(year, month+1, day);
    }

    private void initViews(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Add Demo Request");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        restInteraction = new RestInteraction(AddRequestDemoScreen.this);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        addRequest = findViewById(R.id.addRequest);
        customerCompanyName = findViewById(R.id.customerCompanyName);
        customerPersonName = findViewById(R.id.customerPersonName);
        customerContactName = findViewById(R.id.customerContactName);
        customerMail = findViewById(R.id.customerMail);
        scheduleDate = findViewById(R.id.scheduleDate);
        models = findViewById(R.id.models);
        timeSlots = findViewById(R.id.timeSlots);
        Utils.timeSlotModels.clear();
        Utils.timeSlotModels.add(new TimeSlotModel("Select Time Slot", "Select Time Slot", 0));
        makeTimeSpinner();

        List<String> modelList = new ArrayList<String>();
        modelList.add("Select Tetative Model");
        modelList.add("WiJungle U20");
        modelList.add("WiJungle U35");
        modelList.add("WiJungle U50");
        modelList.add("WiJungle U75");
        modelList.add("WiJungle U100");
        modelList.add("WiJungle U150");
        modelList.add("WiJungle U250");
        modelList.add("WiJungle U350");
        modelList.add("WiJungle U500");
        modelList.add("WiJungle U1000");
        modelList.add("WiJungle U1500");
        modelList.add("WiJungle U2500");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, modelList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        models.setAdapter(dataAdapter);



        addRequest.setOnClickListener(this);
        scheduleDate.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.addRequest:
                String error = validation();
                if(error!="") Snackbar.make(coordinatorLayout, error, Snackbar.LENGTH_LONG).show();
                else sendRequests();
             break;
            case R.id.scheduleDate:
                showDialog(999);
             break;
        }
    }

    private void sendRequests(){
        demoDate = Validation.changeDateFormat(demoDate, "dd/mm/yyyy", "yyyy-mm-dd");
        Log.d(TAG+" date : ", demoDate);
        restInteraction.sendJsonObjectRequests(ApiUrl.addDemoRequestUrl + "companyname=" + URLEncoder.encode(companyName) + "&mobileno=" + URLEncoder.encode(contactNumber) +
                "&contactper=" + URLEncoder.encode(personName) + "&model=" + modelName + "&email=" + URLEncoder.encode(mailAddress) +
                "&datetime=" + URLEncoder.encode(demoDate) + "&timeslot=" + URLEncoder.encode(timeSlot), true, new VolleyCallback() {


            @Override
            public void onSuccessResponse(String result) {
                try {
                    JSONObject response = new JSONObject(result);
                    if(response.optString("status").equalsIgnoreCase("fail")){
                        Utils.alert(AddRequestDemoScreen.this, response.optString("errors"));
                        Log.d(TAG+"Error 2 : ", response.optString("errors"));
                    }else {
                        setResult(RESULT_OK);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(String result) {

            }
        });

    }

    public void getBookedSlotes(String date) {

        restInteraction.sendJsonObjectRequests(ApiUrl.apiUrl+"type=3&id="+date, true,
                new VolleyCallback() {


                    @Override
                    public void onSuccessResponse(String result) {
                        Utils.timeSlotModels.clear();
                        Utils.timeSlotModels.add(new TimeSlotModel("Select Time Slot", "Select Time Slot", 0));
                        Utils.timeSlotModels.add(new TimeSlotModel("9:30:00", "9:30-10:30", 0));
                        Utils.timeSlotModels.add(new TimeSlotModel("11:00:00", "11:00-12:00", 0));
                        Utils.timeSlotModels.add(new TimeSlotModel("12:30:00", "12:30-1:30", 0));
                        Utils.timeSlotModels.add(new TimeSlotModel("14:30:00", "2:30-3:30", 0));
                        Utils.timeSlotModels.add(new TimeSlotModel("16:00:00", "4:00-5:30", 0));
                        Utils.timeSlotModels.add(new TimeSlotModel("18:00:00", "6:00-7:00", 0));
                        JSONObject response = null;
                        try {
                            response = new JSONObject(result);
                            if(response.optString("status").equalsIgnoreCase("fail")){

                                Log.d(TAG+"Error 2 : ", response.optString("errors"));
                            }else {
                                JSONArray jsonArray = response.optJSONArray("data");
                                if(jsonArray!=null && jsonArray.length()>0){
                                    ArrayList<TimeSlotModel> timeSlotModels = new ArrayList<>();
                                    for(int i=0;i<Utils.timeSlotModels.size();i++){
                                        for(int j=0;j<jsonArray.length();j++){
                                               if(Utils.timeSlotModels.get(i).getTime().equals(jsonArray.getString(j))){
                                                   TimeSlotModel timeSlotModel = new TimeSlotModel(Utils.timeSlotModels.get(i).getTime(), Utils.timeSlotModels.get(i).getSlot(), 1);
                                                   Utils.timeSlotModels.remove(i);
                                                   Utils.timeSlotModels.add(i, timeSlotModel);
                                               }
                                        }
                                    }
                                }
                            }
                            makeTimeSpinner();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorResponse(String result) {

                    }
                });
    }

    private void makeTimeSpinner(){

        timeSlotAdapter = new TimeSlotAdapter(AddRequestDemoScreen.this, Utils.timeSlotModels);
        timeSlots.setAdapter(timeSlotAdapter);
    }

    private String validation(){
        String error="";
        companyName = customerCompanyName.getText().toString().trim();
        personName = customerPersonName.getText().toString().trim();
        contactNumber = customerContactName.getText().toString().trim();
        mailAddress = customerMail.getText().toString().trim();
        demoDate = scheduleDate.getText().toString().trim();

        if(companyName.equals("")) error = "Please enter Company Name";
        else if(personName.equals("")) error = "Please enter Person Name";
        else if(contactNumber.equals("")) error = "Please enter Contact Number";
        else if(mailAddress.equals("")) error = "Please enter Mail Address";
        else if(modelName.equals("Select Tetative Model")) error = "Please select Model";
        else if(demoDate.equals("")) error = "Please choose Date";
        else if(timeSlot.equals("Select Time Slot")) error = "Please select Time slot";

        if(!Validation.checkAlphaNumeric(companyName)){
            error = "Company Name must be Alpha numeric";
        }else if(!Validation.checkAlphaNumeric(personName)){
            error = "Contact Person Name must be Alpha numeric";
        }else if(!Validation.isValidEmail(mailAddress)){
            error = "Please enter valid email address";
        }

        return error;
    }

    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this,
                    myDateListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day
                    showDate(arg1, arg2+1, arg3);
                }
            };

    private void showDate(int year, int month, int day) {
        String m="";
        if(month<10) m = "0"+month;
        else m = month+"";

        StringBuilder d = new StringBuilder().append(m).append("/")
                .append(day).append("/").append(year);
        scheduleDate.setText(new StringBuilder().append(day).append("/")
                .append(m).append("/").append(year));
        getBookedSlotes(d.toString());
    }



}
