package com.httpcart.wijunglepartner.Models;

public class SearchByDemoModel {
    String nameId;

    public SearchByDemoModel(String nameId) {
        this.nameId = nameId;
    }

    public String getNameId() {
        return nameId;
    }

    public void setNameId(String nameId) {
        this.nameId = nameId;
    }
}
