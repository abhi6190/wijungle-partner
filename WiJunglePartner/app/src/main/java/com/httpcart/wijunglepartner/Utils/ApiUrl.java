package com.httpcart.wijunglepartner.Utils;

/**
 * Created by VARIABLE on 3/31/2017.
 */
public class ApiUrl {
    public static String baseUrl = "https://partner.wijungle.com/";
    public static String getUrl = baseUrl+"tables/get/";
    public static String processUrl = baseUrl+"process/";
    public static String apiUrl = baseUrl+"api/getEditValues.php?";
    public static String loginUrl = baseUrl+"checkLogin.php?";
    public static String getDemoRequestUrl = getUrl+"demoGroupList.php?limit=10";
    public static String getCurrentOrderstUrl = getUrl+"getOrdershowList.php?headerfield=ordergroup&limit=10";
    public static String getPastOrderstUrl = getUrl+"getPastOrdersList.php?headerfield=pastorder&limit=10";
    public static String getDemoCommentsUrl = getUrl+"viewdemocomment.php?pageno=1&limit=10";
    public static String addDemoRequestUrl = processUrl+"requestdemoOperation.php?";
    public static String searchByDemoUrl = baseUrl+"suggestion/requestdemo.php?type=0&";

}
