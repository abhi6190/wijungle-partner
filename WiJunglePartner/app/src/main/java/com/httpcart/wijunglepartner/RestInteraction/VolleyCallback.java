package com.httpcart.wijunglepartner.RestInteraction;

import com.httpcart.wijunglepartner.Models.CommonModel;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public interface VolleyCallback {
    void onSuccessResponse(String result);
    void onErrorResponse(String result);
}
