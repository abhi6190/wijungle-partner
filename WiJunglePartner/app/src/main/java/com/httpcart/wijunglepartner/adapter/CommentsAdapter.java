package com.httpcart.wijunglepartner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.httpcart.wijunglepartner.Models.CommentModel;
import com.httpcart.wijunglepartner.R;
import com.httpcart.wijunglepartner.Utils.Validation;

import java.util.ArrayList;

public class CommentsAdapter extends BaseAdapter {
    Context context;
    ArrayList<CommentModel> items;
    LayoutInflater layoutInflater;

    public CommentsAdapter(Context context, ArrayList<CommentModel> items){
        this.context = context;
        this.items = items;
        layoutInflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.comment_list_items, parent, false);
        TextView tvPostedOn = convertView.findViewById(R.id.tvPostedOn);
        TextView tvComment = convertView.findViewById(R.id.tvComment);
        TextView tvNextTalk = convertView.findViewById(R.id.tvNextTalk);

        tvNextTalk.setText(items.get(position).getNexttalk());
        tvPostedOn.setText(items.get(position).getPostedon());
        tvComment.setText(items.get(position).getComment());

        return convertView;
    }
}
