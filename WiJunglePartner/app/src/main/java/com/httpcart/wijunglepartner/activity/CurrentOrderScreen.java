package com.httpcart.wijunglepartner.activity;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.httpcart.wijunglepartner.Models.CommentModel;
import com.httpcart.wijunglepartner.Models.CommonModel;
import com.httpcart.wijunglepartner.Models.RequestDemoModel;
import com.httpcart.wijunglepartner.R;
import com.httpcart.wijunglepartner.RestInteraction.RestInteraction;
import com.httpcart.wijunglepartner.RestInteraction.VolleyCallback;
import com.httpcart.wijunglepartner.Utils.ApiUrl;
import com.httpcart.wijunglepartner.Utils.ConnectionDetector;
import com.httpcart.wijunglepartner.Utils.Utils;
import com.httpcart.wijunglepartner.adapter.CommonRecyclerAdapter;
import com.httpcart.wijunglepartner.adapter.RequestDemoAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class CurrentOrderScreen extends BaseActivity {
    private CommonRecyclerAdapter mAdapter;
    private String TAG = getClass().getSimpleName();
    private RecyclerView recyclerView;
    private LinearLayout noData;
    ConnectionDetector cd;
    LinkedHashMap<String, String> serverData;
    int pageNo=1;
    Boolean isInternetPresent = false;
    ArrayList<CommonModel> commonModel = new ArrayList<>();
    RestInteraction restInteraction;
    boolean loading=true;
    Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_current_order_screen, contentFrameLayout);
        mContext = CurrentOrderScreen.this;
        restInteraction = new RestInteraction(this);
        noData = findViewById(R.id.noData);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_orders);
        cd = new ConnectionDetector(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        mAdapter = new CommonRecyclerAdapter(mContext, commonModel);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    int visibleItemCount = linearLayoutManager.getChildCount();
                    int totalItemCount = linearLayoutManager.getItemCount();
                    int pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            pageNo++;
                            getAllOrders();
                        }
                    }
                }
            }
        });
        FloatingActionButton fab = findViewById(R.id.addOrderFab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(mContext, AddOrderScreen.class), Utils.REQUEST_CODE);
            }
        });

        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            getAllOrders();
        }else {
            Utils.showNoConnectionDialog(mContext);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.setCheckedItem(R.id.nav_manage);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        Log.d(TAG+" check login data", resultCode+"");
        if(requestCode==Utils.REQUEST_CODE){
            if(resultCode==RESULT_OK){
                pageNo=1;
                getAllOrders();
            }else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void getAllOrders(){

        restInteraction.sendJsonObjectRequests(ApiUrl.getCurrentOrderstUrl+"&pageno="+pageNo, true, new VolleyCallback() {


            @Override
            public void onSuccessResponse(String result) {
                try {
                    JSONObject response = new JSONObject(result);
                    if(response.optString("status").equalsIgnoreCase("fail")){
                        Utils.alert(mContext, response.optString("errors"));
                    }else {
                        JSONArray headers = response.optJSONArray("Header");
                        JSONArray rows = response.optJSONArray("Rows");
                        if(rows.length()>0 || pageNo!=1) {
                            noData.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            try {

                                for(int i=0;i<rows.length();i++){
                                    JSONArray innerRowData = rows.optJSONArray(i);
                                    serverData = new LinkedHashMap<>();
                                    for(int j=0;j<innerRowData.length();j++){
                                        String head = headers.getString(j);
                                        if(headers.getString(j).equals("")){
                                            head = "custom_header"+j;
                                        }
                                        serverData.put(head, innerRowData.getString(j));
                                    }

                                    commonModel.add(new CommonModel(serverData));
                                }




                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if(rows.length()==0){
                                loading=false;
                            }else {
                                mAdapter.notifyDataSetChanged();
                                if(rows.length()>=10) {
                                    loading = true;
                                }else loading = false;
                            }

                        }else {
                            loading=false;
                            noData.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(String result) {
                loading=true;
            }
        });
    }

}
