package com.httpcart.wijunglepartner.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.JsonArray;
import com.httpcart.wijunglepartner.AppController;
import com.httpcart.wijunglepartner.Models.CommonModel;
import com.httpcart.wijunglepartner.Models.RequestDemoModel;
import com.httpcart.wijunglepartner.R;
import com.httpcart.wijunglepartner.RestInteraction.OnLoadMoreListener;
import com.httpcart.wijunglepartner.RestInteraction.RestInteraction;
import com.httpcart.wijunglepartner.RestInteraction.VolleyCallback;
import com.httpcart.wijunglepartner.Utils.ApiUrl;
import com.httpcart.wijunglepartner.Utils.ConnectionDetector;
import com.httpcart.wijunglepartner.Utils.Preferences;
import com.httpcart.wijunglepartner.Utils.Utils;
import com.httpcart.wijunglepartner.adapter.CommentsAdapter;
import com.httpcart.wijunglepartner.adapter.CommonRecyclerAdapter;
import com.httpcart.wijunglepartner.adapter.RequestDemoAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class RequestDemoScreen extends BaseActivity {
    private CommonRecyclerAdapter mAdapter;
    private String TAG = "RequestDemoScreen";
    private RecyclerView recyclerView;
    private LinearLayout noData;
    ConnectionDetector cd;
    LinkedHashMap<String, String> serverData;
    int pageNo=1;
    Boolean isInternetPresent = false;
    ArrayList<CommonModel> commonModels = new ArrayList<>();
    RestInteraction restInteraction;
    boolean loading=true;
    private EditText etSearch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_request_demo_screen, contentFrameLayout);
        etSearch = findViewById(R.id.etSearch);
        restInteraction = new RestInteraction(this);
        noData = findViewById(R.id.noData);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        cd = new ConnectionDetector(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        mAdapter = new CommonRecyclerAdapter(RequestDemoScreen.this, commonModels);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                   int visibleItemCount = linearLayoutManager.getChildCount();
                   int totalItemCount = linearLayoutManager.getItemCount();
                   int pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            pageNo++;
                            getAllRequests("");
                        }
                    }
                }
            }
        });
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startActivity(new Intent(RequestDemoScreen.this, AddRequestDemoScreen.class));
                startActivityForResult(new Intent(RequestDemoScreen.this, AddRequestDemoScreen.class), Utils.REQUEST_CODE);
            }
        });
        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() >= 0){
                    commonModels.clear();
                    pageNo = 1;
                    getAllRequests(s.toString());
                }

            }
        });

        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            getAllRequests("");
        }else {
            Utils.showNoConnectionDialog(RequestDemoScreen.this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.setCheckedItem(R.id.nav_slideshow);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        Log.d(TAG+" check login data", resultCode+"");
        if(requestCode==Utils.REQUEST_CODE){
            if(resultCode==RESULT_OK){
                pageNo=1;
                getAllRequests("");
            }else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void getAllRequests(String search){

        restInteraction.sendJsonObjectRequests(ApiUrl.getDemoRequestUrl+"&pageno="+pageNo+"&ajaxsearchterm="+search, true, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                try {
                    JSONObject response = new JSONObject(result);
                    if(response.optString("status").equalsIgnoreCase("fail")){
                        Utils.alert(RequestDemoScreen.this, response.optString("errors"));
                    }else {
                        JSONArray headers = response.optJSONArray("Header");
                        JSONArray rows = response.optJSONArray("Rows");
                        JSONArray HiddenIndexs = response.optJSONArray("HiddenIndexes");
                        if(rows.length()>0 || pageNo!=1) {
                            noData.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            try {

                                for(int i=0;i<rows.length();i++){
                                    JSONArray innerRowData = rows.optJSONArray(i);
                                    serverData = new LinkedHashMap<>();
                                    for(int j=0;j<innerRowData.length();j++){
                                        if(Utils.checkValueInJsonArray(HiddenIndexs, headers.getString(j))){
                                            serverData.put("hidden_"+headers.getString(j), innerRowData.getString(j));
                                        }else{
                                            serverData.put(headers.getString(j), innerRowData.getString(j));
                                        }

                                    }

                                    commonModels.add(new CommonModel(serverData));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if(rows.length()==0){
                                loading=false;
                            }else {

                                if(rows.length()>=10) {
                                    loading = true;
                                }else loading = false;
                            }

                        }else {
                            loading=false;
                            noData.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        }
                        mAdapter.notifyDataSetChanged();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(String result) {
                loading=true;
            }
        });
    }

}
