package com.httpcart.wijunglepartner.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.httpcart.wijunglepartner.Models.TimeSlotModel;
import com.httpcart.wijunglepartner.R;

import java.util.ArrayList;

public class TimeSlotAdapter extends BaseAdapter {
    Context context;
    ArrayList<TimeSlotModel> items;
    LayoutInflater layoutInflater;
    public TimeSlotAdapter(Context context, ArrayList<TimeSlotModel> items){
        this.context = context;
        this.items = items;
        layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.time_slot_list_items, parent, false);
        TextView timeSlot = (TextView)convertView.findViewById(R.id.timeSlot);
        if(items.get(position).getIsEnabled()==1) {
            timeSlot.setTextColor(Color.parseColor("#bcbcbb"));
        }else {
            timeSlot.setTextColor(Color.parseColor("#000000"));
        }
        timeSlot.setText(items.get(position).getSlot());
        return convertView;
    }
}
