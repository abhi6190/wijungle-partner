package com.httpcart.wijunglepartner.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.httpcart.wijunglepartner.Models.SearchByDemoModel;
import com.httpcart.wijunglepartner.R;
import com.httpcart.wijunglepartner.RestInteraction.RestInteraction;
import com.httpcart.wijunglepartner.RestInteraction.SearchDemoCallback;
import com.httpcart.wijunglepartner.RestInteraction.VolleyCallback;
import com.httpcart.wijunglepartner.Utils.ApiUrl;
import com.httpcart.wijunglepartner.Utils.Utils;
import com.httpcart.wijunglepartner.Utils.Validation;
import com.httpcart.wijunglepartner.activity.AddOrderScreen;
import com.httpcart.wijunglepartner.activity.AddRequestDemoScreen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class AutocompleteAdapter extends ArrayAdapter implements Filterable {
    private ArrayList<SearchByDemoModel> items;
    private String TAG = "";
    private RestInteraction restInteraction;
    private Context context;
    private SearchDemoCallback demoCallback;
    private String COUNTRY_URL =
            "http://demo.hackerkernel.com/jqueryui_autocomplete_dropdown_with_php_and_json/country.php?term=";

    public AutocompleteAdapter(Context context, int resource, SearchDemoCallback demoCallback) {
        super(context, resource);
        this.context = context;
        this.demoCallback = demoCallback;
        items = new ArrayList<>();
        restInteraction = new RestInteraction(context);
        TAG = context.getClass().getSimpleName();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public SearchByDemoModel getItem(int position) {
        return items.get(position);
    }

    @Override
    public Filter getFilter() {
        Filter myFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if(constraint != null){
                    try{
                        //get data from the web
                        String term = constraint.toString();
                        sendRequests(term);
                    }catch (Exception e){
                        Log.d("HUS","EXCEPTION "+e);
                    }
                    filterResults.values = items;
                    filterResults.count = items.size();
                }else {
                    demoCallback.onEmptyResponse();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if(results != null && results.count > 0){
                    notifyDataSetChanged();
                }else{
                    notifyDataSetInvalidated();
                }
            }
        };

        return myFilter;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.auto_complete_layout,parent,false);

        //get Country
        SearchByDemoModel contry = items.get(position);

        TextView countryName = (TextView) view.findViewById(R.id.demoName);

        countryName.setText(contry.getNameId());

        return view;
    }


    private void sendRequests(String search){
        restInteraction.sendJsonObjectRequests(ApiUrl.searchByDemoUrl + "search=" + URLEncoder.encode(search), false, new VolleyCallback() {


            @Override
            public void onSuccessResponse(String result) {
                try {
                    JSONObject response = new JSONObject(result);
                    if(response.optString("status").equalsIgnoreCase("fail")){
                        Utils.alert(context, response.optString("errors"));
                        Log.d(TAG+"Error 2 : ", response.optString("errors"));
                    }else{
                        String dataArray[] = response.optString("data").split(",");
                        items.clear();
                        for (String temp: dataArray){
                            System.out.println(temp);
                            items.add(new SearchByDemoModel(temp));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(String result) {

            }
        });

    }

}
