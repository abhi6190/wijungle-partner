package com.httpcart.wijunglepartner.Models;

public class CommentModel {

    String postedon;
    String comment;
    String nexttalk;
    String postedby;
    String id;

    public CommentModel(String postedon, String comment, String nexttalk, String postedby, String id) {
        this.postedon = postedon;
        this.comment = comment;
        this.nexttalk = nexttalk;
        this.postedby = postedby;
        this.id = id;
    }

    public String getPostedon() {
        return postedon;
    }

    public void setPostedon(String postedon) {
        this.postedon = postedon;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getNexttalk() {
        return nexttalk;
    }

    public void setNexttalk(String nexttalk) {
        this.nexttalk = nexttalk;
    }

    public String getPostedby() {
        return postedby;
    }

    public void setPostedby(String postedby) {
        this.postedby = postedby;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
