package com.httpcart.wijunglepartner.RestInteraction;

import android.app.ProgressDialog;
import android.content.Context;
import android.media.tv.TvContract;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.httpcart.wijunglepartner.AppController;
import com.httpcart.wijunglepartner.Models.CommonModel;
import com.httpcart.wijunglepartner.Utils.ApiUrl;
import com.httpcart.wijunglepartner.Utils.Utils;
import com.httpcart.wijunglepartner.activity.AddRequestDemoScreen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class RestInteraction {
       Context context;
       ProgressDialog pDialog;
       LinkedHashMap<String, String> serverData;
       String tag_json_arry = "json_array_req";
       public RestInteraction(Context context){
           this.context = context;
       }

//    public void sendJsonObjectRequests1(String url, boolean isDialogVisible, final VolleyCallback callback){
//        final String TAG = context.getClass().getSimpleName();
//
//        Log.d(TAG+ " URL :", url);
//        if(pDialog!=null && pDialog.isShowing()){
//            pDialog.dismiss();
//            pDialog = null;
//        }
//        if(isDialogVisible) {
//            pDialog = new ProgressDialog(context);
//            pDialog.setMessage("Loading...");
//            pDialog.show();
//        }
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
//                url, null,
//                new Response.Listener<JSONObject>() {
//
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.d(TAG+" response", response.toString());
//                        if(pDialog!=null) pDialog.hide();
//                        ArrayList<CommonModel> commonModel = new ArrayList<>();
//                        if(response.optString("status").equalsIgnoreCase("fail")){
//                            Utils.alert(context, response.optString("errors"));
//                        }else {
//                            JSONArray headers = response.optJSONArray("Header");
//                            JSONArray rows = response.optJSONArray("Rows");
//                            JSONArray HiddenIndexs = response.optJSONArray("HiddenIndexes");
//                            if(rows.length()>0) {
//                                try {
//
//                                    for(int i=0;i<rows.length();i++){
//                                        JSONArray innerRowData = rows.optJSONArray(i);
//                                        serverData = new LinkedHashMap<>();
//                                        for(int j=0;j<innerRowData.length();j++){
//                                            if(Utils.checkValueInJsonArray(HiddenIndexs, headers.getString(j))){
//                                                serverData.put("hidden_"+headers.getString(j), innerRowData.getString(j));
//                                            }else{
//                                                serverData.put(headers.getString(j), innerRowData.getString(j));
//                                            }
//
//                                        }
//                                        commonModel.add(new CommonModel(serverData));
//
//                                    }
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//
//
//                            }else {
//                                serverData = new LinkedHashMap<>();
//                            }
//
//                        }
//
//                        callback.onSuccessResponse1(commonModel);
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d(TAG, "Error: " + error.getMessage());
//                if(pDialog!=null) pDialog.hide();
//                callback.onErrorResponse(error.getMessage());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> headers = super.getHeaders();
//                Log.d("header size : ",headers.size()+"");
//                if (headers == null || headers.equals(Collections.emptyMap())) {
//                    headers = new HashMap<String, String>();
//                }
//                AppController.getInstance().addSessionCookie(headers);
//                return headers;
//            }
//        };
//        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
//                50000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//
//// Adding request to request queue
//        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_arry);
//    }
//


    public void sendJsonObjectRequests(String url, boolean isDialogVisible, final VolleyCallback callback){
        final String TAG = context.getClass().getSimpleName();
        Log.d(TAG+ " URL :", url);
        if(isDialogVisible) {
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Loading...");
            pDialog.show();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG+" response", response.toString());
                        if(pDialog!=null) pDialog.hide();
                        callback.onSuccessResponse(response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.getMessage());
                if(pDialog!=null) pDialog.hide();
                callback.onErrorResponse(error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = super.getHeaders();
                Log.d("header size : ",headers.size()+"");
                if (headers == null || headers.equals(Collections.emptyMap())) {
                    headers = new HashMap<String, String>();
                }
                AppController.getInstance().addSessionCookie(headers);
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

// Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_arry);
    }
}
