package com.httpcart.wijunglepartner.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.httpcart.wijunglepartner.Models.CommonModel;
import com.httpcart.wijunglepartner.Models.RequestDemoModel;
import com.httpcart.wijunglepartner.R;
import com.httpcart.wijunglepartner.RestInteraction.OnLoadMoreListener;
import com.httpcart.wijunglepartner.Utils.Utils;
import com.httpcart.wijunglepartner.activity.ViewCommentScreen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
public class CommonRecyclerAdapter extends RecyclerView.Adapter<CommonRecyclerAdapter.MyViewHolder> {

    private List<CommonModel> items;
    Context context;
    public CommonRecyclerAdapter(Context context, List<CommonModel> items){
        this.context = context;
        this.items = items;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout llCommon, ll, llButton;
        private TextView textView, textView1;
        private Button button;
        public MyViewHolder(View view) {
            super(view);
            llCommon =  view.findViewById(R.id.llCommon);
            llButton = new LinearLayout(context);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0,5,0,0);
            llButton.setGravity(Gravity.END);
            llButton.setLayoutParams(layoutParams);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.common_list_items, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        CommonModel commonModel = items.get(position);
        final LinkedHashMap<String, String> temp = commonModel.getMap();
        holder.llCommon.removeAllViews();
        String valueColor = "#98a6ad";
        for (Map.Entry<String, String> entry : temp.entrySet()) {
            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
            String val = entry.getValue();
            if(Utils.contains(entry.getKey(), "hidden_")){
                continue;
            }
            if((entry.getKey().equals("") || Utils.contains(entry.getKey(), "custom_")) && val.equals("")){
                continue;
            }
            if(val==null || val.equals("null")){
                val = "-";
            }

            if(Utils.containsHTMLTags(val)){
                if(!Utils.contains(val, "<button")) {
                    if(Utils.contains(val, "<a")){
                        valueColor = "#328cc1";
                    }
                    val = Html.fromHtml(val).toString();
                }else{
                    val = Html.fromHtml(val).toString();
                    LinearLayout.LayoutParams buttonParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    holder.button = new Button(new ContextThemeWrapper(context, R.style.MyButton));
                    holder.button.setLayoutParams(buttonParams);
                    holder.button.setMinWidth(130);
                    holder.button.setText(val);
                    holder.button.setTextColor(Color.parseColor("#ffffff"));
                    holder.llButton.addView(holder.button);
                    continue;
                }
            }
            holder.ll = new LinearLayout(context);
            holder.ll.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0,5,0,0);
            holder.ll.setLayoutParams(layoutParams);
            holder.textView = new TextView(context);
            holder.textView.setTypeface(null, Typeface.BOLD);
            LinearLayout.LayoutParams keyTextParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1.5f);
            LinearLayout.LayoutParams ValueTextParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 2);
            holder.textView.setText(entry.getKey()+" : ");
            holder.textView.setLayoutParams(keyTextParams);
            holder.textView.setTextSize(14f);
            holder.textView.setTextColor(Color.parseColor("#666666"));
            holder.textView1 = new TextView(context);
            holder.textView1.setText(" "+val);
            holder.textView1.setLayoutParams(ValueTextParams);
            holder.textView1.setTextSize(14f);
            holder.textView1.setTextColor(Color.parseColor(valueColor));

            holder.ll.addView(holder.textView);
            holder.ll.addView(holder.textView1);
            holder.llCommon.addView(holder.ll);

        }
        if(valueColor.equals("#328cc1")){
            if(temp.containsKey("id")) {
                holder.textView1.setTag(temp.get("id"));
            }else if(temp.containsKey("hidden_id")){
                holder.textView1.setTag(temp.get("hidden_id"));
            }
        }
        if(holder.llCommon!=null) {
            ((ViewGroup)holder.llCommon).removeView(holder.llButton);
        }
        holder.llCommon.addView(holder.llButton);

        holder.textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(context.getClass().getSimpleName().equalsIgnoreCase("RequestDemoScreen")) {
                    String id = v.getTag().toString();
                    Log.d(context.getClass().getSimpleName(), id);
                    context.startActivity(new Intent(context, ViewCommentScreen.class).putExtra("demoid", id));
                }else{
                    String id = v.getTag().toString();
                    Log.d(context.getClass().getSimpleName(), id);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}