package com.httpcart.wijunglepartner.Models;

public class TimeSlotModel {

    String time;
    String slot;
    int isEnabled;

    public TimeSlotModel(String time, String slot, int isEnabled) {
        this.time = time;
        this.slot = slot;
        this.isEnabled = isEnabled;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSlot() {
        return slot;
    }

    public void setSlot(String slot) {
        this.slot = slot;
    }

    public int getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(int isEnabled) {
        this.isEnabled = isEnabled;
    }
}
