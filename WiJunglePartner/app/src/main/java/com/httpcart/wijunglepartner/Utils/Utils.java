package com.httpcart.wijunglepartner.Utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.httpcart.wijunglepartner.AppController;
import com.httpcart.wijunglepartner.Models.TimeSlotModel;
import com.httpcart.wijunglepartner.Models.WijungleModel;
import com.httpcart.wijunglepartner.R;
import com.httpcart.wijunglepartner.activity.HomeScreen;
import com.httpcart.wijunglepartner.activity.LoginActivity;

import org.json.JSONArray;
import org.jsoup.Jsoup;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

/**
 * Created by VARIABLE on 12/7/2016.
 */
public class Utils {
    public static String message = "";
    public static String mobileNo = "";
    public static final int REQUEST_CODE = 1;
    public static String PARTNER_ID = "PARTNER_ID";
    public static String PARTNER_UNAME = "PARTNER_UNAME";
    public static String PARTNER_PASS = "PARTNER_PASS";
    public static ArrayList<WijungleModel> wijungleModels;
    public static ArrayList<TimeSlotModel> timeSlotModels = new ArrayList<>();
    public static int type ;
    public static int subType ;
    public static String apiResponse="";

    public static void doLogin(final Context context, final String uname, final String password){
            String tag_string_req = "string_req";
            String url = ApiUrl.loginUrl + "employeename=" + URLEncoder.encode(uname)
                    + "&password=" + URLEncoder.encode(password)+ "&medium=1";
            Log.d("url", url);

            StringRequest strReq = new StringRequest(Request.Method.GET,
                    url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Log.d("Utils", response.toString());

                    if(response.toString().equals("&0&/")){
                        Preferences preferences = new Preferences(context);
                        preferences.savePartnerInfo("101", uname, password);
                    }
                }

            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Utils", "Error: " + error.getMessage());
                }
            }){
                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    Log.d("header size : ",response.headers.size()+"");
                    AppController.getInstance().checkSessionCookie(response.headers);
                    return super.parseNetworkResponse(response);
                }

            };


            strReq.setRetryPolicy(new DefaultRetryPolicy(
                    50000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    public static boolean containsHTMLTags(String s){
        String textOfHtmlString = Jsoup.parse(s).text();
        boolean containedHTMLTag = !textOfHtmlString.equals(s);
        return containedHTMLTag;
    }

    public static boolean contains(String s, String indexOf){
        if(s.indexOf(indexOf)>-1){
            return true;
        }else return false;
    }

    public static boolean checkValueInJsonArray(JSONArray jsonArray, String usernameToFind){
        return jsonArray.toString().contains(usernameToFind);
    }

//    private SSLSocketFactory getSocketFactory(Context context) {
//
//        CertificateFactory cf = null;
//        try {
//            cf = CertificateFactory.getInstance("X.509");
//            InputStream caInput = context.getResources().openRawResource(R.raw.server);
//            Certificate ca;
//            try {
//                ca = cf.generateCertificate(caInput);
//                Log.e("CERT", "ca=" + ((X509Certificate) ca).getSubjectDN());
//            } finally {
//                caInput.close();
//            }
//
//
//            String keyStoreType = KeyStore.getDefaultType();
//            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
//            keyStore.load(null, null);
//            keyStore.setCertificateEntry("ca", ca);
//
//
//            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
//            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
//            tmf.init(keyStore);
//
//
//            HostnameVerifier hostnameVerifier = new HostnameVerifier() {
//                @Override
//                public boolean verify(String hostname, SSLSession session) {
//
//                    Log.e("CipherUsed", session.getCipherSuite());
//                    return hostname.compareTo("192.168.1.10")==0; //The Hostname of your server
//
//                }
//            };
//
//
//            HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
//            SSLContext context = null;
//            context = SSLContext.getInstance("TLS");
//
//            context.init(null, tmf.getTrustManagers(), null);
//            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
//
//            SSLSocketFactory sf = context.getSocketFactory();
//
//
//            return sf;
//
//        } catch (CertificateException e) {
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (KeyStoreException e) {
//            e.printStackTrace();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (KeyManagementException e) {
//            e.printStackTrace();
//        }
//
//        return  null;
//    }


    public static void showNoConnectionDialog(Context ctx1)
    {
        final Context ctx = ctx1;
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setCancelable(true);
        builder.setMessage("Please make sure your internet is properly working.");
        builder.setTitle("No Internet Connection");
        builder.setIcon(R.mipmap.fail);
        builder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which)
            {
                ctx.startActivity(new Intent(Settings.ACTION_SETTINGS));
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                return;
            }
        });

        builder.setOnCancelListener(new DialogInterface.OnCancelListener()
        {
            public void onCancel(DialogInterface dialog) {
                return;
            }
        });
        builder.show();

//        changeTextLineColor(builder);
    }

    public static void alert(Context context, String message){
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setNeutralButton(context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })

                .show();
    }

    public static void changeTextLineColor(AlertDialog.Builder builder){
        Dialog d = builder.show();
        int dividerId = d.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
        View divider = d.findViewById(dividerId);
        divider.setBackgroundColor(Color.parseColor("#00a284"));
        int textViewId = d.getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
        TextView tv = (TextView) d.findViewById(textViewId);
        tv.setTextColor(Color.parseColor("#00a284"));
    }

}
