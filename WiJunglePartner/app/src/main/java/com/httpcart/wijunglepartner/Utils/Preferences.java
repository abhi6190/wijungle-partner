package com.httpcart.wijunglepartner.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preferences {
    SharedPreferences sharedPreferences;
    Context context;

    public Preferences(Context context){
        this.context = context;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void savePartnerInfo(String partnerId, String uname, String pass){
        sharedPreferences.edit().putString(Utils.PARTNER_ID, partnerId).commit();
        sharedPreferences.edit().putString(Utils.PARTNER_UNAME, uname).commit();
        sharedPreferences.edit().putString(Utils.PARTNER_PASS, pass).commit();
    }

    public String getPartnerId(){
        return sharedPreferences.getString(Utils.PARTNER_ID, "");
    }
    public String getPartnerUname(){
        return sharedPreferences.getString(Utils.PARTNER_UNAME, "");
    }
    public String getPartnerPass(){
        return sharedPreferences.getString(Utils.PARTNER_PASS, "");
    }

}
