package com.httpcart.wijunglepartner.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.httpcart.wijunglepartner.Models.CommentModel;
import com.httpcart.wijunglepartner.Models.CommonModel;
import com.httpcart.wijunglepartner.Models.RequestDemoModel;
import com.httpcart.wijunglepartner.R;
import com.httpcart.wijunglepartner.RestInteraction.RestInteraction;
import com.httpcart.wijunglepartner.RestInteraction.VolleyCallback;
import com.httpcart.wijunglepartner.Utils.ApiUrl;
import com.httpcart.wijunglepartner.Utils.Utils;
import com.httpcart.wijunglepartner.adapter.CommentsAdapter;
import com.httpcart.wijunglepartner.adapter.RequestDemoAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class ViewCommentScreen extends AppCompatActivity {
    private ListView commentList;
    private String demoId="0";
    private RestInteraction restInteraction;
    LinearLayout noData;
    private CommentsAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_comment_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Comment Screen");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        restInteraction = new RestInteraction(this);
        commentList = findViewById(R.id.commentList);
        noData = findViewById(R.id.noData);
        demoId = getIntent().getStringExtra("demoid");
        getAllComments(demoId);
    }

    private void getAllComments(String demoId){

        restInteraction.sendJsonObjectRequests(ApiUrl.getDemoCommentsUrl+"&id="+demoId, true, new VolleyCallback() {

            @Override
            public void onSuccessResponse(String result) {
                try {
                    JSONObject response = new JSONObject(result);
                    if(response.optString("status").equalsIgnoreCase("fail")){
                        Utils.alert(ViewCommentScreen.this, response.optString("errors"));
                    }else {
                        JSONArray headers = response.optJSONArray("Header");
                        JSONArray rows = response.optJSONArray("Rows");
                        if(rows.length()>0) {
                            noData.setVisibility(View.GONE);
                            commentList.setVisibility(View.VISIBLE);
                            try {
                                ArrayList<CommentModel> commentModels = new ArrayList<>();
                                for(int i=0;i<rows.length();i++){
                                    JSONArray innerRowData = rows.optJSONArray(i);
                                    commentModels.add(new CommentModel(innerRowData.getString(0), innerRowData.getString(1),
                                                innerRowData.getString(2), innerRowData.getString(3), innerRowData.getString(4)));


                                }
                                mAdapter = new CommentsAdapter(ViewCommentScreen.this, commentModels);
                                commentList.setAdapter(mAdapter);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else {
                            noData.setVisibility(View.VISIBLE);
                            commentList.setVisibility(View.GONE);
                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(String result) {

            }
        });
    }
}
