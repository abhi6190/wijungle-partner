package com.httpcart.wijunglepartner.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.httpcart.wijunglepartner.Models.CommonModel;
import com.httpcart.wijunglepartner.R;
import com.httpcart.wijunglepartner.RestInteraction.RestInteraction;
import com.httpcart.wijunglepartner.RestInteraction.VolleyCallback;
import com.httpcart.wijunglepartner.Utils.ApiUrl;
import com.httpcart.wijunglepartner.Utils.ConnectionDetector;
import com.httpcart.wijunglepartner.Utils.Utils;
import com.httpcart.wijunglepartner.adapter.CommonRecyclerAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class PastOrderScreen extends BaseActivity {

    private CommonRecyclerAdapter mAdapter;
    private String TAG = getClass().getSimpleName();
    private RecyclerView recyclerView;
    private LinearLayout noData;
    ConnectionDetector cd;
    LinkedHashMap<String, String> serverData;
    int pageNo=1;
    Boolean isInternetPresent = false;
    List<CommonModel> commonModel = new ArrayList<>();
    RestInteraction restInteraction;
    boolean loading=true;
    Context mContext;
    EditText etSearchPast;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_past_order_screen, contentFrameLayout);
        mContext = PastOrderScreen.this;
        restInteraction = new RestInteraction(this);
        noData = findViewById(R.id.noData);
        etSearchPast = findViewById(R.id.etSearchPast);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_past_order);
        cd = new ConnectionDetector(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        mAdapter = new CommonRecyclerAdapter(mContext, commonModel);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    int visibleItemCount = linearLayoutManager.getChildCount();
                    int totalItemCount = linearLayoutManager.getItemCount();
                    int pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            pageNo++;
                            getAllPastOrders("");
                        }
                    }
                }
            }
        });

        etSearchPast.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() >= 0){
                    commonModel.clear();
                    pageNo = 1;
                    getAllPastOrders(s.toString());
                }
            }
        });

        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            getAllPastOrders("");
        }else {
            Utils.showNoConnectionDialog(mContext);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.setCheckedItem(R.id.past_order);
    }


    private void getAllPastOrders(String search){

        restInteraction.sendJsonObjectRequests(ApiUrl.getPastOrderstUrl+"&pageno="+pageNo+"&ajaxsearchterm="+search, true, new VolleyCallback() {

            @Override
            public void onSuccessResponse(String result) {
                try {
                    JSONObject response = new JSONObject(result);
                    if(response.optString("status").equalsIgnoreCase("fail")){
                        Utils.alert(mContext, response.optString("errors"));
                    }else {
                        JSONArray headers = response.optJSONArray("Header");
                        JSONArray rows = response.optJSONArray("Rows");
                        JSONArray HiddenIndexs = response.optJSONArray("HiddenIndexes");
                        if(rows.length()>0 || pageNo!=1) {
                            noData.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            try {

                                for(int i=0;i<rows.length();i++){
                                    JSONArray innerRowData = rows.optJSONArray(i);
                                    serverData = new LinkedHashMap<>();
                                    for(int j=0;j<innerRowData.length();j++){
                                        String head = headers.getString(j);
                                        if(headers.getString(j).equals("")){
                                            head = "custom_header"+j;
                                        }

                                        if(Utils.checkValueInJsonArray(HiddenIndexs, headers.getString(j))){
                                            serverData.put("hidden_"+headers.getString(j), innerRowData.getString(j));
                                        }else{
                                            serverData.put(head, innerRowData.getString(j));
                                        }
                                    }

                                    commonModel.add(new CommonModel(serverData));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if(rows.length()==0){
                                loading=false;
                            }else {

                                if(rows.length()>=10) {
                                    loading = true;
                                }else loading = false;
                            }

                        }else {
                            loading=false;
                            noData.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        }

                        mAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(String result) {
                loading=true;
            }
        });
    }

}
