package com.httpcart.wijunglepartner.Utils;

import android.text.TextUtils;
import android.util.Patterns;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static boolean checkAlphaNumeric(String string){
        String regex = "^[a-zA-Z0-9]+$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }

    public static String changeDateFormat(String oldDate, String oldDateFormat, String newDateFormat){
        SimpleDateFormat dateFormat = new SimpleDateFormat(oldDateFormat);
        Date sourceDate = null;
        try {
            sourceDate = dateFormat.parse(oldDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat targetFormat = new SimpleDateFormat(newDateFormat);
        String newDate = targetFormat.format(sourceDate);
        return newDate;
    }
}
