package com.httpcart.wijunglepartner.Models;

import java.util.LinkedHashMap;
import java.util.Map;

public class CommonModel {

    LinkedHashMap<String, String> map;

    public CommonModel(LinkedHashMap<String, String> map){
        this.map = map;
    }

    public LinkedHashMap<String, String> getMap() {
        return map;
    }

    public void setMap(LinkedHashMap<String, String> map) {
        this.map = map;
    }
}
