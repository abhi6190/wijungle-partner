package RestInteraction;


import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

import DB.DBConstants;
import DB.DBOperations;
import Model.ChapterModel;
import Model.ClassModel;
import Model.EntranceExamTopicModel;
import Model.EntrancePaperModel;
import Model.ExamCategoryModel;
import Model.ExamPaperModel;
import Model.FFlowModel;
import Model.FriendRequestModel;
import Model.FriendsModel;
import Model.GalleryCommentModel;
import Model.ImageModel;
import Model.LinkModel;
import Model.PDFModel;
import Model.PostModel;
import Model.QuizModel;
import Model.TopicModel;
import Model.URCModel;
import Model.UserModel;
import Model.VideoModel;
import Utils.Utils;


public class RESTInteraction {

	private static RESTInteraction _instance;
	private DefaultHttpClient httpclient;
	private HttpPost postRequest;
	private HttpResponse httpResponse;
	private HttpEntity httpEntity;
	Context mContext;
	SharedPreferences sharedPreferences;
	public String userId;
	private ArrayList<NameValuePair> nameValuePairs;
	private HttpGet getRequest;
	private HttpDelete delRequest;
	DBOperations dbOperations;

	public RESTInteraction(Context ctx) {
		mContext = ctx;
		sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		dbOperations = new DBOperations(mContext);
	}

	public static RESTInteraction getInstance(Context ctx) {
		if (_instance == null) {
			_instance = new RESTInteraction(ctx);
		}
		return _instance;
	}

	private void initializeHttpClient() {
		httpclient = new DefaultHttpClient();
		nameValuePairs = new ArrayList<NameValuePair>();
	}

	private String sendHttpRequest(String url) {
		String jSonStr = null;
		try {
			postRequest = new HttpPost(url);
			postRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs,
					HTTP.UTF_8));
			httpResponse = httpclient.execute(postRequest);
			httpEntity = httpResponse.getEntity();
			if (httpEntity != null) {
				jSonStr = EntityUtils.toString(httpEntity);
				return jSonStr.substring(jSonStr.indexOf("{"));
			}
		} catch (Exception e) {
			if (e != null) {
				e.getMessage();
			}
		}
		return jSonStr;
	}

	private String sendHttpGetRequest(String url) {
		String jSonStr = null;
		try {
			HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
			DefaultHttpClient client = new DefaultHttpClient();
			SchemeRegistry registry = new SchemeRegistry();
			SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
			socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
			registry.register(new Scheme("https", socketFactory, 443));
			SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
			httpclient = new DefaultHttpClient(mgr, client.getParams());
			HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);

			getRequest = new HttpGet(url);
			httpResponse = httpclient.execute(getRequest);
			httpEntity = httpResponse.getEntity();
			if (httpEntity != null) {
				jSonStr = EntityUtils.toString(httpEntity);
				return	jSonStr.substring(jSonStr.indexOf("{"));
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jSonStr;
	}

	public String loginuser(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				if (jsonObj.getString("status").equals("success")) {
					sharedPreferences.edit().putString("userid", jsonObj.optString("id")).apply();
					sharedPreferences.edit().putString("name", jsonObj.optString("name")).apply();
					sharedPreferences.edit().putString("image", jsonObj.optString("image")).apply();
					sharedPreferences.edit().putString("email", jsonObj.optString("email")).apply();
					sharedPreferences.edit().putString("dob", jsonObj.optString("dob")).apply();
					sharedPreferences.edit().putString("address", jsonObj.optString("address")).apply();
					sharedPreferences.edit().putString("phone", jsonObj.optString("phone")).apply();
					sharedPreferences.edit().putString("country", jsonObj.optString("country")).apply();
					sharedPreferences.edit().putString("state", jsonObj.optString("state")).apply();
					sharedPreferences.edit().putString("city", jsonObj.optString("city")).apply();
					sharedPreferences.edit().putString("sname", jsonObj.optString("sname")).apply();
					if (jsonObj.optString("topics").length()>0) {
						String temp = "81,"+jsonObj.optString("topics");
						sharedPreferences.edit().putString("topics", temp).apply();
					}else{
						sharedPreferences.edit().putString("topics", jsonObj.optString("topics")).apply();
					}
				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}

	public String common(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				if (jsonObj.getString("status").equals("success")) {

				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}

	public String getAllTopics(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				if (jsonObj.getString("status").equals("success")) {
					JSONArray jsonArray = jsonObj.optJSONArray("topic");
					Utils.topicModels.clear();
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						Utils.topicModels.add(new TopicModel(jsonObject.optString("id"),
								jsonObject.optString("name"), 0));
					}
				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}

	public String getEntranceExamTopics(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				if (jsonObj.getString("status").equals("success")) {
					JSONArray jsonArray = jsonObj.optJSONArray("exam");
					Utils.entranceExamTopicModels.clear();
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						Utils.entranceExamTopicModels.add(new EntranceExamTopicModel(jsonObject.optString("id"),
								jsonObject.optString("name")));
					}
				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}

	public String getAllUsers(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				if (jsonObj.getString("status").equals("success")) {
					JSONArray jsonArray = jsonObj.optJSONArray("users");
					Utils.userModels.clear();
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						Utils.userModels.add(new UserModel(jsonObject.optString("id"),
								jsonObject.optString("name"),
								jsonObject.optString("sname"),
								jsonObject.optString("email"),
								jsonObject.optString("dd"),
								jsonObject.optString("mm"),
								jsonObject.optString("yyyy"),
								jsonObject.optString("gender"),
								jsonObject.optString("address"),
								jsonObject.optString("phone"),
								jsonObject.optString("profile_image"),
								jsonObject.optString("country"),
								jsonObject.optString("state"),
								jsonObject.optString("city")));
					}
				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}


	public String getComments(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				if (jsonObj.getString("status").equals("success")) {
					JSONArray jsonArray = jsonObj.optJSONArray("comments");
					Utils.galleryCommentModels.clear();
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						Utils.galleryCommentModels.add(new GalleryCommentModel(jsonObject.optString("c_id"),
								jsonObject.optString("user_id"),
								jsonObject.optString("c_item_id"),
								jsonObject.optString("c_text"),
								jsonObject.optString("c_when")));
					}
				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}

	public String getAllUrc(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				if (jsonObj.getString("status").equals("success")) {
					JSONArray jsonArray = jsonObj.optJSONArray("topic");
					Utils.urcModels.clear();
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						Utils.urcModels.add(new URCModel(jsonObject.optString("id"),
								jsonObject.optString("name")));
					}
				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}

	public String getAllClasses(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				if (jsonObj.getString("status").equals("success")) {
					JSONArray jsonArray = jsonObj.optJSONArray("classes");
					Utils.classModels.clear();
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						Utils.classModels.add(new ClassModel(jsonObject.optString("id"),
								jsonObject.optString("name")));
					}
				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}

	public String getAllLink(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				if (jsonObj.getString("status").equals("success")) {
					JSONArray jsonArray = jsonObj.optJSONArray("links");
					Utils.linkModels.clear();
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						Utils.linkModels.add(new LinkModel(jsonObject.optString("id"),
								jsonObject.optString("name"),
								jsonObject.optString("url"),
								jsonObject.optString("category_id")));
					}
				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}

	public String getAllImages(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				if (jsonObj.getString("status").equals("success")) {
					JSONArray jsonArray = jsonObj.optJSONArray("images");
					Utils.imageModels.clear();
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						Utils.imageModels.add(new ImageModel(jsonObject.optString("id"),
								jsonObject.optString("user_id"),
								jsonObject.optString("cat_id"),
								jsonObject.optString("topic_id"),
								jsonObject.optString("title"),
								jsonObject.optString("src"),
								jsonObject.optString("comments_count")));
					}
				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}

	public String getAllVideos(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				Utils.videoModels.clear();
				if (jsonObj.getString("status").equals("success")) {
					JSONArray jsonArray = jsonObj.optJSONArray("video");
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						Utils.videoModels.add(new VideoModel(jsonObject.optString("id"),
								jsonObject.optString("user_id"),
								jsonObject.optString("cat_id"),
								jsonObject.optString("topic_id"),
								jsonObject.optString("video_link"),
								jsonObject.optString("description"),
								jsonObject.optString("status")));
					}
				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}


	public String getlikeCount(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				if (jsonObj.getString("status").equals("success")) {
					Utils.likeCount = jsonObj.optString("count");

				}else {
					Utils.likeCount = jsonObj.optString("count");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}

	public String getunlikeCount(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				if (jsonObj.getString("status").equals("success")) {
					Utils.unlikeCount = jsonObj.optString("count");

				}else {
					Utils.unlikeCount = jsonObj.optString("count");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}

	public String getAllChapters(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				//Utils.chapterModels.clear();
				if (jsonObj.getString("status").equals("success")) {
					JSONArray jsonArray = jsonObj.optJSONArray("chapters");
//					Utils.linkModels.clear();
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						Utils.chapterModels.add(new ChapterModel(jsonObject.optString("id"),
								jsonObject.optString("class_id"),
								jsonObject.optString("chapter_name"),
								jsonObject.optString("name"),
								jsonObject.optString("category_id")));
					}
				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}

	public String getExamCategory(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				if (jsonObj.getString("status").equals("success")) {
					JSONArray jsonArray = jsonObj.optJSONArray("exam");
//					Utils.examCategoryModels.clear();
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						Utils.examCategoryModels.add(new ExamCategoryModel(jsonObject.optString("id"),
								jsonObject.optString("name"),
								jsonObject.optString("exam_id")));
					}
				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}
	public String getExamPaper(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
//				Utils.examPaperModels.clear();
				if (jsonObj.getString("status").equals("success")) {
					JSONArray jsonArray = jsonObj.optJSONArray("exampapers");

					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						Utils.examPaperModels.add(new ExamPaperModel(jsonObject.optString("id"),
								jsonObject.optString("class_id"),
								jsonObject.optString("path"),
								jsonObject.optString("year"),
								jsonObject.optString("category_id"),
								jsonObject.optString("title"),
								jsonObject.optString("type"),
								jsonObject.optString("post_date"),
								jsonObject.optString("downloads")));
					}
				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}

	public String getAllPDF(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				if (jsonObj.getString("status").equals("success")) {
					JSONArray jsonArray = jsonObj.optJSONArray("pdf");
					Utils.pdfModels.clear();
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						Utils.pdfModels.add(new PDFModel(jsonObject.optString("id"),
								jsonObject.optString("class_id"),
								jsonObject.optString("user_id"),
								jsonObject.optString("chapter_id"),
								jsonObject.optString("title"),
								jsonObject.optString("path"),
								jsonObject.optString("post_date"),
								jsonObject.optString("downloads"),
								jsonObject.optString("category_id")));
					}
				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}

	public String getAllQuiz(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				Utils.quizModels.clear();
				if (jsonObj.getString("status").equals("success")) {
					JSONArray jsonArray = jsonObj.optJSONArray("quiz");

					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						Utils.quizModels.add(new QuizModel(jsonObject.optString("id"),
								jsonObject.optString("post_date"),
								jsonObject.optString("category_id"),
								jsonObject.optString("user_id"),
								jsonObject.optString("topic_id"),
								jsonObject.optString("quiz_type_id"),
								jsonObject.optString("question"),
								jsonObject.optString("answer"),
								jsonObject.optString("feature_img")));
					}
				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}

	public String getEntrancePaper(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				if (jsonObj.getString("status").equals("success")) {
					JSONArray jsonArray = jsonObj.optJSONArray("pdf");
					Utils.entrancePaperModels.clear();
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						Utils.entrancePaperModels.add(new EntrancePaperModel(jsonObject.optString("id"),
								jsonObject.optString("exam_id"),
								jsonObject.optString("user_id"),
								jsonObject.optString("year"),
								jsonObject.optString("title"),
								jsonObject.optString("path"),
								jsonObject.optString("post_date"),
								jsonObject.optString("downloads"),
								jsonObject.optString("paper")));
					}
				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}

	public String getAllPosts(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				if (jsonObj.getString("status").equals("success")) {
					JSONArray jsonArray = jsonObj.optJSONArray("posts");
					Utils.postModels.clear();
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						Utils.postModels.add(new PostModel(jsonObject.optString("id"),
								jsonObject.optString("user_id"),
								jsonObject.optString("post_title"),
								jsonObject.optString("post_des"),
								jsonObject.optString("topic_id"),
								jsonObject.optString("dd"),
								jsonObject.optString("feature_img"),
								jsonObject.optString("slug"),
								jsonObject.optString("answer_request"),
								jsonObject.optString("count")));
					}

//					Collections.sort(Utils.postModels);
				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}

	public String getFFlowPosts(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				if (jsonObj.getString("status").equals("success")) {
					JSONArray jsonArray = jsonObj.optJSONArray("posts");

					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						Utils.fFlowModels.add(new FFlowModel(jsonObject.optString("id"),
								jsonObject.optString("user_id"),
								jsonObject.optString("post_title"),
								jsonObject.optString("post_des"),
								jsonObject.optString("topic_id"),
								jsonObject.optString("dd"),
								jsonObject.optString("feature_img"),
								jsonObject.optString("slug"),
								jsonObject.optString("answer_request"),
								jsonObject.optString("count")));
					}

//					Collections.sort(Utils.postModels);
				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}

	public String getFriendRequest(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				Utils.friendRequestModels.clear();
				if (jsonObj.getString("status").equals("success")) {
					JSONArray jsonArray = jsonObj.optJSONArray("friends");

					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						Utils.friendRequestModels.add(new FriendRequestModel(jsonObject.optString("id"),
								jsonObject.optString("sender_id"),
								jsonObject.optString("reciver_id"),
								jsonObject.optString("name")));
					}

				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}

	public String getFriends(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				Utils.friendsModels.clear();
				if (jsonObj.getString("status").equals("success")) {
					JSONArray jsonArray = jsonObj.optJSONArray("friends");

					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						Utils.friendsModels.add(new FriendsModel(jsonObject.optString("id"),
								jsonObject.optString("sender_id"),
								jsonObject.optString("reciver_id"),
								jsonObject.optString("name"),
								jsonObject.optString("image")));
					}

				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}

	public String getChat(String url){
		Log.e("url", url);
		try {
			String response = sendHttpGetRequest(url);
			if (response != null) {
				Log.e("res", response);
				JSONObject jsonObj = new JSONObject(response);
				if (jsonObj.getString("status").equals("success")) {
					JSONArray jsonArray = jsonObj.optJSONArray("chat");

					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						ContentValues contentValues = new ContentValues();
						contentValues.put(DBConstants.COLUMN_SENDER_ID, jsonObject.getString("user_id"));
						contentValues.put(DBConstants.COLUMN_RECEIVER_ID, jsonObject.getString("friend_id"));
						contentValues.put(DBConstants.COLUMN_MESSAGE, jsonObject.getString("msg"));
						contentValues.put(DBConstants.COLUMN_TIME, jsonObject.getString("time"));
						contentValues.put(DBConstants.COLUMN_STATUS, Utils.status);
						contentValues.put(DBConstants.COLUMN_BEEP, Utils.beep);
						dbOperations.addChatToDB(contentValues);
					}

				}else {
					Utils.message = jsonObj.getString("message");
				}
				return jsonObj.getString("status");
			}else {
				return "error";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "error";
	}
}
