package com.uthworks.ammhu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import Model.GalleryCommentModel;
import Model.PostCommentModel;
import RestInteraction.RESTInteraction;
import Utils.ApiUrl;
import Utils.Utils;

/**
 * Created by VARIABLE on 2/2/2017.
 */
public class PostCommentScreen extends AppCompatActivity {
    ListView articleList;
    MyAdapter myAdapter;
    RESTInteraction restInteraction;
    private Toolbar mToolbarView;
    String  postId;
    String tag_json_obj = "json_obj_req";
    ProgressDialog pDialog;
    String TAG = "MainActivity";
    SharedPreferences sharedPreferences;
    String userId = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comment_layout);
        mToolbarView = (Toolbar) findViewById(R.id.toolbar);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mToolbarView.setTitleTextColor(Color.WHITE);
        mToolbarView.setTitle("Comments");
        setSupportActionBar(mToolbarView);
        userId = sharedPreferences.getString("userid", "");
        postId= getIntent().getStringExtra("postid");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mToolbarView.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        restInteraction = RESTInteraction.getInstance(this);
        articleList = (ListView)findViewById(R.id.articleList);

        GetComments();
    }

    void GetComments(){
        pDialog = new ProgressDialog(PostCommentScreen.this);
        pDialog.setTitle("Please wait ...");
        pDialog.setMessage("Get comments ...");
        pDialog.setCancelable(false);
        pDialog.show();
//        Log.d("url", ApiUrl.getAllPostUrl + "topicid=" + catId);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getpostcommentsUrl+"post_id="+postId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        pDialog.hide();
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("comments");
                                ArrayList<PostCommentModel> postCommentModels = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    postCommentModels.add(new PostCommentModel(jsonObject.optString("post_id    "),
                                            jsonObject.optString("user_id"),
                                            jsonObject.optString("content"),
                                            jsonObject.optString("post_date"),
                                            jsonObject.optString("feature_img")));
                                }

                                if (postCommentModels.size()>0) {
                                    myAdapter = new MyAdapter(PostCommentScreen.this,postCommentModels);
                                    articleList.setAdapter(myAdapter);
                                }else{
                                    Toast.makeText(PostCommentScreen.this, "No Comments found...", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(PostCommentScreen.this, "No Comments found...", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public static class MyAdapter extends BaseAdapter {
        Context context;
        ArrayList<PostCommentModel> items;
        LayoutInflater layoutInflater;
        public MyAdapter(Context context, ArrayList<PostCommentModel> latestArticleModels){
            this.context = context;
            items = latestArticleModels;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = null;
            if (view == null){
                view = layoutInflater.inflate(R.layout.comment_list_items, parent, false);
            }
            TextView tvCommentor = (TextView)view.findViewById(R.id.tvCommentor);
            TextView tvComment = (TextView)view.findViewById(R.id.tvComment);
            tvComment.setText(Html.fromHtml(items.get(position).getContent()));
//			tvCommentor.setText("By : "+items.get(position).getName());
            tvCommentor.setText("Posted on : "+items.get(position).getPost_date());
            return view;
        }

    }

    private static  String getDate(long timeStamp){

        try{
            DateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }

}
