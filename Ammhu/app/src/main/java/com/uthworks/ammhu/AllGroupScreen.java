package com.uthworks.ammhu;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import Model.GroupModel;
import Model.PostModel;
import Utils.ApiUrl;
import Utils.Utils;
import Utils.MultipartUtility;
import adapter.FullMenuAdapter;
import adapter.GroupAdapter;

/**
 * Created by VARIABLE on 3/7/2017.
 */
public class AllGroupScreen extends AppCompatActivity{
    static RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    static RecyclerView.Adapter mAdapter;
    static String tag_json_obj = "json_obj_req";
    ProgressDialog pDialog;
    String TAG = "MainActivity";
    SharedPreferences sharedPreferences;
    String userId;
    Button btnCreateGrp;
    final String uploadFileName = "ammhu.png";
    private static final int PICK_REQUEST_IMAG = 1;
    private static final int CAPTURE_REQUEST_IMAGE = 2;
    private final int PIC_CROP = 3;
    private Uri uri;
    String TEMP_PROFILE_PIC_FILE="", imgArray;
    Random random;
    int low = 0123456;
    int high = 504598766;
    private String filePath= "";
    String path, error;
    String result = "";
    ImageView ivGrpImg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.creategroup_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Groups");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        userId = sharedPreferences.getString("userid", "");
        mRecyclerView = (RecyclerView)findViewById(R.id.recyclerView_listing);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(AllGroupScreen.this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        btnCreateGrp = (Button)findViewById(R.id.btnCreateGrp);
        btnCreateGrp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCreateDialog();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAllGroups(userId);
    }

    void showCreateDialog(){
        final Dialog dialog =new Dialog(AllGroupScreen.this, R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_group_dialog);
        ivGrpImg = (ImageView)dialog.findViewById(R.id.ivGrpImg);
        final EditText etGrpName = (EditText)dialog.findViewById(R.id.etGrpName);
        Button btnCreateGrp = (Button)dialog.findViewById(R.id.btnCreateGrp);
        ivGrpImg.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                random = new Random();
                int i2 = (random.nextInt(high - low) + low * high);
                TEMP_PROFILE_PIC_FILE = "Ammhu" + i2 + ".jpg";
                showDialog();
            }
        });
        btnCreateGrp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etGrpName.getText().toString().trim().length()>0){
                    dialog.dismiss();
                    addGroup(etGrpName.getText().toString().trim(), TEMP_PROFILE_PIC_FILE);
                }else{
                    Toast.makeText(AllGroupScreen.this, "Please enter Group title.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog.show();
    }

    void getAllGroups(String catId){
        pDialog = new ProgressDialog(AllGroupScreen.this);
        pDialog.setCancelable(false);
        pDialog.setTitle("Please wait ...");
        pDialog.setMessage("Getting groups ...");
        pDialog.show();
        Log.d("url", ApiUrl.getallgroupsUrl + "userid=" + catId);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getallgroupsUrl+"userid="+catId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        pDialog.hide();
                        try {
                            ArrayList<GroupModel> groupModels = new ArrayList<>();
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("groups");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    groupModels.add(new GroupModel(jsonObject.optString("grop_id"),
                                            jsonObject.optString("owner_id"),
                                            jsonObject.optString("group_title"),
                                            jsonObject.optString("group_img"),
                                            jsonObject.optString("date")));
                                }

                                mAdapter = new GroupAdapter(AllGroupScreen.this, groupModels);
                                mRecyclerView.setAdapter(mAdapter);

                            }else {
                                mAdapter = new GroupAdapter(AllGroupScreen.this, groupModels);
                                mRecyclerView.setAdapter(mAdapter);
                                Utils.message = jsonObj.getString("message");
                                Toast.makeText(AllGroupScreen.this, Utils.message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // hide the progress dialog
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }


    void addGroup(String groupTitle, String grpImg){
        pDialog = new ProgressDialog(AllGroupScreen.this);
        pDialog.setCancelable(false);
        pDialog.setTitle("Please wait ...");
        pDialog.setMessage("Creating group ...");
        pDialog.show();
        Log.d("url", ApiUrl.creategroupUrl + "user_id=" + userId+"&grouptitle="+ URLEncoder.encode(groupTitle)+"&groupimage="+
        URLEncoder.encode(grpImg));
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.creategroupUrl + "user_id=" + userId+"&grouptitle="+ URLEncoder.encode(groupTitle)+"&groupimage="+
                        URLEncoder.encode(grpImg), null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        pDialog.hide();
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                Utils.message = jsonObj.getString("message");
                                Toast.makeText(AllGroupScreen.this, Utils.message, Toast.LENGTH_SHORT).show();
                                getAllGroups(userId);
                            }else {
                                Utils.message = jsonObj.getString("message");
                                Toast.makeText(AllGroupScreen.this, Utils.message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // hide the progress dialog
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private void showDialog() {
        final Dialog dialog = new Dialog(AllGroupScreen.this,
                android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.custom);
        ((TextView) dialog.findViewById(R.id.text_camera))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        takePic(CAPTURE_REQUEST_IMAGE);
                    }
                });
        ((TextView) dialog.findViewById(R.id.text_gallery))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        takePic(PICK_REQUEST_IMAG);
                    }
                });
        ((ImageView) dialog.findViewById(R.id.iVCamCancel))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
        dialog.show();
    }

    private void takePic(int action) {
        if (isSDCARDMounted()) {
            switch (action) {
                case PICK_REQUEST_IMAG:
                    Intent i = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    i.setType("image/*");
                    i.putExtra("crop", "true");
                    i.putExtra("aspectX", 1.5);
                    i.putExtra("aspectY", 1);
                    i.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri());
                    i.putExtra("outputFormat",
                            Bitmap.CompressFormat.JPEG.toString());
                    startActivityForResult(i, PIC_CROP);
                    break;
                case CAPTURE_REQUEST_IMAGE:
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    uri = getOutputMediaFileUri(); // create a file to save the
                    // image
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    startActivityForResult(intent, CAPTURE_REQUEST_IMAGE);
                    break;

                default:
                    break;
            }
        }
    }

    private Uri getTempUri() {
        Uri uri = null;
        if (getTempFile() != null) {
            uri = Uri.fromFile(getTempFile());
        }
        return uri;
    }

    private File getTempFile() {
        if (isSDCARDMounted()) {
            File f = new File(Environment.getExternalStorageDirectory(),
                    TEMP_PROFILE_PIC_FILE);
            try {
                f.createNewFile();
            } catch (IOException e) {

            }
            return f;
        } else {
            return null;
        }
    }

    /** Create a File for saving an image or video */
    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "Ammhu");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("", "failed to create directory");
                return null;
            }
        }

        File mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_DEMO.jpeg");
        if (!mediaFile.exists())
            try {
                mediaFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        return mediaFile;
    }

    /** Create a file Uri for saving an image or video */
    private static Uri getOutputMediaFileUri() {
        return Uri.fromFile(getOutputMediaFile());
    }

    static boolean isSDCARDMounted() {
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED))
            return true;
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CAPTURE_REQUEST_IMAGE: {
                    cropImage(uri);
                }
                break;
                case PIC_CROP: {
                    File tempFile = getTempFile();
                    String name = "";
                    filePath = Environment.getExternalStorageDirectory() + "/"
                            + TEMP_PROFILE_PIC_FILE;
                    imgArray = imageToByteArray(filePath);
                }
                break;
                default:
                    break;
            }
        }
    }

    private String imageToByteArray(String path) {
        Bitmap bm = decodeSampledBitmapFromResource(path, 150, 50);
        ivGrpImg.setImageBitmap(bm);
        new UploadImageAsync().execute();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
        } catch (Exception e) {
            filePath = null;
            return null;
        }
    }

    public Bitmap RotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        Bitmap bitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
        return bitmap;
    }
    private void cropImage(Uri picUri) {
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.setDataAndType(picUri, "image/*");
        cropIntent.putExtra("crop", "true");
        cropIntent.putExtra("aspectX", 1.5);
        cropIntent.putExtra("aspectY", 1);
        cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri());
        cropIntent.putExtra("outputFormat",
                Bitmap.CompressFormat.JPEG.toString());
        startActivityForResult(cropIntent, PIC_CROP);
    }



    public static Bitmap decodeSampledBitmapFromResource(String filepath,
                                                         int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filepath, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filepath, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public String uploadFile(String sourceFileUri) {

        String fileName = sourceFileUri;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        File uploadFile1 = new File("e:/Test/images.jpg");
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);

        if (sourceFile != null && !sourceFile.isFile()) {
            Log.e("uploadFile", "Source File not exist :"
                    +filePath + "" + uploadFileName);

            runOnUiThread(new Runnable() {
                public void run() {
                }
            });

            return "fail";
        }
        else
        {
            String charset = "UTF-8";
            File file = new File(fileName);

            try {
                MultipartUtility multipart = new MultipartUtility("https://ammhu.com/android/uploadScript1.php", charset);

                multipart.addHeaderField("User-Agent", "CodeJava");
                multipart.addHeaderField("Test-Header", "Ammhu");
//                multipart.addFormField("userid", userId);
                multipart.addFilePart("imageFile[]", file);

                List<String> response = multipart.finish();
                System.out.println("SERVER REPLIED:");

                for (String line : response) {
                    System.out.println(line);
                    result = line;
                }
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.optString("status").equalsIgnoreCase("success")) {
                        result = "success";
                        boolean delete = file.delete();
                    }else {
                        if (jsonObject.has("errors")) {
                            error = jsonObject.getString("errors");
                        }
                        boolean delete = file.delete();
                        result = "Failure";
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    boolean delete = file.delete();
                }
            } catch (IOException ex) {
                System.err.println(ex);
                boolean delete = file.delete();
            }
            return result;

        } // End else block 
    }

    public class UploadImageAsync extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(AllGroupScreen.this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Please wait...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return uploadFile(filePath);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("success")) {
//                updateImage();
            }else if (result.equalsIgnoreCase("Failure")) {
                Toast.makeText(AllGroupScreen.this, "Oops.. An error occurred. Please try again", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(AllGroupScreen.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
