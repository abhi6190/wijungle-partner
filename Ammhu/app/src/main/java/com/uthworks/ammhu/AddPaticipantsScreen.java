package com.uthworks.ammhu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;

import Model.FriendsModel;
import Model.ParticipantModel;
import Model.PostCommentModel;
import Utils.ApiUrl;
import Utils.Utils;
import adapter.FriendsAdapter;

/**
 * Created by VARIABLE on 3/9/2017.
 */
public class AddPaticipantsScreen extends AppCompatActivity {
    ListView list;
    Button btnCreateGrp;
    MyAdapter myAdapter;
    String userId, groupId;
    SharedPreferences sharedPreferences;
    String tag_json_obj = "json_obj_req";
    ProgressDialog pDialog;
    String TAG = "MainActivity";
    ArrayList<ParticipantModel> participantModels;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_participants);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Add Partcipants");
        setSupportActionBar(toolbar);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        userId = sharedPreferences.getString("userid", "");
        groupId = getIntent().getStringExtra("groupid");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        list = (ListView)findViewById(R.id.list);
        btnCreateGrp = (Button)findViewById(R.id.btnCreateGrp);
        btnCreateGrp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String allId ="";
                for (int i = 0; i < participantModels.size(); i++) {
                    if (participantModels.get(i).getIsCheck().equalsIgnoreCase("1")){
                        if (participantModels.get(i).getSender_id().equalsIgnoreCase(userId)) {
                            allId += participantModels.get(i).getReciver_id() + ",";
                        }else{
                            allId += participantModels.get(i).getSender_id()+",";
                        }
                    }
                }
                if (allId.length()>0) {
                    allId = allId.substring(0,allId.length()-1);
                    addFriends(allId);
                }else{
                    Toast.makeText(AddPaticipantsScreen.this, "Please select Participants.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        getFriends();
    }

    void getFriends(){
        pDialog = new ProgressDialog(AddPaticipantsScreen.this);
        pDialog.setCancelable(false);
        pDialog.setTitle("Please wait ...");
        pDialog.setMessage("Getting friends ...");
        pDialog.show();
//       String url = "http://elogic.southindia.cloudapp.azure.com:8083/transtruck/admindashboard/tables/get/childadminshowList.php?pageno=1&limit=10";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getFriendsUrl + "reciver_id=" + userId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        pDialog.hide();
                        try {
                            participantModels = new ArrayList<>();
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("friends");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    participantModels.add(new ParticipantModel(jsonObject.optString("id"),
                                            jsonObject.optString("sender_id"),
                                            jsonObject.optString("reciver_id"),
                                            jsonObject.optString("name"),
                                            jsonObject.optString("image"),
                                            "0"));
                                }
                                myAdapter = new MyAdapter(AddPaticipantsScreen.this, participantModels);
                                list.setAdapter(myAdapter);

                            }else {
                                myAdapter = new MyAdapter(AddPaticipantsScreen.this, participantModels);
                                list.setAdapter(myAdapter);
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    void addFriends(String friendId){
        pDialog = new ProgressDialog(AddPaticipantsScreen.this);
        pDialog.setCancelable(false);
        pDialog.setTitle("Please wait ...");
        pDialog.setMessage("Adding partcipants ...");
        pDialog.show();
        Log.d("url", ApiUrl.addparticipantsUrl + "groupid=" + groupId+"&userid="+ URLEncoder.encode(friendId)+"&groupowner="+userId );
//       String url = "http://elogic.southindia.cloudapp.azure.com:8083/transtruck/admindashboard/tables/get/childadminshowList.php?pageno=1&limit=10";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.addparticipantsUrl + "groupid=" + groupId+"&userid="+friendId+"&groupowner="+userId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        pDialog.hide();
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                finish();

                            }else {

                                Utils.message = jsonObj.getString("message");
                                Toast.makeText(AddPaticipantsScreen.this, Utils.message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public  class MyAdapter extends BaseAdapter {
        Context context;
        ArrayList<ParticipantModel> items;
        LayoutInflater layoutInflater;
        public MyAdapter(Context context, ArrayList<ParticipantModel> latestArticleModels){
            this.context = context;
            items = latestArticleModels;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = null;
            if (view == null){
                view = layoutInflater.inflate(R.layout.participants_list_items, parent, false);
            }
            ImageView ivGrpImage = (ImageView)view.findViewById(R.id.ivGrpImage);
            TextView tvGrpName = (TextView)view.findViewById(R.id.tvGrpName);
            CheckBox cbAddFriend = (CheckBox)view.findViewById(R.id.cbAddFriend);
            tvGrpName.setText(Html.fromHtml(items.get(position).getName()));
            if (items.get(position).getImage().length()>0){
                String url = "https://ammhu.com/images/"+items.get(position).getImage();
                url=url.replaceAll(" ", "%20");
                Picasso.with(context)
                        .load(url)
                        .placeholder(R.mipmap.photo)
                        .into(ivGrpImage);
            }

            if (items.get(position).getIsCheck().equalsIgnoreCase("1")){
                cbAddFriend.setChecked(true);
            }else{
                cbAddFriend.setChecked(false);
            }

            cbAddFriend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (items.get(position).getIsCheck().equalsIgnoreCase("1")){
                        ParticipantModel tempParticipantModel = new ParticipantModel(items.get(position).getId(),
                                items.get(position).getSender_id(),
                                items.get(position).getReciver_id(),
                                items.get(position).getName(),
                                items.get(position).getImage(),
                                "0");
                        participantModels.remove(position);
                        participantModels.add(position, tempParticipantModel);
                    }else{
                        ParticipantModel tempParticipantModel = new ParticipantModel(items.get(position).getId(),
                                items.get(position).getSender_id(),
                                items.get(position).getReciver_id(),
                                items.get(position).getName(),
                                items.get(position).getImage(),
                                "1");
                        participantModels.remove(position);
                        participantModels.add(position, tempParticipantModel);
                    }
                    notifyDataSetChanged();
                }
            });


            return view;
        }

    }
}
