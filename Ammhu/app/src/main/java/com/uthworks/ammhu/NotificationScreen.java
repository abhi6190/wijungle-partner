package com.uthworks.ammhu;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Model.NotificationModel;
import Model.PDFModel;
import Model.PostModel;
import Utils.ApiUrl;
import Utils.Utils;
import adapter.FullMenuAdapter;

/**
 * Created by VARIABLE on 1/19/2017.
 */
public class NotificationScreen extends AppCompatActivity {
    ListView downloadList;
    String chapterid;
    MyAdapter myAdapter;
    SharedPreferences sharedPreferences;
    String userId;
    String tag_json_obj = "json_obj_req";
    ProgressDialog pDialog;
    String TAG = "MainActivity";
    ArrayList<NotificationModel> notificationModels;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.download_layout);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        userId = sharedPreferences.getString("userid", "");
        toolbar.setTitle("Notifications");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        downloadList = (ListView) findViewById(R.id.downloadList);
        downloadList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(NotificationScreen.this, NotiPostDetailScreen.class).putExtra("postid", notificationModels.get(position).getPost_id()));
            }
        });
        getAllNotification();
    }

    void getAllNotification(){
        pDialog = new ProgressDialog(NotificationScreen.this);
        pDialog.setTitle("Please wait ...");
        pDialog.setMessage("Get notifications ...");
        pDialog.setCancelable(false);
        pDialog.show();
//        Log.d("url", ApiUrl.getAllPostUrl + "topicid=" + catId);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getnotificationsUrl+"user_id="+userId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        pDialog.hide();
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("noti");
                                 notificationModels = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    notificationModels.add(new NotificationModel(jsonObject.optString("user_id"),
                                            jsonObject.optString("from"),
                                            jsonObject.optString("name"),
                                            jsonObject.optString("type"),
                                            jsonObject.optString("message"),
                                            jsonObject.optString("post_id")));
                                }

                                myAdapter = new MyAdapter(NotificationScreen.this, notificationModels);
                                downloadList.setAdapter(myAdapter);

                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    class MyAdapter extends BaseAdapter {
        Context context;
        LayoutInflater layoutInflater;
        ArrayList<NotificationModel> pdfModels;
        public MyAdapter(Context context, ArrayList<NotificationModel> pdfModels){
            this.context = context;
            this.pdfModels = pdfModels;
            layoutInflater = LayoutInflater.from(context);
        }
        @Override
        public int getCount() {
            return pdfModels.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = null;
            if (view == null){
                view = layoutInflater.inflate(R.layout.noti_list_items, parent, false);
            }
            TextView tvDissh = (TextView)view.findViewById(R.id.tvDissh);

            tvDissh.setText("New Post by "+Html.fromHtml(pdfModels.get(position).getName())+" "+Html.fromHtml(pdfModels.get(position).getMessage()));
            return view;
        }
    }
}
