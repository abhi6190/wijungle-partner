package com.uthworks.ammhu;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import DB.DBConstants;
import DB.DBOperations;
import Model.EntrancePaperModel;
import Model.TopicModel;
import Utils.Utils;
import Utils.ApiUrl;

/**
 * Created by VARIABLE on 9/25/2016.
 */
public class SignupScreen extends AppCompatActivity {
    EditText name, email, password;
    static EditText dob;
    Button email_sign_in_button;
    List<String> categories = new ArrayList<String>();
    String strSpinner = "";
    DBOperations dbOperations;
    AutoCompleteTextView autoCompleteTextView1, autoCompleteTextView2;
    ArrayList<String> countryList, stateList;
    String countryName="", stateName="";
    GridView gridView;
    MyAdapter myAdapter;
    String tag_json_obj = "json_obj_req";
    String TAG = "OwnProfileScreen";
    ProgressDialog pDialog;
    ArrayList<TopicModel> tempModels = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_layout);
        gridView = (GridView)findViewById(R.id.gridView);
        for (int i = 0; i < Utils.topicModels.size(); i++) {
            if(!Utils.topicModels.get(i).getId().equals("81")){
                tempModels.add(Utils.topicModels.get(i));
            }
        }
        myAdapter = new MyAdapter(this,tempModels);
        gridView.setAdapter(myAdapter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dbOperations =new DBOperations(this);
        autoCompleteTextView1 = (AutoCompleteTextView)findViewById(R.id.autoCompleteTextView1);
        autoCompleteTextView2 = (AutoCompleteTextView)findViewById(R.id.autoCompleteTextView2);
        name = (EditText)findViewById(R.id.name);
        email = (EditText)findViewById(R.id.email);
        dob = (EditText)findViewById(R.id.dob);
        password = (EditText)findViewById(R.id.password);
        email_sign_in_button = (Button)findViewById(R.id.email_sign_in_button);
        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });
         countryList = getCountry();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.select_dialog_item, countryList);
        autoCompleteTextView1.setThreshold(1);
        autoCompleteTextView1.setAdapter(adapter);
        autoCompleteTextView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                countryName = autoCompleteTextView1.getText().toString();
//                stateList.clear();
                stateList = getState(countryName);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(SignupScreen.this,
                        android.R.layout.select_dialog_item, stateList);
                autoCompleteTextView2.setThreshold(1);
                autoCompleteTextView2.setAdapter(adapter);

            }
        });

        autoCompleteTextView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                stateName = autoCompleteTextView2.getText().toString();

            }
        });


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(SignupScreen.this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        email_sign_in_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userName = name.getText().toString().trim();
                String userEmail = email.getText().toString().trim();
                String dobStr = dob.getText().toString().trim();
                String userPwd = password.getText().toString().trim();
                if (userName.length() == 0){
                    showMessage("Enter your Name");
                    return;
                }

                if (userEmail.length() == 0){
                    showMessage("Enter your Email");
                    return;
                }

                if (dobStr.length() == 0){
                    showMessage("Enter your Date of Birth");
                    return;
                }

                if (userPwd.length() <= 4){
                    showMessage("Password must be 4 characters long");
                    return;
                }

                if(autoCompleteTextView1.getText().toString().length() == 0){
                    showMessage("Enter country");
                    return;
                }
                if(autoCompleteTextView2.getText().toString().length() == 0){
                    showMessage("Enter state");
                    return;
                }
                String ids = "";
                for (int i = 0; i < Utils.topicModels.size(); i++) {
                    if (Utils.topicModels.get(i).getIsCheck() == 1){
                        ids += Utils.topicModels.get(i).getId() + ",";
                    }
                }
                if (ids.contains(",")) {
                    ids = ids.substring(0,ids.length()-1);
                }
                //Toast.makeText(SignupScreen.this, ids, Toast.LENGTH_SHORT).show();
                String url = ApiUrl.registerUrl+"fname="+URLEncoder.encode(userName)
                        +"&email="+URLEncoder.encode(userEmail) + "&dob=" + URLEncoder.encode(dobStr)
                        + "&pwd=" + URLEncoder.encode(userPwd) +
                        "&country=" + URLEncoder.encode(autoCompleteTextView1.getText().toString())
                        + "&state=" + URLEncoder.encode(autoCompleteTextView2.getText().toString())
                        +"&topics="+URLEncoder.encode(ids)+
                        "&type=app";
                SignUp(url);

            }
        });
    }

    void SignUp(String url){
        Log.d("url", url);
        pDialog = new ProgressDialog(SignupScreen.this);
        pDialog.setTitle("Please wait ...");
        pDialog.setMessage("Signing up ...");
        pDialog.setCancelable(false);
        pDialog.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        pDialog.hide();
                        if (jsonObj.optString("status").equals("success")){
                            startActivity(new Intent(SignupScreen.this, LaunchingActivity.class));
                            finish();
                            Toast.makeText(SignupScreen.this, jsonObj.optString("message"), Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(SignupScreen.this, jsonObj.optString("message"), Toast.LENGTH_SHORT).show();
                        }
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog_MinWidth, this, year, month, day);
            datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            return datePickerDialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            dob.setText(new StringBuilder().append(day).append("-")
                    .append(month).append("-").append(year));

        }
    }


    class MyAdapter extends BaseAdapter {
        Context context;
        LayoutInflater layoutInflater;
        ArrayList<TopicModel> items;
        public MyAdapter(Context context, ArrayList<TopicModel> items){
            this.context = context;
            this.items = items;
            layoutInflater = LayoutInflater.from(context);
        }
        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = null;
            if (view == null){
                view = layoutInflater.inflate(R.layout.grid_items_topics, parent, false);
            }
            CheckBox cbTopic = (CheckBox)view.findViewById(R.id.cbTopic);
            cbTopic.setText(items.get(position).getTopicName());
            if (items.get(position).getIsCheck() == 1){
                cbTopic.setChecked(true);
            }else{
                cbTopic.setChecked(false);
            }
            cbTopic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TopicModel model = null;
                    if (items.get(position).getIsCheck() == 1){
                         model = new TopicModel(items.get(position).getId(),
                                items.get(position).getTopicName(), 0);
                    }else{
                         model = new TopicModel(items.get(position).getId(),
                                items.get(position).getTopicName(), 1);
                    }
                    items.remove(position);
                    items.add(position, model);
                    notifyDataSetChanged();
                }
            });
            return view;
        }
    }

    ArrayList<String> getCountry()
    {
        ArrayList<String> strings = new ArrayList<>();
        Cursor cursor = dbOperations.getCountry();
        if (cursor != null && cursor.getCount()>0){
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                strings.add(cursor.getString(cursor.getColumnIndexOrThrow(DBConstants.COLUMN_NAME)));
            }

        }
        return strings;
    }

    ArrayList<String> getState(String cntry)
    {
        ArrayList<String> strings = new ArrayList<>();
        Cursor cursor = dbOperations.getStates(cntry);
        if (cursor != null && cursor.getCount()>0){
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                strings.add(cursor.getString(cursor.getColumnIndexOrThrow(DBConstants.COLUMN_NAME)));
            }
        }
        return strings;
    }

    void showMessage(String message){
        Toast.makeText(SignupScreen.this, message, Toast.LENGTH_SHORT).show();
    }

}
