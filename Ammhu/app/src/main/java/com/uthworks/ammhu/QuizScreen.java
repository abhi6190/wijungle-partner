package com.uthworks.ammhu;

import android.app.DownloadManager;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import Model.EntrancePaperModel;
import RestInteraction.RESTInteraction;

/**
 * Created by VARIABLE on 11/25/2016.
 */
public class QuizScreen extends AppCompatActivity {
    ListView downloadList;
    RESTInteraction restInteraction;
    MyAdapter myAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.download_layout);
        restInteraction = RESTInteraction.getInstance(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Quiz");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        downloadList = (ListView)findViewById(R.id.downloadList);
    }

    class MyAdapter extends BaseAdapter {
        Context context;
        LayoutInflater layoutInflater;
        ArrayList<EntrancePaperModel> pdfModels;
        public MyAdapter(Context context, ArrayList<EntrancePaperModel> pdfModels){
            this.context = context;
            this.pdfModels = pdfModels;
            layoutInflater = LayoutInflater.from(context);
        }
        @Override
        public int getCount() {
            return pdfModels.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = null;
            if (view == null){
                view = layoutInflater.inflate(R.layout.quiz_list_items, parent, false);
            }
            TextView tvQuestion = (TextView)view.findViewById(R.id.tvQuestion);
            TextView tvDate = (TextView)view.findViewById(R.id.tvDate);
            TextView tvViewAns = (TextView)view.findViewById(R.id.tvViewAns);

            tvViewAns.setText(pdfModels.get(position).getDownloads());
            tvQuestion.setText(Html.fromHtml(pdfModels.get(position).getTitle()));
            tvDate.setText(pdfModels.get(position).getPost_date());
            return view;
        }
    }
}
