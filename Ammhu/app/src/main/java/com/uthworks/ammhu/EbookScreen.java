package com.uthworks.ammhu;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Model.EbookCategoryModel;
import Model.PostModel;
import Model.UserModel;
import Utils.ApiUrl;
import Utils.Utils;
import adapter.EbookAdapter;
import adapter.FullMenuAdapter;
import adapter.UserAdapter;

/**
 * Created by VARIABLE on 1/19/2017.
 */
public class EbookScreen extends AppCompatActivity {
    RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;
    String userId;
    SharedPreferences sharedPreferences;
    String tag_json_obj = "json_obj_req";
    ProgressDialog pDialog;
    String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.firend_request_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("EBooks");
        setSupportActionBar(toolbar);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        userId = sharedPreferences.getString("userid", "");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_listing);
        mRecyclerView.setHasFixedSize(true);
//        llBottom = (LinearLayout)view.findViewById(R.id.llBottom);
//        llBottom.setVisibility(View.VISIBLE);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        getAllEbookCat();
    }

    void getAllEbookCat(){
        pDialog = new ProgressDialog(EbookScreen.this);
        pDialog.setTitle("Please wait ...");
        pDialog.setMessage("Get ebook categories ...");
        pDialog.setCancelable(false);
        pDialog.show();
        String url = "";
        if (Utils.isEbook){
            url = ApiUrl.getebookcategory2Url;
        }else{
            url = ApiUrl.getebookcategoryUrl;
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        pDialog.hide();
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("categories");
                                ArrayList<EbookCategoryModel> ebookCategoryModels = new ArrayList<>();
                                if (jsonArray != null) {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        ebookCategoryModels.add(new EbookCategoryModel(jsonObject.optString("id"),
                                                jsonObject.optString("name")));
                                    }
                                }

                                mAdapter = new EbookAdapter(EbookScreen.this, ebookCategoryModels);
                                mRecyclerView.setAdapter(mAdapter);

                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }
}
