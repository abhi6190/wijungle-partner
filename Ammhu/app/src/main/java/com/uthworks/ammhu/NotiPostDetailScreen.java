package com.uthworks.ammhu;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.login.LoginManager;
import com.google.android.gms.plus.Plus;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Model.NotificationModel;
import Model.PostModel;
import RestInteraction.RESTInteraction;
import Utils.Utils;
import Utils.ApiUrl;
import adapter.HorizontalAdapter;

/**
 * Created by VARIABLE on 1/19/2017.
 */
public class NotiPostDetailScreen extends AppCompatActivity {
    ImageView ivFullImage, ivLike, ivUnlike;
    TextView tvFullImageName, tvViewComments;
    EditText etComment;
    RESTInteraction restInteraction;
    private ViewPager viewPager1;
//    Button btnPostCmnt;
    String postId;
    TextView tvUnlike, tvLike, tvPostDesc ;
    String tag_json_obj = "json_obj_req";
    ProgressDialog pDialog;
    String TAG = "MainActivity";
    ArrayList<PostModel> postModels;
    SharedPreferences sharedPreferences;
    String userId= "";
    FloatingActionButton floatingActionButton;
    private int currentIndex;
    private int startIndex;
    private int endIndex;
    List<String> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_detail_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Post");
        setSupportActionBar(toolbar);
        viewPager1 = (ViewPager) findViewById(R.id.pager);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        restInteraction = RESTInteraction.getInstance(this);
        postId = getIntent().getStringExtra("postid");
        floatingActionButton = (FloatingActionButton)findViewById(R.id.fab);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        userId = sharedPreferences.getString("userid", "");
        tvLike = (TextView)findViewById(R.id.tvLike);
        tvUnlike = (TextView)findViewById(R.id.tvUnlike);
        tvPostDesc = (TextView)findViewById(R.id.tvPostDesc);
        getLikeCount();
        getUnLikeCount();
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(NotiPostDetailScreen.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.forgot_password);
                final EditText etEmailFP = (EditText)dialog.findViewById(R.id.etEmailFP);
                Button btnForgtPwd = (Button)dialog.findViewById(R.id.btnForgtPwd);
                btnForgtPwd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (etEmailFP.getText().toString().length()>0){
                            dialog.dismiss();
                            new SendComment(postId, userId, etEmailFP.getText().toString(), etEmailFP).execute();
                        }else{
                            Toast.makeText(NotiPostDetailScreen.this, "Please enter your comment.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                dialog.show();
            }
        });
        ivFullImage = (ImageView)findViewById(R.id.ivFullImage);
        ivLike = (ImageView)findViewById(R.id.ivLike);
        ivUnlike = (ImageView)findViewById(R.id.ivUnlike);
        getSinglePost();
        ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userId.length()>0) {
                    likeUnlikePost("1");
                }else{
                    startActivity(new Intent(NotiPostDetailScreen.this, LaunchingActivity.class));
                }
            }
        });

        ivUnlike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userId.length()>0) {
                    likeUnlikePost("0");
                }else{
                    startActivity(new Intent(NotiPostDetailScreen.this, LaunchingActivity.class));
                }
            }
        });
        tvFullImageName = (TextView)findViewById(R.id.tvFullImageName);
        tvViewComments = (TextView)findViewById(R.id.tvViewComments);
        tvViewComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NotiPostDetailScreen.this, PostCommentScreen.class).putExtra("postid", postId));
            }
        });
        etComment = (EditText)findViewById(R.id.etComment);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.share, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
      if (id == R.id.action_share) {
            if (sharedPreferences.getString("userid", "").length()>0) {
                if(postModels.size()>0) {
//                    String  html = "Check out the latest post at Ammhu : " + postModels.get(0).getPost_title()
//                            + "</br>https://ammhu.com/post/" + postModels.get(0).getId()+"/"+postModels.get(0).getSlug()
//                            + "</br>Click on the below link to get the app now: </br>https://play.google.com/store/apps/details?id=com.uthworks.ammhu&hl=en";
//                    Intent sendIntent = new Intent();
//                    sendIntent.setAction(Intent.ACTION_SEND);
//                    sendIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(html));
//                    sendIntent.setType("text/plain");
//                    startActivity(sendIntent);


                    try {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, "Ammhu");
                        String sAux = "\nCheck out the latest post at Ammhu : \n"+postModels.get(0).getPost_title()+"\n\n";
                        sAux = sAux + "https://play.google.com/store/apps/details?id=com.uthworks.ammhu&hl=en \n\n";
                        i.putExtra(Intent.EXTRA_TEXT, sAux);
                        startActivity(Intent.createChooser(i, "choose one"));
                    } catch(Exception e) {
                        //e.toString();
                    }


                }else
                    Toast.makeText(NotiPostDetailScreen.this, "Unable to share the post now. Please try again.", Toast.LENGTH_SHORT).show();
            }else{
                startActivity(new Intent(NotiPostDetailScreen.this, LaunchingActivity.class));
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void getSinglePost(){
        pDialog = new ProgressDialog(NotiPostDetailScreen.this);
        pDialog.setTitle("Please wait ...");
        pDialog.setMessage("Get post ...");
        pDialog.setCancelable(false);
        pDialog.show();
//        Log.d("url", ApiUrl.getAllPostUrl + "topicid=" + catId);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getsinglepostUrl+"postid="+postId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        pDialog.hide();
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("posts");
                                postModels = new ArrayList<>();
                                if (jsonArray != null) {

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        postModels.add(new PostModel(jsonObject.optString("id"),
                                                jsonObject.optString("user_id"),
                                                jsonObject.optString("post_title"),
                                                jsonObject.optString("post_des"),
                                                jsonObject.optString("topic_id"),
                                                jsonObject.optString("dd"),
                                                jsonObject.optString("feature_img"),
                                                jsonObject.optString("slug"),
                                                jsonObject.optString("answer_request"),
                                                jsonObject.optString("count")));
                                    }
                                    if (postModels.get(0).getFeature_img().length()>0) {
                                       items = Arrays.asList(postModels.get(0).getFeature_img().split("\\s*,\\s*"));
                                        HorizontalAdapter adapter = new HorizontalAdapter(NotiPostDetailScreen.this, items);
                                        viewPager1.setAdapter(adapter);

                                        CirclePageIndicator titleIndicator = (CirclePageIndicator) findViewById(R.id.titles);
                                        titleIndicator.setViewPager(viewPager1);
                                        titleIndicator.setFillColor(Color.parseColor("#76c2af"));
                                    }else{
                                        viewPager1.setVisibility(View.GONE);
                                        ivFullImage.setVisibility(View.VISIBLE);
                                        ivFullImage.setImageResource(R.mipmap.placeholder);
                                    }
//                                    else{
//                                        String url = "https://ammhu.com/uploads/"+postModels.get(0).getFeature_img();
//                                        url=url.replaceAll(" ", "%20");
//                                        if (postModels.get(0).getFeature_img().length()>0) {
////                                            Picasso.with(NotiPostDetailScreen.this)
////                                                    .load(url)
////                                                    .placeholder(R.mipmap.placeholder) // optional
////                                                    .error(R.mipmap.placeholder)         // optional
////                                                    .into(ivFullImage);
//                                        }else{
//                                            ivFullImage.setImageResource(R.mipmap.placeholder);
//                                        }
//                                    }


                                    tvFullImageName.setText(Html.fromHtml(postModels.get(0).getPost_title()));
                                    tvPostDesc.setText(Html.fromHtml(postModels.get(0).getPost_des()));
                                }


                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }



    void getLikeCount(){
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getpostlikecountUrl+"post_id="+postId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                tvLike.setText(jsonObj.optString("count"));

                            }else {
                                tvLike.setText("0");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }


    void getUnLikeCount(){
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getpostunlikecountUrl+"post_id="+postId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                tvUnlike.setText(jsonObj.optString("count"));

                            }else {
                                tvUnlike.setText("0");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    void likeUnlikePost(String vote){
        pDialog = new ProgressDialog(NotiPostDetailScreen.this);
        pDialog.setTitle("Please wait ...");
        pDialog.setMessage("Voting ...");
        pDialog.setCancelable(false);
        pDialog.show();
//        Log.d("url", ApiUrl.getAllPostUrl + "topicid=" + catId);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.post_votesUrl+"post_id="+postId+"&user_id="+userId+"&vote="+vote, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        pDialog.hide();
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                getLikeCount();
                                getUnLikeCount();

                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }



    public class SendComment extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String catid;
        String topicid;
        String videoLink;
        EditText editText;

        public SendComment(String catid, String topicid, String videoLink, EditText editText){
            this.catid = catid;
            this.topicid = topicid;
            this.videoLink = videoLink;
            this.editText = editText;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(NotiPostDetailScreen.this);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Please wait ...");
            progressDialog.setMessage("Posting comment ...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.common(ApiUrl.post_commentUrl + "post_id=" + catid + "&user_id=" + topicid
                    + "&content=" + URLEncoder.encode(videoLink));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("success")) {
                editText.setText("");
                Toast.makeText(NotiPostDetailScreen.this, "Comment post succesfully.", Toast.LENGTH_SHORT).show();
            }else if (result.equalsIgnoreCase("fail")) {
//                Toast.makeText(LaunchingActivity.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
//                Toast.makeText(LaunchingActivity.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }


}
