package com.uthworks.ammhu;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Model.EBookSubCatModel;
import Model.EbookCategoryModel;
import Utils.ApiUrl;
import Utils.Utils;
import adapter.EBookSubCatAdapter;
import adapter.EbookAdapter;

/**
 * Created by VARIABLE on 1/19/2017.
 */
public class EBookSubCatScreen extends AppCompatActivity {
    RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;
    String userId;
    SharedPreferences sharedPreferences;
    String tag_json_obj = "json_obj_req";
    ProgressDialog pDialog;
    String TAG = "MainActivity";
    String catId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.firend_request_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("EBooks Sub Category");
        setSupportActionBar(toolbar);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        userId = sharedPreferences.getString("userid", "");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        catId = getIntent().getStringExtra("catid");
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_listing);
        mRecyclerView.setHasFixedSize(true);
//        llBottom = (LinearLayout)view.findViewById(R.id.llBottom);
//        llBottom.setVisibility(View.VISIBLE);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        getAllEbookCat(catId);
    }

    void getAllEbookCat(String catId){
        pDialog = new ProgressDialog(EBookSubCatScreen.this);
        pDialog.setTitle("Please wait ...");
        pDialog.setMessage("Get ebook categories ...");
        pDialog.setCancelable(false);
        pDialog.show();
        String url = "";
        if (Utils.isEbook){
            url = ApiUrl.getsubcategoryebook2Url;
        }else{
            url = ApiUrl.getsubcategoryebookUrl;
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url+"pcat_id="+catId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        pDialog.hide();
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("categories");
                                ArrayList<EBookSubCatModel> ebookCategoryModels = new ArrayList<>();
                                if (jsonArray != null) {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        ebookCategoryModels.add(new EBookSubCatModel(jsonObject.optString("id"),
                                                jsonObject.optString("subcat"),
                                                jsonObject.optString("pcat_id")));
                                    }
                                }

                                mAdapter = new EBookSubCatAdapter(EBookSubCatScreen.this, ebookCategoryModels);
                                mRecyclerView.setAdapter(mAdapter);

                            }else {
                                Utils.message = jsonObj.getString("message");
                                Toast.makeText(EBookSubCatScreen.this, jsonObj.getString("message"), Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }
}
