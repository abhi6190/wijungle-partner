package com.uthworks.ammhu;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import DB.DBConstants;
import DB.DBOperations;
import Model.ParticipantModel;
import RestInteraction.RESTInteraction;
import Utils.ApiUrl;
import Utils.Utils;

/**
 * Created by VARIABLE on 3/10/2017.
 */
public class GroupChatScreen extends AppCompatActivity {
    ScrollView sv;
    LinearLayout llMain;
    EditText etMessage;
    ImageView ivSendMessage;
    RESTInteraction restInteraction;
    DBOperations dbOperations;
    SharedPreferences sharedPreferences;
    String userid, groupId;
    Cursor cursor;
    String tag_json_obj = "json_obj_req";
    ProgressDialog pDialog;
    String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Group Chat");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        sv = (ScrollView)findViewById(R.id.sv);
        restInteraction = RESTInteraction.getInstance(this);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        dbOperations = new DBOperations(this);
        userid = sharedPreferences.getString("userid", "");
        groupId = getIntent().getStringExtra("groupid");
        llMain = (LinearLayout)findViewById(R.id.llMain);
        etMessage = (EditText)findViewById(R.id.etMessage);
        ivSendMessage = (ImageView)findViewById(R.id.ivSendMessage);
        ivSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etMessage.getText().toString().length()>0) {
                    new SendMessage(groupId, userid, etMessage.getText().toString()).execute();
                }else{
                    Toast.makeText(GroupChatScreen.this, "Please enter message.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        GroupChatScreen.this.registerReceiver(mMessageReceiver, new IntentFilter("com.uthworks.ammhu"));
        updateChat();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            String message = intent.getStringExtra("message");
            Log.d("messageBroadcast", message);
            if (message.equals("1")){
                updateChat();
            }
        }
    };

    void getGroupChat(){
        pDialog = new ProgressDialog(GroupChatScreen.this);
        pDialog.setCancelable(false);
        pDialog.setTitle("Please wait ...");
        pDialog.setMessage("Getting previous chat ...");
        pDialog.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getgroupchatUrl + "groupid=" + groupId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        pDialog.hide();
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("chat");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    ContentValues contentValues = new ContentValues();
                                    contentValues.put(DBConstants.COLUMN_GROUP_ID, jsonObject.getString("grop_id"));
                                    contentValues.put(DBConstants.COLUMN_FRIEND_ID, jsonObject.getString("friend_id"));
                                    contentValues.put(DBConstants.COLUMN_RECIVER_ID, jsonObject.getString("reciver_id"));
                                    contentValues.put(DBConstants.COLUMN_MESSAGE, jsonObject.getString("msg"));
                                    contentValues.put(DBConstants.COLUMN_TIME, jsonObject.getString("time"));
                                    contentValues.put(DBConstants.COLUMN_STATUS, Utils.status);
                                    contentValues.put(DBConstants.COLUMN_BEEP, Utils.beep);
                                    dbOperations.addGroupChatToDB(contentValues);
                                }
                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    void updateChat(){
        cursor = dbOperations.getGroupChat(groupId);
        llMain.removeAllViews();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
//        float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        int dpWidth = displayMetrics.widthPixels;
        dpWidth = (dpWidth*80)/100;
        if (cursor != null && cursor.getCount()>0){
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                LinearLayout linearLayout = new LinearLayout(GroupChatScreen.this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(10, 10, 10, 10);
//                extractContent(getMessage.getString(getMessage.getColumnIndexOrThrow(DBConstants.COLUMN_MESSAGE)));
//                linearLayout.setElevation(10.0f);
                linearLayout.setPadding(30, 30, 30, 30);
                linearLayout.setOrientation(LinearLayout.VERTICAL);

                TextView textView ;
                TextView textViewTime = null ;

                LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
                layoutParams3.setMargins(10, 0, 10,0);
                if (cursor.getString(cursor.getColumnIndexOrThrow(DBConstants.COLUMN_FRIEND_ID)).equals(userid)) {
                    linearLayout.setBackgroundResource(R.drawable.two_eighty);
                    layoutParams.gravity = Gravity.RIGHT;
                    linearLayout.setLayoutParams(layoutParams);
                    textViewTime = new TextView(GroupChatScreen.this);
                    textViewTime.setGravity(Gravity.RIGHT);

//                            textViewTime.setTextColor(Color.parseColor("#000000"));
                    textViewTime.setTextSize(12);
                    String time = cursor.getString(cursor.getColumnIndexOrThrow(DBConstants.COLUMN_TIME));
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date strDate = null;
                    Date date2 = null;
                    try {
                        strDate = sdf.parse(time);
                        date2 = sdf.parse(sdf.format(new Date()));
                        if (date2.after(strDate)) {
                            DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            fromFormat.setLenient(false);
                            DateFormat toFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
                            toFormat.setLenient(false);
                            Date date = fromFormat.parse(time);
                            System.out.println(toFormat.format(date));
                            textViewTime.setText(toFormat.format(date));
                        }else{
                            DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            fromFormat.setLenient(false);
                            DateFormat toFormat = new SimpleDateFormat("hh:mm a");
                            toFormat.setLenient(false);
                            Date date = fromFormat.parse(time);
                            System.out.println(toFormat.format(date));
                            textViewTime.setText(toFormat.format(date));
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    textView = new TextView(GroupChatScreen.this);
                    textView.setMaxWidth(dpWidth);
                    textView.setLayoutParams(layoutParams3);
                    textView.setTextColor(Color.parseColor("#000000"));

                    textView.setText(Html.fromHtml(cursor.getString(cursor.getColumnIndexOrThrow(DBConstants.COLUMN_MESSAGE))) + "\n");
                    linearLayout.addView(textView);
                }else{
                    linearLayout.setLayoutParams(layoutParams);
                    linearLayout.setBackgroundResource(R.drawable.one);
                    textViewTime = new TextView(GroupChatScreen.this);
                    textViewTime.setGravity(Gravity.RIGHT);
                    textViewTime.setTextSize(12);
//                            textViewTime.setTextColor(Color.parseColor("#000000"));
                    String time = cursor.getString(cursor.getColumnIndexOrThrow(DBConstants.COLUMN_TIME));
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date strDate = null;
                    Date date2 = null;
                    try {
                        strDate = sdf.parse(time);
                        date2 = sdf.parse(sdf.format(new Date()));
                        if (date2.after(strDate)) {
                            DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            fromFormat.setLenient(false);
                            DateFormat toFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
                            toFormat.setLenient(false);
//                                    String dateStr = "2011-07-09";
                            Date date = fromFormat.parse(time);
                            System.out.println(toFormat.format(date));
                            textViewTime.setText(toFormat.format(date));
                        }else{
                            DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            fromFormat.setLenient(false);
                            DateFormat toFormat = new SimpleDateFormat("hh:mm a");
                            toFormat.setLenient(false);
                            Date date = fromFormat.parse(time);
                            System.out.println(toFormat.format(date));
                            textViewTime.setText(toFormat.format(date));
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    textView = new TextView(GroupChatScreen.this);
                    textView.setTextColor(Color.parseColor("#000000"));
                    textView.setMaxWidth(dpWidth);
                    textView.setLayoutParams(layoutParams3);
                    textView.setText(Html.fromHtml(cursor.getString(cursor.getColumnIndexOrThrow(DBConstants.COLUMN_MESSAGE))) + "\n");
                    linearLayout.addView(textView);

                }
                linearLayout.addView(textViewTime);
                llMain.addView(linearLayout);
            }
            sv.post(new Runnable() {
                public void run() {
                    sv.fullScroll(View.FOCUS_DOWN);
                }
            });
        }else{
            getGroupChat();
        }
    }

    public class SendMessage extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String groupId; String userId; String msg;
        public SendMessage(String groupId, String userId, String msg) {
            this.groupId =groupId;
            this.userId = userId;
            this.msg = msg;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(GroupChatScreen.this);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Please wait ...");
            progressDialog.setMessage("Sending message ...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.common(ApiUrl.sendgroupchatUrl + "user_id=" + URLEncoder.encode(userId)
                    + "&groupid=" + URLEncoder.encode(groupId) + "&msg=" + URLEncoder.encode(msg)+"&status="+ Utils.status+"&beep="+Utils.beep);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("success")) {
                etMessage.setText("");
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String formattedDate = df.format(new Date());
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBConstants.COLUMN_GROUP_ID, groupId);
                contentValues.put(DBConstants.COLUMN_FRIEND_ID, userId);
                contentValues.put(DBConstants.COLUMN_RECIVER_ID, "");
                contentValues.put(DBConstants.COLUMN_MESSAGE, msg);
                contentValues.put(DBConstants.COLUMN_TIME, formattedDate);
                contentValues.put(DBConstants.COLUMN_STATUS, Utils.status);
                contentValues.put(DBConstants.COLUMN_BEEP, Utils.beep);
                dbOperations.addGroupChatToDB(contentValues);
                updateChat();
            }else if (result.equalsIgnoreCase("fail")) {
            }
            else {
            }
        }
    }
}
