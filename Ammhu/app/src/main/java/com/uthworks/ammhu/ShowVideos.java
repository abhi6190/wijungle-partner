package com.uthworks.ammhu;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.VideoView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Model.ImageModel;
import Model.PDFModel;
import Model.VideoModel;
import RestInteraction.RESTInteraction;
import Utils.ApiUrl;
import Utils.Utils;

/**
 * Created by VARIABLE on 11/21/2016.
 */
public class ShowVideos extends AppCompatActivity {
    GridView gridVideo;
    MyAdapter myAdapter;
    Spinner spinner1;
    RESTInteraction restInteraction;
    String catId = "";
    List<String> categories1 = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_video);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Videos");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        gridVideo = (GridView)findViewById(R.id.gridVideo);
        restInteraction = RESTInteraction.getInstance(this);
        catId = getIntent().getStringExtra("catid");
        gridVideo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Dialog dialog = new Dialog(ShowVideos.this);
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog.setContentView(R.layout.video_view);
//                VideoView video_view = (VideoView)dialog.findViewById(R.id.video_view);
//                Uri uri = Uri.parse(Utils.videoModels.get(position).getVideo_link());
//                video_view.setVideoURI(uri);
//                video_view.start();
//                dialog.show();
//                startActivity(new Intent(ShowVideos.this, PlayVideo.class).putExtra("url", Utils.videoModels.get(position).getVideo_link()));
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Utils.videoModels.get(position).getVideo_link())));
            }

        });
        spinner1 = (Spinner) findViewById(R.id.spinner1);

        categories1.add("Select Category");
        for (int i = 0; i < Utils.topicModels.size(); i++) {
            categories1.add(Utils.topicModels.get(i).getTopicName());
        }

        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories1);

        // Drop down layout style - list view with radio button
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner1.setAdapter(dataAdapter1);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    new GetAllVideos(catId, "0").execute();
                }else{
                    String s = Utils.topicModels.get(position+1).getId();
                    new GetAllVideos(catId, s).execute();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public class GetAllVideos extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String catid;
        String topicId;

        public GetAllVideos(String catid, String topicId){
            this.catid = catid;
            this.topicId = topicId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ShowVideos.this);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Please wait ...");
            progressDialog.setMessage("Getting videos ...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.getAllVideos(ApiUrl.getAllVideosUrl + "cat_id=" + catid + "&topicid=" + topicId);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("success")) {
                myAdapter = new MyAdapter(ShowVideos.this, Utils.videoModels);
                gridVideo.setAdapter(myAdapter);
            }else if (result.equalsIgnoreCase("fail")) {
                myAdapter = new MyAdapter(ShowVideos.this, Utils.videoModels);
                gridVideo.setAdapter(myAdapter);
//                Toast.makeText(LaunchingActivity.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
//                Toast.makeText(LaunchingActivity.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    class MyAdapter extends BaseAdapter {
        LayoutInflater layoutInflater;
        Context context;
        ArrayList<VideoModel> items;
        public MyAdapter(Context context, ArrayList<VideoModel> imageModels){
            this.context = context;
            items = imageModels;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View mView = null;
            if (mView == null){
                mView = layoutInflater.inflate(R.layout.grid_list_items, parent, false);
            }
            ImageView ivgalleryImage = (ImageView)mView.findViewById(R.id.ivgalleryImage);
            TextView ivgalleryImageName = (TextView)mView.findViewById(R.id.ivgalleryImageName);
            String thumbnail = items.get(position).getVideo_link();
            thumbnail = thumbnail.substring(thumbnail.lastIndexOf("=")+1, thumbnail.length());
            Picasso.with(context)
                    .load("http://img.youtube.com/vi/" + thumbnail.trim()+"/1.jpg")
                    .into(ivgalleryImage);
//            UrlImageViewHelper.setUrlDrawable(ivgalleryImage, url);
//            try {
//                ivgalleryImage.setImageBitmap(retriveVideoFrameFromVideo(items.get(position).getVideo_link()));
//            } catch (Throwable throwable) {
//                throwable.printStackTrace();
//            }
            ivgalleryImageName.setText(items.get(position).getDescription());

            return mView;
        }
    }


}
