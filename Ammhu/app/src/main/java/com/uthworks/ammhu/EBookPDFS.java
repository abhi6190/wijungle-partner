package com.uthworks.ammhu;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import Model.EBookPDFModel;
import Model.EBookSubCatModel;
import Model.PDFModel;
import Utils.Utils;
import Utils.ApiUrl;
import adapter.EBookSubCatAdapter;

/**
 * Created by VARIABLE on 1/20/2017.
 */
public class EBookPDFS extends AppCompatActivity {
    ListView downloadList;
    String catId, subCatId;
    MyAdapter myAdapter;
    private DownloadManager downloadManager;
    private long downloadReference;
    ArrayList<PDFModel> pdfModels;
    String tag_json_obj = "json_obj_req";
    ProgressDialog pDialog;
    String TAG = "MainActivity";
    ArrayList<EBookPDFModel> eBookPDFModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.download_layout);
        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        registerReceiver(downloadReceiver, filter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("PDF");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        catId = getIntent().getStringExtra("catid");
        subCatId = getIntent().getStringExtra("subcatid");
        downloadList = (ListView) findViewById(R.id.downloadList);
        downloadList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                Uri Download_Uri = Uri.parse("https://ammhu.com/upload/" + eBookPDFModels.get(position).getPdf());
                DownloadManager.Request request = new DownloadManager.Request(Download_Uri);

                //Restrict the types of networks over which this download may proceed.
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                //Set whether this download may proceed over a roaming connection.
                request.setAllowedOverRoaming(false);
                //Set the title of this download, to be displayed in notifications (if enabled).
                request.setTitle("Download started");
                //Set a description of this download, to be displayed in notifications (if enabled)
//                request.setDescription("Android Data download using DownloadManager.");
                //Set the local destination for the downloaded file to a path within the application's external files directory
                request.setDestinationInExternalFilesDir(EBookPDFS.this, Environment.DIRECTORY_DOWNLOADS, eBookPDFModels.get(position).getPdf());

                //Enqueue a new download and same the referenceId
                downloadReference = downloadManager.enqueue(request);
            }
        });

        getAllEbookCat(catId, subCatId);

    }

    void getAllEbookCat(String catId, String subCatId){
        pDialog = new ProgressDialog(EBookPDFS.this);
        pDialog.setTitle("Please wait ...");
        pDialog.setMessage("Getting pdf ...");
        pDialog.setCancelable(false);
        pDialog.show();
        String url = "";
        if (Utils.isEbook){
            url = ApiUrl.getebook2URL;
        }else{
            url = ApiUrl.getebookURL;
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url+"cat_id="+catId+"&sub_cat_id="+subCatId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        pDialog.hide();
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("books");
                                 eBookPDFModels = new ArrayList<>();
                                if (jsonArray != null) {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        eBookPDFModels.add(new EBookPDFModel(jsonObject.optString("id"),
                                                jsonObject.optString("name"),
                                                jsonObject.optString("category_id"),
                                                jsonObject.optString("sub_category_id"),
                                                jsonObject.optString("pdf"),
                                                jsonObject.optString("download")));
                                    }

                                    myAdapter = new MyAdapter(EBookPDFS.this, eBookPDFModels);
                                    downloadList.setAdapter(myAdapter);
                                }


                            }else {
                                Utils.message = jsonObj.getString("message");
                                Toast.makeText(EBookPDFS.this, jsonObj.getString("message"), Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //check if the broadcast message is for our Enqueued download
            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            if(downloadReference == referenceId){
                int ch;
                ParcelFileDescriptor file;
                StringBuffer strContent = new StringBuffer("");
                StringBuffer countryData = new StringBuffer("");

                //parse the JSON data and display on the screen
                try {
                    file = downloadManager.openDownloadedFile(downloadReference);
                    FileInputStream fileInputStream
                            = new ParcelFileDescriptor.AutoCloseInputStream(file);

                    while( (ch = fileInputStream.read()) != -1)
                        strContent.append((char)ch);

//                    JSONObject responseObj = new JSONObject(strContent.toString());
//                    JSONArray countriesObj = responseObj.getJSONArray("countries");
//
//                    for (int i=0; i<countriesObj.length(); i++){
//                        Gson gson = new Gson();
//                        String countryInfo = countriesObj.getJSONObject(i).toString();
//                        Country country = gson.fromJson(countryInfo, Country.class);
//                        countryData.append(country.getCode() + ": " + country.getName() +"\n");
//                    }
//
//                    TextView showCountries = (TextView) findViewById(R.id.countryData);
//                    showCountries.setText(countryData.toString());

                    Toast toast = Toast.makeText(EBookPDFS.this,
                            "Downloading of data just finished", Toast.LENGTH_LONG);
                    toast.show();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    };

    class MyAdapter extends BaseAdapter {
        Context context;
        LayoutInflater layoutInflater;
        ArrayList<EBookPDFModel> pdfModels;
        public MyAdapter(Context context, ArrayList<EBookPDFModel> pdfModels){
            this.context = context;
            this.pdfModels = pdfModels;
            layoutInflater = LayoutInflater.from(context);
        }
        @Override
        public int getCount() {
            return pdfModels.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = null;
            if (view == null){
                view = layoutInflater.inflate(R.layout.product_item_layout, parent, false);
            }
            TextView tvPdfname = (TextView)view.findViewById(R.id.tvPdfname);
            TextView tvDate = (TextView)view.findViewById(R.id.tvDate);
            TextView tvDownloads = (TextView)view.findViewById(R.id.tvDownloads);

            tvDownloads.setText(pdfModels.get(position).getDownload());
            tvPdfname.setText(Html.fromHtml(pdfModels.get(position).getName()));
//            tvDate.setText(pdfModels.get(position).getPost_date());
            return view;
        }
    }
}
