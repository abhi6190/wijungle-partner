package com.uthworks.ammhu;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import Model.PostModel;
import Model.VideoModel;
import RestInteraction.RESTInteraction;
import Utils.Utils;

/**
 * Created by VARIABLE on 1/18/2017.
 */
public class SearchVideoScreen extends AppCompatActivity {
    GridView gridVideo;
    MyAdapter myAdapter;
    ArrayList<VideoModel> videoModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_video);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Videos");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        gridVideo = (GridView) findViewById(R.id.gridVideo);
        gridVideo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(videoModels.get(position).getVideo_link())));
            }
        });
        videoModels = (ArrayList<VideoModel>) getIntent().getSerializableExtra("video");
        myAdapter = new MyAdapter(SearchVideoScreen.this, videoModels);
        gridVideo.setAdapter(myAdapter);
    }

    class MyAdapter extends BaseAdapter {
        LayoutInflater layoutInflater;
        Context context;
        ArrayList<VideoModel> items;
        public MyAdapter(Context context, ArrayList<VideoModel> imageModels){
            this.context = context;
            items = imageModels;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View mView = null;
            if (mView == null){
                mView = layoutInflater.inflate(R.layout.grid_list_items, parent, false);
            }
            ImageView ivgalleryImage = (ImageView)mView.findViewById(R.id.ivgalleryImage);
            TextView ivgalleryImageName = (TextView)mView.findViewById(R.id.ivgalleryImageName);
            String thumbnail = items.get(position).getVideo_link();
            thumbnail = thumbnail.substring(thumbnail.lastIndexOf("=")+1, thumbnail.length());
            Picasso.with(context)
                    .load("http://img.youtube.com/vi/" + thumbnail.trim()+"/1.jpg")
                    .into(ivgalleryImage);
            ivgalleryImageName.setText(items.get(position).getDescription());

            return mView;
        }
    }
}
