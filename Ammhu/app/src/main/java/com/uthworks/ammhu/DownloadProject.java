package com.uthworks.ammhu;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import Model.PDFModel;
import RestInteraction.RESTInteraction;
import Utils.ApiUrl;
import Utils.Utils;

/**
 * Created by VARIABLE on 11/7/2016.
 */
public class DownloadProject extends AppCompatActivity {
    ListView downloadList;
    String chapterid;
    RESTInteraction restInteraction;
    MyAdapter myAdapter;
    private DownloadManager downloadManager;
    private long downloadReference;
    String userId= "";
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.download_layout);
        restInteraction = RESTInteraction.getInstance(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("PDF/DOC");
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        userId = sharedPreferences.getString("userid", "");
        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        registerReceiver(downloadReceiver, filter);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        chapterid = getIntent().getStringExtra("chapterid");
        downloadList = (ListView)findViewById(R.id.downloadList);
        downloadList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (userId.length()>0) {
                downloadManager = (DownloadManager)getSystemService(DOWNLOAD_SERVICE);
                String  filePath = "https://ammhu.com/project/"+Utils.pdfModels.get(position).getPath().replaceAll(" ", "%20");
                Uri Download_Uri = Uri.parse(filePath);
                DownloadManager.Request request = new DownloadManager.Request(Download_Uri);

                //Restrict the types of networks over which this download may proceed.
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                //Set whether this download may proceed over a roaming connection.
                request.setAllowedOverRoaming(false);
                //Set the title of this download, to be displayed in notifications (if enabled).
                request.setTitle(Utils.pdfModels.get(position).getPath());
                //Set a description of this download, to be displayed in notifications (if enabled)
//                request.setDescription("Android Data download using DownloadManager.");
                //Set the local destination for the downloaded file to a path within the application's external files directory
                request.setDestinationInExternalFilesDir(DownloadProject.this, Environment.DIRECTORY_DOWNLOADS,Utils.pdfModels.get(position).getPath());

                //Enqueue a new download and same the referenceId
                downloadReference = downloadManager.enqueue(request);
                }else{
                    startActivity(new Intent(DownloadProject.this, LaunchingActivity.class));
                }
            }
        });
        new GetAllPDF(chapterid).execute();
    }

    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //check if the broadcast message is for our Enqueued download
            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            if(downloadReference == referenceId){
                int ch;
                ParcelFileDescriptor file;
                StringBuffer strContent = new StringBuffer("");
                StringBuffer countryData = new StringBuffer("");

                //parse the JSON data and display on the screen
                try {
                    file = downloadManager.openDownloadedFile(downloadReference);
                    FileInputStream fileInputStream
                            = new ParcelFileDescriptor.AutoCloseInputStream(file);

                    while( (ch = fileInputStream.read()) != -1)
                        strContent.append((char)ch);


                    Toast toast = Toast.makeText(DownloadProject.this,
                            "Downloading finished", Toast.LENGTH_LONG);
                    toast.show();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    };

    public class GetAllPDF extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String catid;
        String topicid;

        public GetAllPDF(String catid){
            this.catid = catid;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(DownloadProject.this);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Please wait ...");
            progressDialog.setMessage("Getting all files ...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.getAllPDF(ApiUrl.getAllProjectsUrl + "chapter_id=" + catid);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("success")) {
                myAdapter = new MyAdapter(DownloadProject.this, Utils.pdfModels);
                downloadList.setAdapter(myAdapter);
            }else if (result.equalsIgnoreCase("fail")) {
                Toast.makeText(DownloadProject.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
//                Toast.makeText(LaunchingActivity.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    class MyAdapter extends BaseAdapter {
        Context context;
        LayoutInflater layoutInflater;
        ArrayList<PDFModel> pdfModels;
        public MyAdapter(Context context, ArrayList<PDFModel> pdfModels){
            this.context = context;
            this.pdfModels = pdfModels;
            layoutInflater = LayoutInflater.from(context);
        }
        @Override
        public int getCount() {
            return pdfModels.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = null;
            if (view == null){
                view = layoutInflater.inflate(R.layout.product_item_layout, parent, false);
            }
            TextView tvPdfname = (TextView)view.findViewById(R.id.tvPdfname);
            TextView tvDate = (TextView)view.findViewById(R.id.tvDate);
            TextView tvDownloads = (TextView)view.findViewById(R.id.tvDownloads);

            tvDownloads.setText(pdfModels.get(position).getDownloads());
            tvPdfname.setText(Html.fromHtml(pdfModels.get(position).getTitle()));
            tvDate.setText(pdfModels.get(position).getPost_date());
            return view;
        }
    }
}
