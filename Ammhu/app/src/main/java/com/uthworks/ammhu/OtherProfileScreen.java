package com.uthworks.ammhu;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;

import Model.FollowerModel;
import Model.FollowingModel;
import Model.FriendsModel;
import Model.PostModel;
import Model.UserModel;
import RestInteraction.RESTInteraction;
import Utils.ApiUrl;
import Utils.Utils;
import adapter.FullMenuAdapter;

/**
 * Created by VARIABLE on 1/22/2017.
 */
public class OtherProfileScreen extends AppCompatActivity {

    RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;
    ImageView ivUsrPic;
    TextView tvName, tvDOB, tvFriendsCount, tvFollowingCount, tvfollowersCount;
    RESTInteraction restInteraction;
    //    int pos = -1;
    Button btnAddFriend;
    SharedPreferences sharedPreferences;
    String tag_json_obj = "json_obj_req";
    ProgressDialog pDialog;
    String TAG = "MainActivity";
    String userId, otherUserId;
    ArrayList<FollowingModel> followingModels;
    ArrayList<FollowerModel> followerModels;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Profile");
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        userId = sharedPreferences.getString("userid", "");
        otherUserId = getIntent().getStringExtra("id");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnAddFriend = (Button)findViewById(R.id.btnAddFriend);
        ivUsrPic = (ImageView)findViewById(R.id.ivUsrPic);
        tvName = (TextView)findViewById(R.id.tvName);
        tvDOB = (TextView)findViewById(R.id.tvDOB);
        tvFriendsCount = (TextView)findViewById(R.id.tvFriendsCount);
        tvfollowersCount = (TextView)findViewById(R.id.tvfollowersCount);
        tvFollowingCount = (TextView)findViewById(R.id.tvFollowingCount);
        restInteraction = RESTInteraction.getInstance(this);
        mRecyclerView = (RecyclerView)findViewById(R.id.recyclerView_listing);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        btnAddFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.getString("email", "").length() > 0) {
                    new SendFriendRequest(sharedPreferences.getString("userid", ""), otherUserId).execute();
                } else {
                    startActivity(new Intent(OtherProfileScreen.this, LaunchingActivity.class));
                    finish();
                }
            }
        });

        tvFriendsCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OtherProfileScreen.this, FriendsScreen.class));
            }
        });
        tvfollowersCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OtherProfileScreen.this, FollowersScreen.class).putExtra("followers", followerModels));
            }
        });
        tvFollowingCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OtherProfileScreen.this, FollowingScreen.class).putExtra("following", followingModels));
            }
        });

        getUserDetail();
    }

    void getUserDetail(){
        pDialog = new ProgressDialog(OtherProfileScreen.this);
        pDialog.setTitle("Please wait ...");
        pDialog.setMessage("Get user data");
        pDialog.setCancelable(false);
        pDialog.show();
//       String url = "http://elogic.southindia.cloudapp.azure.com:8083/transtruck/admindashboard/tables/get/childadminshowList.php?pageno=1&limit=10";
        Log.d("ur", ApiUrl.getuserbyidUrl+"id=" + otherUserId);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getuserbyidUrl+"id=" + otherUserId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        pDialog.hide();
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("users");
                                tvName.setText(jsonArray.optJSONObject(0).optString("name") + " "+ jsonArray.optJSONObject(0).optString("sname"));
                                tvDOB.setText(jsonArray.optJSONObject(0).optString("dd")+"-"+jsonArray.optJSONObject(0).optString("mm")+"-"+jsonArray.optJSONObject(0).optString("yyyy"));
                                if (jsonArray.optJSONObject(0).optString("profile_image").length()>0){
                                    String url = "https://ammhu.com/images/"+jsonArray.optJSONObject(0).optString("profile_image");
                                    url=url.replaceAll(" ", "%20");
                                    Picasso.with(OtherProfileScreen.this)
                                            .load(url)
                                            .placeholder(R.mipmap.photo)
                                            .into(ivUsrPic);
                                }

                                getUserPosts();
                                getFriends();
                                getFollowers();
                                getFollowing();
                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    void getUserPosts(){
//       String url = "http://elogic.southindia.cloudapp.azure.com:8083/transtruck/admindashboard/tables/get/childadminshowList.php?pageno=1&limit=10";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getUserPostUrl+"user_id=" + otherUserId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
//                        pDialog.hide();
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("posts");
                                Utils.postModels.clear();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    Utils.postModels.add(new PostModel(jsonObject.optString("id"),
                                            jsonObject.optString("user_id"),
                                            jsonObject.optString("post_title"),
                                            jsonObject.optString("post_des"),
                                            jsonObject.optString("topic_id"),
                                            jsonObject.optString("dd"),
                                            jsonObject.optString("feature_img"),
                                            jsonObject.optString("slug"),
                                            jsonObject.optString("answer_request"),
                                            jsonObject.optString("count")));
                                }

                                mAdapter = new FullMenuAdapter(OtherProfileScreen.this, Utils.postModels);
                                mRecyclerView.setAdapter(mAdapter);

//					Collections.sort(Utils.postModels);
                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
//                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    void getFriends(){
//       String url = "http://elogic.southindia.cloudapp.azure.com:8083/transtruck/admindashboard/tables/get/childadminshowList.php?pageno=1&limit=10";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getFriendsUrl + "reciver_id=" + otherUserId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
//                        pDialog.hide();
                        try {
                            Utils.friendsModels.clear();
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("friends");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    Utils.friendsModels.add(new FriendsModel(jsonObject.optString("id"),
                                            jsonObject.optString("sender_id"),
                                            jsonObject.optString("reciver_id"),
                                            jsonObject.optString("name"),
                                            jsonObject.optString("image")));
                                }
//                                mAdapter = new FriendsAdapter(OtherProfileScreen.this, Utils.friendsModels);
//                                mRecyclerView.setAdapter(mAdapter);
                                tvFriendsCount.setText(Utils.friendsModels.size()+"");

                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
//                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    void getFollowers(){
//       String url = "http://elogic.southindia.cloudapp.azure.com:8083/transtruck/admindashboard/tables/get/childadminshowList.php?pageno=1&limit=10";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getFollowersUrl + "userid=" + otherUserId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
//                        pDialog.hide();
                        try {
                            followerModels = new ArrayList<>();
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("followers");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    followerModels.add(new FollowerModel(jsonObject.optString("id"),
                                            jsonObject.optString("user_id"),
                                            jsonObject.optString("followers_id"),
                                            jsonObject.optString("name"),
                                            jsonObject.optString("image")));
                                }
//                                mAdapter = new FriendsAdapter(OtherProfileScreen.this, Utils.friendsModels);
//                                mRecyclerView.setAdapter(mAdapter);
                                tvfollowersCount.setText(followerModels.size()+"");

                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
//                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }


    void getFollowing(){
//       String url = "http://elogic.southindia.cloudapp.azure.com:8083/transtruck/admindashboard/tables/get/childadminshowList.php?pageno=1&limit=10";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getFollowingUrl + "userid=" + otherUserId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
//                        pDialog.hide();
                        try {
                           followingModels = new ArrayList<>();
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("following");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    followingModels.add(new FollowingModel(jsonObject.optString("id"),
                                            jsonObject.optString("user_id"),
                                            jsonObject.optString("followers_id"),
                                            jsonObject.optString("name"),
                                            jsonObject.optString("image")));
                                }
//                                mAdapter = new FriendsAdapter(OtherProfileScreen.this, Utils.friendsModels);
//                                mRecyclerView.setAdapter(mAdapter);
                                tvFollowingCount.setText(followingModels.size()+"");

                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
//                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public class SendFriendRequest extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String fromId; String toId;
        public SendFriendRequest(String fromId, String toId) {
            this.fromId =fromId;
            this.toId = toId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(OtherProfileScreen.this);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Please wait ...");
            progressDialog.setMessage("Sending request ...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.common(ApiUrl.sendFriendRequestUrl + "sender_id=" + URLEncoder.encode(fromId)
                    + "&reciver_id=" + URLEncoder.encode(toId) + "&status=" + "0");
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("success")) {
                Toast.makeText(OtherProfileScreen.this, "Your request has been sent to user.", Toast.LENGTH_SHORT).show();
            }else if (result.equalsIgnoreCase("fail")) {
                Toast.makeText(OtherProfileScreen.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(OtherProfileScreen.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public class GetAllPosts extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String catid;
        public GetAllPosts(String catid){
            this.catid = catid;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setCancelable(false);
//            progressDialog.setTitle("Please wait ...");
//            progressDialog.setMessage("Getting posts ...");
//            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.getAllPosts(ApiUrl.getUserPostUrl+"user_id="+catid);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//            progressDialog.dismiss();
            if (result.equals("success")) {

                mAdapter = new FullMenuAdapter(OtherProfileScreen.this, Utils.postModels);
                mRecyclerView.setAdapter(mAdapter);
            }else if (result.equalsIgnoreCase("fail")) {
//                Toast.makeText(LaunchingActivity.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
//                Toast.makeText(LaunchingActivity.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
