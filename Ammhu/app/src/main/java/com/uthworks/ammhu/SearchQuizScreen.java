package com.uthworks.ammhu;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import Model.PostModel;
import Model.QuizModel;
import Utils.Utils;

/**
 * Created by VARIABLE on 1/17/2017.
 */
public class SearchQuizScreen extends AppCompatActivity {
    ListView listView;
    MyAdapter myAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_quiz);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Quiz");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        listView = (ListView)findViewById(R.id.listViewOnlineRes);
        ArrayList<QuizModel> quizModels = (ArrayList<QuizModel>) getIntent().getSerializableExtra("quiz");
        myAdapter = new MyAdapter(this, quizModels);
        listView.setAdapter(myAdapter);
    }

    class MyAdapter extends BaseAdapter {
        Context context;
        LayoutInflater layoutInflater;
        ArrayList<QuizModel> pdfModels;
        public MyAdapter(Context context, ArrayList<QuizModel> pdfModels){
            this.context = context;
            this.pdfModels = pdfModels;
            layoutInflater = LayoutInflater.from(context);
        }
        @Override
        public int getCount() {
            return pdfModels.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = null;
            if (view == null){
                view = layoutInflater.inflate(R.layout.quiz_list_items, parent, false);
            }
            TextView tvQuestion = (TextView)view.findViewById(R.id.tvQuestion);
            TextView tvDate = (TextView)view.findViewById(R.id.tvDate);
            final TextView tvViewAns = (TextView)view.findViewById(R.id.tvViewAns);
            final TextView tvAnswer = (TextView)view.findViewById(R.id.tvAnswer);
            final View view1 = (View)view.findViewById(R.id.tvAnswer);
            tvViewAns.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (v.getTag().toString().equals("1")){
                        view1.setVisibility(View.VISIBLE);
                        tvAnswer.setVisibility(View.VISIBLE);
                        tvViewAns.setTag("2");
                    }else{
                        view1.setVisibility(View.GONE);
                        tvAnswer.setVisibility(View.GONE);
                        tvViewAns.setTag("1");
                    }
                }
            });
//            tvViewAns.setText(pdfModels.get(position).getDownloads());
            tvQuestion.setText("Q : "+ Html.fromHtml(pdfModels.get(position).getQuestion()));
            tvDate.setText(pdfModels.get(position).getPost_date());
            tvAnswer.setText("Ans : "+Html.fromHtml(pdfModels.get(position).getAnswer()));
            return view;
        }
    }
}
