package com.uthworks.ammhu;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import DB.DBConstants;
import DB.DBOperations;
import RestInteraction.RESTInteraction;
import Utils.ApiUrl;
import Utils.Utils;

/**
 * Created by VARIABLE on 11/26/2016.
 */
public class ChatScreen extends AppCompatActivity {
    ScrollView sv;
    LinearLayout llMain;
    EditText etMessage;
    ImageView ivSendMessage;
    RESTInteraction restInteraction;
    DBOperations dbOperations;
    SharedPreferences sharedPreferences;
    String userid, toId;
    Cursor cursor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Chat");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        sv = (ScrollView)findViewById(R.id.sv);
        restInteraction = RESTInteraction.getInstance(this);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        dbOperations = new DBOperations(this);
        userid = sharedPreferences.getString("userid", "");
        toId = getIntent().getStringExtra("toid");
        llMain = (LinearLayout)findViewById(R.id.llMain);
        etMessage = (EditText)findViewById(R.id.etMessage);
        ivSendMessage = (ImageView)findViewById(R.id.ivSendMessage);
        ivSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etMessage.getText().toString().length()>0) {
                    new SendMessage(userid, toId, etMessage.getText().toString()).execute();
                }else{
                    Toast.makeText(ChatScreen.this, "Please enter message.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        ChatScreen.this.registerReceiver(mMessageReceiver, new IntentFilter("com.uthworks.ammhu"));
        updateChat();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            String message = intent.getStringExtra("message");
            Log.d("messageBroadcast", message);
            if (message.equals("1")){
                    updateChat();
            }
        }
    };

    public class GetAllChatAsync extends AsyncTask<String, Void, String> {
        String toId;
        ProgressDialog progressDialog;

        public GetAllChatAsync( String toId) {
            this.toId = toId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ChatScreen.this);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Please wait ...");
            progressDialog.setMessage("Getting previous chat ...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.getChat(ApiUrl.getChatUrl + "user_id=" + userid + "&friend_id=" + toId);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result.equalsIgnoreCase("success")) {
                updateChat();

            }else if(result.equalsIgnoreCase("fail"))
            {
//                if (progressDialog != null & progressDialog.isShowing()){
//                    progressDialog.dismiss();
//
            }
            else
            {
//                if (progressDialog != null & progressDialog.isShowing()){
//                    progressDialog.dismiss();
//                }
                Toast.makeText(ChatScreen.this, "Oops ...We are unable to reach our server right now. Try again later!!!", Toast.LENGTH_SHORT).show();

            }
        }
    }

    void updateChat(){
        cursor = dbOperations.getChat(userid, toId);
        llMain.removeAllViews();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
//        float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        int dpWidth = displayMetrics.widthPixels;
        dpWidth = (dpWidth*80)/100;
        if (cursor != null && cursor.getCount()>0){
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                LinearLayout linearLayout = new LinearLayout(ChatScreen.this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(10, 10, 10, 10);
//                extractContent(getMessage.getString(getMessage.getColumnIndexOrThrow(DBConstants.COLUMN_MESSAGE)));
//                linearLayout.setElevation(10.0f);
                linearLayout.setPadding(30, 30, 30, 30);
                linearLayout.setOrientation(LinearLayout.VERTICAL);

                TextView textView ;
                TextView textViewTime = null ;

                LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
                layoutParams3.setMargins(10, 0, 10,0);
                if (cursor.getString(cursor.getColumnIndexOrThrow(DBConstants.COLUMN_SENDER_ID)).equals(userid)) {
                    linearLayout.setBackgroundResource(R.drawable.two_eighty);
                    layoutParams.gravity = Gravity.RIGHT;
                    linearLayout.setLayoutParams(layoutParams);
                    textViewTime = new TextView(ChatScreen.this);
                    textViewTime.setGravity(Gravity.RIGHT);

//                            textViewTime.setTextColor(Color.parseColor("#000000"));
                    textViewTime.setTextSize(12);
                    String time = cursor.getString(cursor.getColumnIndexOrThrow(DBConstants.COLUMN_TIME));
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date strDate = null;
                    Date date2 = null;
                    try {
                        strDate = sdf.parse(time);
                        date2 = sdf.parse(sdf.format(new Date()));
                        if (date2.after(strDate)) {
                            DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            fromFormat.setLenient(false);
                            DateFormat toFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
                            toFormat.setLenient(false);
                            Date date = fromFormat.parse(time);
                            System.out.println(toFormat.format(date));
                            textViewTime.setText(toFormat.format(date));
                        }else{
                            DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            fromFormat.setLenient(false);
                            DateFormat toFormat = new SimpleDateFormat("hh:mm a");
                            toFormat.setLenient(false);
                            Date date = fromFormat.parse(time);
                            System.out.println(toFormat.format(date));
                            textViewTime.setText(toFormat.format(date));
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    textView = new TextView(ChatScreen.this);
                    textView.setMaxWidth(dpWidth);
                    textView.setLayoutParams(layoutParams3);
                    textView.setTextColor(Color.parseColor("#000000"));

                    textView.setText(Html.fromHtml(cursor.getString(cursor.getColumnIndexOrThrow(DBConstants.COLUMN_MESSAGE))) + "\n");
                    linearLayout.addView(textView);
                }else{
                    linearLayout.setLayoutParams(layoutParams);
                    linearLayout.setBackgroundResource(R.drawable.one);
                    textViewTime = new TextView(ChatScreen.this);
                    textViewTime.setGravity(Gravity.RIGHT);
                    textViewTime.setTextSize(12);
//                            textViewTime.setTextColor(Color.parseColor("#000000"));
                    String time = cursor.getString(cursor.getColumnIndexOrThrow(DBConstants.COLUMN_TIME));
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date strDate = null;
                    Date date2 = null;
                    try {
                        strDate = sdf.parse(time);
                        date2 = sdf.parse(sdf.format(new Date()));
                        if (date2.after(strDate)) {
                            DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            fromFormat.setLenient(false);
                            DateFormat toFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
                            toFormat.setLenient(false);
//                                    String dateStr = "2011-07-09";
                            Date date = fromFormat.parse(time);
                            System.out.println(toFormat.format(date));
                            textViewTime.setText(toFormat.format(date));
                        }else{
                            DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            fromFormat.setLenient(false);
                            DateFormat toFormat = new SimpleDateFormat("hh:mm a");
                            toFormat.setLenient(false);
                            Date date = fromFormat.parse(time);
                            System.out.println(toFormat.format(date));
                            textViewTime.setText(toFormat.format(date));
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    textView = new TextView(ChatScreen.this);
                    textView.setTextColor(Color.parseColor("#000000"));
                    textView.setMaxWidth(dpWidth);
                    textView.setLayoutParams(layoutParams3);
                    textView.setText(Html.fromHtml(cursor.getString(cursor.getColumnIndexOrThrow(DBConstants.COLUMN_MESSAGE))) + "\n");
                    linearLayout.addView(textView);

                }
                linearLayout.addView(textViewTime);
                llMain.addView(linearLayout);
            }
            sv.post(new Runnable() {
                public void run() {
                    sv.fullScroll(View.FOCUS_DOWN);
                }
            });
        }else{
            new GetAllChatAsync(toId).execute();
        }
    }

    public class SendMessage extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String fromId; String toId; String msg;
        public SendMessage(String fromId, String toId, String msg) {
            this.fromId =fromId;
            this.toId = toId;
            this.msg = msg;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ChatScreen.this);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Please wait ...");
            progressDialog.setMessage("Sending message ...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.common(ApiUrl.sendChatUrl + "user_id=" + URLEncoder.encode(fromId)
                    + "&friend_id=" + URLEncoder.encode(toId) + "&msg=" + URLEncoder.encode(msg)+"&status="+Utils.status+"&beep="+Utils.beep);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("success")) {
                etMessage.setText("");
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String formattedDate = df.format(new Date());
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBConstants.COLUMN_SENDER_ID, fromId);
                contentValues.put(DBConstants.COLUMN_RECEIVER_ID, toId);
                contentValues.put(DBConstants.COLUMN_MESSAGE, msg);
                contentValues.put(DBConstants.COLUMN_TIME, formattedDate);
                contentValues.put(DBConstants.COLUMN_STATUS, Utils.status);
                contentValues.put(DBConstants.COLUMN_BEEP, Utils.beep);
                dbOperations.addChatToDB(contentValues);
                updateChat();
            }else if (result.equalsIgnoreCase("fail")) {
            }
            else {
            }
        }
    }
}
