package com.uthworks.ammhu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;

import Model.EntranceExamTopicModel;
import Model.ImageModel;
import Model.PDFModel;
import Model.PostModel;
import Model.QuizModel;
import Model.UserModel;
import Model.VideoModel;
import Utils.ApiUrl;
import Utils.Utils;

/**
 * Created by VARIABLE on 1/8/2017.
 */
public class SearchAllScreen extends AppCompatActivity {
    EditText etSearchContent;
    Button btnSearch;
    String tag_json_obj = "json_obj_req";
    ProgressDialog pDialog;
    Toolbar toolbar;
    String TAG = "MainActivity";
    SharedPreferences sharedPreferences;
    String  userId;
    ArrayList<PostModel> postModels;
    ArrayList<QuizModel> quizModels;
    ArrayList<PDFModel> pdfModels;
    ArrayList<ImageModel> imageModels;
    ArrayList<VideoModel> videoModels;
    ArrayList<UserModel> userModels;
    TextView tvQuestCount, tvQuizCount, tvPdfCount, tvImagesCount, tvVideoCount, tvUsersCount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_search);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Profile");
        tvQuestCount = (TextView)findViewById(R.id.tvQuestCount);
        tvQuizCount = (TextView)findViewById(R.id.tvQuizCount);
        tvPdfCount = (TextView)findViewById(R.id.tvPdfCount);
        tvImagesCount = (TextView)findViewById(R.id.tvImagesCount);
        tvVideoCount = (TextView)findViewById(R.id.tvVideoCount);
        tvUsersCount = (TextView)findViewById(R.id.tvUsersCount);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        userId = sharedPreferences.getString("userid", "");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        etSearchContent = (EditText)findViewById(R.id.etSearchContent);
        btnSearch = (Button)findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etSearchContent.getText().toString().length()>0) {
                    hideKeyboard();
                    getAllSearch(etSearchContent.getText().toString());
                }else{
                    Toast.makeText(SearchAllScreen.this, "Enter search content", Toast.LENGTH_SHORT).show();
                }
            }
        });

        tvQuestCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SearchAllScreen.this, PostScreen.class).putExtra("posts", postModels));
            }
        });
        tvQuizCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SearchAllScreen.this, SearchQuizScreen.class).putExtra("quiz", quizModels));
            }
        });
        tvPdfCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SearchAllScreen.this, SearchPDFScreen.class).putExtra("pdf", pdfModels));
            }
        });
        tvVideoCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SearchAllScreen.this, SearchVideoScreen.class).putExtra("video", videoModels));
            }
        });
        tvImagesCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SearchAllScreen.this, SearchImage.class).putExtra("image", imageModels));
            }
        });
        tvUsersCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SearchAllScreen.this, SearchUserScreen.class).putExtra("users", userModels));
            }
        });
    }

    void hideKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    void getAllSearch(String searchContent){
        pDialog = new ProgressDialog(SearchAllScreen.this);
        pDialog.setTitle("Please wait ...");
        pDialog.setMessage("Searching data ...");
        pDialog.setCancelable(false);
        pDialog.show();
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.searchmainUrl+"search_content="+ URLEncoder.encode(searchContent), null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        pDialog.hide();
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                 postModels = new ArrayList<>();
                                 quizModels = new ArrayList<>();
                                imageModels = new ArrayList<>();
                                 pdfModels = new ArrayList<>();
                               videoModels = new ArrayList<>();
                                 userModels = new ArrayList<>();
                                JSONArray jsonArray = jsonObj.optJSONArray("posts");
                                if (jsonArray != null) {

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        postModels.add(new PostModel(jsonObject.optString("id"),
                                                jsonObject.optString("user_id"),
                                                jsonObject.optString("post_title"),
                                                jsonObject.optString("post_des"),
                                                jsonObject.optString("topic_id"),
                                                jsonObject.optString("dd"),
                                                jsonObject.optString("feature_img"),
                                                jsonObject.optString("slug"),
                                                jsonObject.optString("answer_request"),
                                                jsonObject.optString("count")));
                                    }
                                }

                                JSONArray jsonArray1 = jsonObj.optJSONArray("quiz");
                                if (jsonArray1 != null) {
                                    for (int i = 0; i < jsonArray1.length(); i++) {
                                        JSONObject jsonObject = jsonArray1.getJSONObject(i);
                                        quizModels.add(new QuizModel(jsonObject.optString("id"),
                                                jsonObject.optString("post_date"),
                                                jsonObject.optString("category_id"),
                                                jsonObject.optString("user_id"),
                                                jsonObject.optString("topic_id"),
                                                jsonObject.optString("quiz_type_id"),
                                                jsonObject.optString("question"),
                                                jsonObject.optString("answer"),
                                                jsonObject.optString("feature_img")));
                                    }
                                }

                                JSONArray jsonArray2 = jsonObj.optJSONArray("images");
                                if (jsonArray2 != null) {
                                    for (int i = 0; i < jsonArray2.length(); i++) {
                                        JSONObject jsonObject = jsonArray2.getJSONObject(i);
                                        imageModels.add(new ImageModel(jsonObject.optString("id"),
                                                jsonObject.optString("user_id"),
                                                jsonObject.optString("cat_id"),
                                                jsonObject.optString("topic_id"),
                                                jsonObject.optString("title"),
                                                jsonObject.optString("src"),
                                                jsonObject.optString("comments_count")));
                                    }
                                }


                                JSONArray jsonArray3 = jsonObj.optJSONArray("pdf");
                                if (jsonArray3 != null) {
                                    for (int i = 0; i < jsonArray3.length(); i++) {
                                        JSONObject jsonObject = jsonArray3.getJSONObject(i);
                                        pdfModels.add(new PDFModel(jsonObject.optString("id"),
                                                jsonObject.optString("class_id"),
                                                jsonObject.optString("user_id"),
                                                jsonObject.optString("chapter_id"),
                                                jsonObject.optString("title"),
                                                jsonObject.optString("path"),
                                                jsonObject.optString("post_date"),
                                                jsonObject.optString("downloads"),
                                                jsonObject.optString("category_id")));
                                    }
                                }

                                JSONArray jsonArray4 = jsonObj.optJSONArray("video");
                                if (jsonArray4 != null) {
                                    for (int i = 0; i < jsonArray4.length(); i++) {
                                        JSONObject jsonObject = jsonArray4.getJSONObject(i);
                                        videoModels.add(new VideoModel(jsonObject.optString("id"),
                                                jsonObject.optString("user_id"),
                                                jsonObject.optString("cat_id"),
                                                jsonObject.optString("topic_id"),
                                                jsonObject.optString("video_link"),
                                                jsonObject.optString("description"),
                                                jsonObject.optString("status")));
                                    }
                                }

                                JSONArray jsonArray5 = jsonObj.optJSONArray("users");
                                if (jsonArray5 != null) {
                                    for (int i = 0; i < jsonArray5.length(); i++) {
                                        JSONObject jsonObject = jsonArray5.getJSONObject(i);
                                        userModels.add(new UserModel(jsonObject.optString("id"),
                                                jsonObject.optString("name"),
                                                jsonObject.optString("sname"),
                                                jsonObject.optString("email"),
                                                jsonObject.optString("dd"),
                                                jsonObject.optString("mm"),
                                                jsonObject.optString("yyyy"),
                                                jsonObject.optString("gender"),
                                                jsonObject.optString("address"),
                                                jsonObject.optString("phone"),
                                                jsonObject.optString("profile_image"),
                                                jsonObject.optString("country"),
                                                jsonObject.optString("state"),
                                                jsonObject.optString("city")));
                                    }
                                }

                                tvQuestCount.setText(postModels.size()+"");
                                tvQuizCount.setText(quizModels.size()+"");
                                tvImagesCount.setText(imageModels.size()+"");
                                tvPdfCount.setText(pdfModels.size()+"");
                                tvUsersCount.setText(userModels.size()+"");
                                tvVideoCount.setText(videoModels.size()+"");
                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }
}
