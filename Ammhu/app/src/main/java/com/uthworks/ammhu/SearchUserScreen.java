package com.uthworks.ammhu;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;

import Model.QuizModel;
import Model.UserModel;
import RestInteraction.RESTInteraction;
import Utils.Utils;
import adapter.FriendsAdapter;
import adapter.UserAdapter;

/**
 * Created by VARIABLE on 1/18/2017.
 */
public class SearchUserScreen extends AppCompatActivity {
    RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;
    String userId;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.firend_request_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Users");
        setSupportActionBar(toolbar);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        userId = sharedPreferences.getString("userid", "");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_listing);
        mRecyclerView.setHasFixedSize(true);
//        llBottom = (LinearLayout)view.findViewById(R.id.llBottom);
//        llBottom.setVisibility(View.VISIBLE);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        ArrayList<UserModel> userModels = (ArrayList<UserModel>) getIntent().getSerializableExtra("users");
        mAdapter = new UserAdapter(SearchUserScreen.this, userModels);
        mRecyclerView.setAdapter(mAdapter);
    }
}
