package com.uthworks.ammhu;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import RestInteraction.RESTInteraction;
import Utils.ApiUrl;
import Utils.Utils;
import adapter.FullMenuAdapter;
import adapter.PostAdapter;

/**
 * Created by VARIABLE on 11/26/2016.
 */
public class FFlowScreen extends AppCompatActivity {
    Spinner spinner1;
    RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;
    RESTInteraction restInteraction;
    List<String> categories = new ArrayList<String>();
    int pageNumber = 0;
    boolean flag = false;
    Button btnShowMore;
    String topicId = "0";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fflow_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Posts");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        spinner1 = (Spinner)findViewById(R.id.spinner1);
        btnShowMore = (Button)findViewById(R.id.btnShowMore);
        btnShowMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageNumber = pageNumber+5;
                new GetAllPosts(pageNumber+"", topicId).execute();
            }
        });
        for (int i = 0; i < Utils.topicModels.size(); i++) {
            categories.add(Utils.topicModels.get(i).getTopicName());
        }
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner1.setAdapter(dataAdapter1);

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Utils.urcId = Utils.urcModels.get(position).getId();
//                new GetAllQuiz(Utils.urcModels.get(position).getId()).execute();
                Utils.fFlowModels.clear();
                pageNumber = 0;
                if (flag == false){
                    new GetAllPosts(pageNumber+"", topicId).execute();
                }else{
                    topicId = Utils.topicModels.get(position).getId();
                    new GetAllPosts(pageNumber+"", Utils.topicModels.get(position).getId()).execute();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        restInteraction = RESTInteraction.getInstance(this);
        mRecyclerView = (RecyclerView)findViewById(R.id.recyclerView_listing);
        mRecyclerView.setHasFixedSize(true);
//        llBottom = (LinearLayout)view.findViewById(R.id.llBottom);
//        llBottom.setVisibility(View.VISIBLE);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);


    }

    public class GetAllPosts extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String catid;
        String topicId;
        public GetAllPosts(String catid, String topicId){
            this.catid = catid;
            this.topicId = topicId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(FFlowScreen.this);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Please wait ...");
            progressDialog.setMessage("Getting posts ...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.getFFlowPosts(ApiUrl.getFFlowPostUrl + "count=" + catid + "&topic_id="+topicId);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("success")) {
                flag = true;
                mAdapter = new PostAdapter(FFlowScreen.this, Utils.fFlowModels);
                mRecyclerView.setAdapter(mAdapter);
            }else if (result.equalsIgnoreCase("fail")) {
//                Toast.makeText(LaunchingActivity.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
//                Toast.makeText(LaunchingActivity.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
