package com.uthworks.ammhu;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Model.GroupModel;
import Model.MemberModel;
import Utils.ApiUrl;
import Utils.Utils;
import adapter.GroupAdapter;
import adapter.MemberAdapter;

/**
 * Created by VARIABLE on 3/9/2017.
 */
public class GroupMemberScreen extends AppCompatActivity{
    static RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    static RecyclerView.Adapter mAdapter;
    static String tag_json_obj = "json_obj_req";
    ProgressDialog pDialog;
    String TAG = "MainActivity";
    SharedPreferences sharedPreferences;
    String userId;
    Button btnCreateGrp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.creategroup_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Group Participants");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        userId = sharedPreferences.getString("userid", "");
        mRecyclerView = (RecyclerView)findViewById(R.id.recyclerView_listing);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(GroupMemberScreen.this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        btnCreateGrp = (Button)findViewById(R.id.btnCreateGrp);
        btnCreateGrp.setText("Add Participants");
        btnCreateGrp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startActivity(new Intent(GroupMemberScreen.this, AddPaticipantsScreen.class).putExtra("groupid", getIntent().getStringExtra("groupid")));
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        getAllGroups(getIntent().getStringExtra("groupid"));
    }
    void getAllGroups(String catId){
        pDialog = new ProgressDialog(GroupMemberScreen.this);
        pDialog.setCancelable(false);
        pDialog.setTitle("Please wait ...");
        pDialog.setMessage("Getting members ...");
        pDialog.show();
        Log.d("url", ApiUrl.getgroupmemberUrl + "groupid=" + catId+"&groupowner="+userId);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getgroupmemberUrl + "groupid=" + catId+"&groupowner="+userId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        pDialog.hide();
                        try {
                            ArrayList<MemberModel> memberModels = new ArrayList<>();
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("members");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    memberModels.add(new MemberModel(jsonObject.optString("id"),
                                            jsonObject.optString("group_id"),
                                            jsonObject.optString("user_id"),
                                            jsonObject.optString("group_owner"),
                                            jsonObject.optString("date"),
                                            jsonObject.optString("name"),
                                            jsonObject.optString("image")));
                                }

                                mAdapter = new MemberAdapter(GroupMemberScreen.this, memberModels);
                                mRecyclerView.setAdapter(mAdapter);

                            }else {
                                mAdapter = new MemberAdapter(GroupMemberScreen.this, memberModels);
                                mRecyclerView.setAdapter(mAdapter);
                                Utils.message = jsonObj.getString("message");
                                Toast.makeText(GroupMemberScreen.this, "No members found", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // hide the progress dialog
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }
}
