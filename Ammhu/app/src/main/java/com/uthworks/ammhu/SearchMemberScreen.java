package com.uthworks.ammhu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;

import Model.UserModel;
import Utils.DelayAutoCompleteTextView;
import Utils.Utils;
import adapter.BookAutoCompleteAdapter;

/**
 * Created by VARIABLE on 11/24/2016.
 */
public class SearchMemberScreen extends AppCompatActivity {

    DelayAutoCompleteTextView bookTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_member_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Search Member");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
         bookTitle = (DelayAutoCompleteTextView) findViewById(R.id.et_book_title);
        bookTitle.setThreshold(2);
        bookTitle.setAdapter(new BookAutoCompleteAdapter(this)); // 'this' is Activity instance
        bookTitle.setLoadingIndicator(
                (android.widget.ProgressBar) findViewById(R.id.pb_loading_indicator));
        bookTitle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                UserModel book = (UserModel) adapterView.getItemAtPosition(position);
                bookTitle.setText(book.getName());
                bookTitle.setSelection(bookTitle.getText().toString().length());
                Utils.isFriends = false;
                startActivity(new Intent(SearchMemberScreen.this, ProfileScreen.class).putExtra("users", book));
            }
        });

    }
}
