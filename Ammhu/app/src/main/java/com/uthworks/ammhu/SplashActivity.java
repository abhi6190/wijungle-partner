package com.uthworks.ammhu;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import DB.DBConstants;
import DB.DBOperations;
import Model.ClassModel;
import Model.URCModel;
import RestInteraction.RESTInteraction;
import Utils.Utils;
import Utils.ApiUrl;
import adapter.GCMClientManager;
/**
 * Created by VARIABLE on 6/17/2016.
 */
public class SplashActivity extends AppCompatActivity {

    public static final int SLEEP_TIME = 4;
    IntentLauncher launcher;
    SharedPreferences sharedPrefs;
    int bg;
    ImageView root, splashImage;
    SplashActivity splashActivity;
    String PROJECT_NUMBER = "608020460147";
    int count = 0;
    SharedPreferences sharedPreferences;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    RESTInteraction restInteraction;
    String gcmId = "";
    String tag_json_obj = "json_obj_req";
    ProgressDialog pDialog;
    String TAG = "MainActivity";
    TextView tvVisitor, tvsub_title;
    private Request.Priority priority = Request.Priority.HIGH;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
//        Fabric.with(this, new Crashlytics());
        init();
    }

    void init() {
//        if (checkAndRequestPermissions()) {
            setContentView(R.layout.splash_layout);
            splashActivity = SplashActivity.this;
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    "com.uthworks.ammhu",
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        } catch (NoSuchAlgorithmException e) {
//
//        }
//        md5("123456");
        tvVisitor = (TextView)findViewById(R.id.tvVisitor);
        tvsub_title = (TextView)findViewById(R.id.tvsub_title);

        tvsub_title.setText("Online Resource Centre For CBSE And Other State Boards & A Categorized Blogging...");
        getVisitorsCount();
//        getStates();
//        getCountry();
        GCMClientManager pushClientManager = new GCMClientManager(this, PROJECT_NUMBER);
        pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
            @Override
            public void onSuccess(String registrationId, boolean isNewRegistration) {

                Log.d("Registration id", registrationId);
                Utils.gcmId = registrationId;
            }

            @Override
            public void onFailure(String ex) {
                super.onFailure(ex);
            }
        });
        restInteraction = RESTInteraction.getInstance(this);
       // new GetAllCategories().execute();
        getCategories();
        addVisitorsCount();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
//                launcher = new IntentLauncher();
//                launcher.start();


//        }
    }





    /**
     * Callback received when a permissions request has been completed.
     */


    void getCategories() {
        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(ApiUrl.getAllUrcUrl);
        if (entry != null) {
            try {
                String data = new String(entry.data, "UTF-8");
                Log.d("cached", data);
                try {
                    JSONObject jsonObj = new JSONObject(data);
                    if (jsonObj.getString("status").equals("success")) {
                        JSONArray jsonArray = jsonObj.optJSONArray("topic");
                        Utils.urcModels.clear();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            Utils.urcModels.add(new URCModel(jsonObject.optString("id"),
                                    jsonObject.optString("name")));
                        }
                        getClasses();
                    } else {
                        Utils.message = jsonObj.getString("message");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                // handle data, like converting it to xml, json, bitmap etc.,
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    ApiUrl.getAllUrcUrl, null,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject jsonObj) {
                            Log.d(TAG, jsonObj.toString());
//                        pDialog.hide();
                            try {
                                if (jsonObj.getString("status").equals("success")) {
                                    JSONArray jsonArray = jsonObj.optJSONArray("topic");
                                    Utils.urcModels.clear();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        Utils.urcModels.add(new URCModel(jsonObject.optString("id"),
                                                jsonObject.optString("name")));
                                    }
                                    getClasses();
                                }else {
                                    Utils.message = jsonObj.getString("message");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    // hide the progress dialog
//                pDialog.hide();
                }
            }){
                @Override
                public Priority getPriority() {
                    return priority;
                }
            };

            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    50000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(jsonObjReq);
           // AppController.getInstance().getRequestQueue().getCache().invalidate(ApiUrl.getAllUrcUrl, true);
        }

    }



    void getVisitorsCount(){
//       String url = "http://elogic.southindia.cloudapp.azure.com:8083/transtruck/admindashboard/tables/get/childadminshowList.php?pageno=1&limit=10";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getvisitorcountUrl, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
//                        pDialog.hide();
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                tvVisitor.setText("Visitors : " + jsonObj.optString("count"));
                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
//                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }


    void addVisitorsCount(){
//       String url = "http://elogic.southindia.cloudapp.azure.com:8083/transtruck/admindashboard/tables/get/childadminshowList.php?pageno=1&limit=10";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.addvisitorcountUrl, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
//                        pDialog.hide();
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                //tvVisitor.setText("Visitors : "+jsonObj.optString("count"));
                            }else {
                                //Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
//                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    void getClasses(){
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getAllClassesUrl, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("classes");
                                Utils.classModels.clear();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    Utils.classModels.add(new ClassModel(jsonObject.optString("id"),
                                            jsonObject.optString("name")));
                                }
                                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                                finish();
                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
//                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }


    void getCountry(){
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                "http://192.168.43.128/old/ammhu/getcountries.php", null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        try {
                            DBOperations dbOperations = new DBOperations(SplashActivity.this);
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("chapters");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    ContentValues contentValues = new ContentValues();
                                    contentValues.put(DBConstants.KEY_ID, jsonObject.optString("id"));
                                    contentValues.put(DBConstants.COLUMN_NAME, jsonObject.optString("name"));
                                    dbOperations.addCountriesToDB(contentValues);
                                }
                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
//                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }



    void getStates(){
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                "http://192.168.43.128/old/ammhu/getstates.php", null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        try {
                            DBOperations dbOperations = new DBOperations(SplashActivity.this);
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("chapters");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    ContentValues contentValues = new ContentValues();
                                    contentValues.put(DBConstants.KEY_ID, jsonObject.optString("id"));
                                    contentValues.put(DBConstants.COLUMN_NAME, jsonObject.optString("name"));
                                    contentValues.put(DBConstants.COLUMN_COUNTRY_ID, jsonObject.optString("country_id"));
                                    dbOperations.addStatesToDB(contentValues);
                                }
                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
//                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            Log.d("pass", hexString.toString());
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public class GetAllCategories extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String catid;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            progressDialog = new ProgressDialog(CategoryScreen.this);
//            progressDialog.setCancelable(false);
//            progressDialog.setTitle("Please wait ...");
//            progressDialog.setMessage("Signin ...");
//            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.getAllUrc(ApiUrl.getAllUrcUrl);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//            progressDialog.dismiss();
            if (result.equals("success")) {
                new GetAllClasses().execute();

            }else if (result.equalsIgnoreCase("fail")) {
//                Toast.makeText(LaunchingActivity.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
//                Toast.makeText(LaunchingActivity.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class GetAllClasses extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            progressDialog = new ProgressDialog(CategoryScreen.this);
//            progressDialog.setCancelable(false);
//            progressDialog.setTitle("Please wait ...");
//            progressDialog.setMessage("Signin ...");
//            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.getAllClasses(ApiUrl.getAllClassesUrl);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//            progressDialog.dismiss();
            if (result.equals("success")) {

                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                finish();
            }else if (result.equalsIgnoreCase("fail")) {
//                Toast.makeText(LaunchingActivity.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
//                Toast.makeText(LaunchingActivity.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean checkAndRequestPermissions() {
        int permissionCallPhone = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE);
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionCallPhone != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CALL_PHONE);
        }
        if (permissionStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }



    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private class IntentLauncher extends Thread {
        @Override
        /**
         * Sleep for some time and than start new activity.
         */
        public void run() {
            try {
                // Sleeping
                Thread.sleep(SLEEP_TIME * 1000);
                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                finish();
            } catch (Exception e) {
                Log.e("error_msg", e.getMessage());
            }
        }
    }
}
