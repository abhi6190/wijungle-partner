package com.uthworks.ammhu;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import Model.ExamPaperModel;
import Model.PDFModel;
import RestInteraction.RESTInteraction;
import Utils.ApiUrl;
import Utils.Utils;

/**
 * Created by VARIABLE on 6/10/2017.
 */
public class DownloadExamPaperScreen extends AppCompatActivity {
    ListView downloadList;
    String chapterid, classid;
    RESTInteraction restInteraction;
    Spinner spinner1;
    List<String> categories1 = new ArrayList<String>();
    MyAdapter myAdapter;
    ArrayList<ExamPaperModel> tempExamPaperModels = new ArrayList<>();
    private DownloadManager downloadManager;
    private long downloadReference;
    String userId= "";
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exam_paper_layout);
        restInteraction = RESTInteraction.getInstance(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("PDF/DOC");
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        userId = sharedPreferences.getString("userid", "");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        registerReceiver(downloadReceiver, filter);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        chapterid = getIntent().getStringExtra("papertype");
        classid = getIntent().getStringExtra("classid");
        downloadList = (ListView)findViewById(R.id.downloadList);
        spinner1 = (Spinner)findViewById(R.id.spinner1);
        downloadList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (userId.length()>0) {
                downloadManager = (DownloadManager)getSystemService(DOWNLOAD_SERVICE);
                String  filePath = "https://ammhu.com/paper/"+tempExamPaperModels.get(position).getPath().replaceAll(" ", "%20");
                Uri Download_Uri = Uri.parse(filePath);
                DownloadManager.Request request = new DownloadManager.Request(Download_Uri);

                //Restrict the types of networks over which this download may proceed.
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                //Set whether this download may proceed over a roaming connection.
                request.setAllowedOverRoaming(false);
                //Set the title of this download, to be displayed in notifications (if enabled).
                request.setTitle(tempExamPaperModels.get(position).getPath());
                //Set a description of this download, to be displayed in notifications (if enabled)
//                request.setDescription("Android Data download using DownloadManager.");
                //Set the local destination for the downloaded file to a path within the application's external files directory
                request.setDestinationInExternalFilesDir(DownloadExamPaperScreen.this, Environment.DIRECTORY_DOWNLOADS,tempExamPaperModels.get(position).getPath());

                //Enqueue a new download and same the referenceId
                downloadReference = downloadManager.enqueue(request);
                }else{
                    startActivity(new Intent(DownloadExamPaperScreen.this, LaunchingActivity.class));
                }
            }
        });

        for (int i = 0; i < Utils.examPaperModels.size(); i++) {
            if(Utils.examPaperModels.get(i).getType().equalsIgnoreCase(chapterid) && Utils.examPaperModels.get(i).getClass_id().equalsIgnoreCase(classid)) {
                if(!categories1.contains(Utils.examPaperModels.get(i).getYear())) {
                    categories1.add(Utils.examPaperModels.get(i).getYear());
                }
            }
        }
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories1);

        // Drop down layout style - list view with radio button
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner1.setAdapter(dataAdapter1);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getData(chapterid, categories1.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //check if the broadcast message is for our Enqueued download
            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            if(downloadReference == referenceId){
                int ch;
                ParcelFileDescriptor file;
                StringBuffer strContent = new StringBuffer("");
                StringBuffer countryData = new StringBuffer("");

                //parse the JSON data and display on the screen
                try {
                    file = downloadManager.openDownloadedFile(downloadReference);
                    FileInputStream fileInputStream
                            = new ParcelFileDescriptor.AutoCloseInputStream(file);

//                    while( (ch = fileInputStream.read()) != -1)
//                        strContent.append((char)ch);

//                    JSONObject responseObj = new JSONObject(strContent.toString());
//                    JSONArray countriesObj = responseObj.getJSONArray("countries");
//
//                    for (int i=0; i<countriesObj.length(); i++){
//                        Gson gson = new Gson();
//                        String countryInfo = countriesObj.getJSONObject(i).toString();
//                        Country country = gson.fromJson(countryInfo, Country.class);
//                        countryData.append(country.getCode() + ": " + country.getName() +"\n");
//                    }
//
//                    TextView showCountries = (TextView) findViewById(R.id.countryData);
//                    showCountries.setText(countryData.toString());

                    Toast toast = Toast.makeText(DownloadExamPaperScreen.this,
                            "Downloading finished", Toast.LENGTH_LONG);
                    toast.show();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    };

    public void getData(String type, String year){
        tempExamPaperModels.clear();
        for (int i = 0; i < Utils.examPaperModels.size(); i++) {
            if(Utils.examPaperModels.get(i).getType().equalsIgnoreCase(type) &&
                    Utils.examPaperModels.get(i).getYear().equalsIgnoreCase(year)&&
                    Utils.examPaperModels.get(i).getClass_id().equalsIgnoreCase(classid)) {
                tempExamPaperModels.add(Utils.examPaperModels.get(i));
            }
        }
        myAdapter = new MyAdapter(DownloadExamPaperScreen.this, tempExamPaperModels);
        downloadList.setAdapter(myAdapter);
    }



    class MyAdapter extends BaseAdapter {
        Context context;
        LayoutInflater layoutInflater;
        ArrayList<ExamPaperModel> pdfModels;
        public MyAdapter(Context context, ArrayList<ExamPaperModel> pdfModels){
            this.context = context;
            this.pdfModels = pdfModels;
            layoutInflater = LayoutInflater.from(context);
        }
        @Override
        public int getCount() {
            return pdfModels.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = null;
            if (view == null){
                view = layoutInflater.inflate(R.layout.product_item_layout, parent, false);
            }
            TextView tvPdfname = (TextView)view.findViewById(R.id.tvPdfname);
            TextView tvDate = (TextView)view.findViewById(R.id.tvDate);
            TextView tvDownloads = (TextView)view.findViewById(R.id.tvDownloads);

            tvDownloads.setText(pdfModels.get(position).getDownloads());
            tvPdfname.setText(Html.fromHtml(pdfModels.get(position).getTitle()));
            tvDate.setText(pdfModels.get(position).getPost_date());
            return view;
        }
    }
}
