package com.uthworks.ammhu;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import Model.ImageModel;
import Model.PDFModel;
import RestInteraction.RESTInteraction;
import Utils.Utils;

/**
 * Created by VARIABLE on 1/18/2017.
 */
public class SearchImage extends AppCompatActivity {
    MyAdapter myAdapter;
    GridView gridGallery;

    ArrayList<ImageModel> imageModels;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_image);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Images");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        gridGallery = (GridView)findViewById(R.id.gridGallery);
        gridGallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(SearchImage.this, FullImageScreen.class).putExtra("pos", imageModels.get(position)));
            }
        });
        imageModels = (ArrayList<ImageModel>) getIntent().getSerializableExtra("image");
        myAdapter = new MyAdapter(this, imageModels);
        gridGallery.setAdapter(myAdapter);
    }

    class MyAdapter extends BaseAdapter {
        LayoutInflater layoutInflater;
        Context context;
        ArrayList<ImageModel> items;
        public MyAdapter(Context context, ArrayList<ImageModel> imageModels){
            this.context = context;
            items = imageModels;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View mView = null;
            if (mView == null){
                mView = layoutInflater.inflate(R.layout.grid_list_items, parent, false);
            }
            ImageView ivgalleryImage = (ImageView)mView.findViewById(R.id.ivgalleryImage);
            TextView ivgalleryImageName = (TextView)mView.findViewById(R.id.ivgalleryImageName);
            String url = "https://ammhu.com/uploads/"+items.get(position).getSrc();
            url=url.replaceAll(" ", "%20");
            Log.d("imageurl", url);
            Picasso.with(context)
                    .load(url.trim())
                    .into(ivgalleryImage);
//            UrlImageViewHelper.setUrlDrawable(ivgalleryImage, url);
            ivgalleryImageName.setText(items.get(position).getTitle());

            return mView;
        }
    }
}
