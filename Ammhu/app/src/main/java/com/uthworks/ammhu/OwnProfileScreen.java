package com.uthworks.ammhu;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import Model.FollowerModel;
import Model.FollowingModel;
import Model.FriendsModel;
import Model.TopicModel;
import Utils.ApiUrl;
import Utils.Utils;
import Utils.MultipartUtility;

/**
 * Created by VARIABLE on 2/24/2017.
 */
public class OwnProfileScreen extends AppCompatActivity {
    ImageView ivUsrPic;
    Button ivUploadPic, btnUpdateBProfile,btnUpdatePDetails;
    EditText etFName, etLName, etDOB, etMobNum, etCountry, etState, etCity, etAddress, etEmail, etPwd;
    TextView tvFriendsCount, tvfollowersCount,tvFollowingCount;
    SharedPreferences sharedPreferences;
    String userId, image, fname, lname, email, pwd, address, country, state, city, dob, mobileno;
    String tag_json_obj = "json_obj_req";
    String TAG = "OwnProfileScreen";
    ProgressDialog pDialog;
    ArrayList<FollowingModel> followingModels;
    ArrayList<FollowerModel> followerModels;
    final String uploadFileName = "quiz.png";
    private static final int PICK_REQUEST_IMAG = 1;
    private static final int CAPTURE_REQUEST_IMAGE = 2;
    private final int PIC_CROP = 3;
    private Uri uri;
    String TEMP_PROFILE_PIC_FILE="", imgArray;
    Random random;
    int low = 0123456;
    int high = 504598766;
    private String filePath= "";
    Button btnManageTopics;
    String path, error;
    String result = "";
    GridView gridView;
    MyAdapter myAdapter;
    String ids = "";
    ArrayList<TopicModel> tempModels = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.own_profile_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Profile");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        ivUsrPic = (ImageView)findViewById(R.id.ivUsrPic);
        ivUploadPic = (Button)findViewById(R.id.ivUploadPic);
        btnManageTopics = (Button)findViewById(R.id.btnManageTopics);
        btnUpdateBProfile = (Button)findViewById(R.id.btnUpdateBProfile);
        btnUpdatePDetails = (Button)findViewById(R.id.btnUpdatePDetails);
        etFName = (EditText)findViewById(R.id.etFName);
        etLName = (EditText)findViewById(R.id.etLName);
        etDOB = (EditText)findViewById(R.id.etDOB);
        etMobNum = (EditText)findViewById(R.id.etMobNum);
        etCountry = (EditText)findViewById(R.id.etCountry);
        etState = (EditText)findViewById(R.id.etState);
        etCity = (EditText)findViewById(R.id.etCity);
        etAddress = (EditText)findViewById(R.id.etAddress);
        etEmail = (EditText)findViewById(R.id.etEmail);
        etPwd = (EditText)findViewById(R.id.etPwd);
        tvFriendsCount = (TextView)findViewById(R.id.tvFriendsCount);
        tvfollowersCount = (TextView)findViewById(R.id.tvfollowersCount);
        tvFollowingCount = (TextView)findViewById(R.id.tvFollowingCount);
        userId = sharedPreferences.getString("userid", "");
        getFriends();
        getFollowers();
        getFollowing();
        btnManageTopics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTopicsDialog();
            }
        });
        ivUploadPic.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                random = new Random();
                int i2 = (random.nextInt(high - low) + low * high);
                TEMP_PROFILE_PIC_FILE = "Ammhu" + i2 + ".jpg";
                showDialog();
            }
        });
        btnUpdateBProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String url = ApiUrl.updateprofileUrl+"userid="+userId+"&fname="+ URLEncoder.encode(etFName.getText().toString())+
                        "&lname="+URLEncoder.encode(etLName.getText().toString()) + "&address="+URLEncoder.encode(etAddress.getText().toString()) +
                        "&country="+ URLEncoder.encode(etCountry.getText().toString())+
                        "&state="+URLEncoder.encode(etState.getText().toString()) +
                        "&city=" + URLEncoder.encode(etCity.getText().toString())+
                        "&dob=" +URLEncoder.encode(etDOB.getText().toString()) +
                        "&mobileno="+ URLEncoder.encode(etMobNum.getText().toString())+ "&email="+"&pwd="+"&flag=0";
                updateProfile(url, 0);
            }
        });
        btnUpdatePDetails.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String url = ApiUrl.updateprofileUrl+"userid="+userId+"&fname="+ URLEncoder.encode(etFName.getText().toString())+
                        "&lname="+URLEncoder.encode(etLName.getText().toString()) + "&address="+URLEncoder.encode(etAddress.getText().toString()) +
                        "&country="+ URLEncoder.encode(etCountry.getText().toString())+
                        "&state="+URLEncoder.encode(etState.getText().toString()) +
                        "&city=" + URLEncoder.encode(etCity.getText().toString())+
                        "&dob=" +URLEncoder.encode(etDOB.getText().toString()) +
                        "&mobileno="+ URLEncoder.encode(etMobNum.getText().toString())+
                        "&email="+URLEncoder.encode(etEmail.getText().toString())+
                        "&pwd="+URLEncoder.encode(etPwd.getText().toString())+"&flag=1";
                updateProfile(url, 1);
            }
        });

        setData();
        tvFriendsCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OwnProfileScreen.this, FriendsScreen.class));
            }
        });
        tvfollowersCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OwnProfileScreen.this, FollowersScreen.class).putExtra("followers", followerModels));
            }
        });
        tvFollowingCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OwnProfileScreen.this, FollowingScreen.class).putExtra("following", followingModels));
            }
        });
    }

    void setText(String str, EditText editText){
        if (str.length()>0){
            editText.setText(Html.fromHtml(str));
        }
    }

    void showTopicsDialog(){
        final Dialog dialog = new Dialog(this, R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.managetopics_layout);
        gridView = (GridView)dialog.findViewById(R.id.gridView);
        List<String> oldTopics = Arrays.asList(sharedPreferences.getString("topics", "").split("\\s*,\\s*"));
        Log.d("old topics", sharedPreferences.getString("topics", ""));
        for (int i = 0; i < Utils.topicModels.size(); i++) {
            if(!Utils.topicModels.get(i).getId().equals("81")){
                tempModels.add(Utils.topicModels.get(i));
            }
        }
        for (int j = 0; j < tempModels.size(); j++) {

            for (int i = 0; i < oldTopics.size(); i++) {
                if(tempModels.get(j).getId().trim().equals(oldTopics.get(i).toString().replaceAll("'", "").trim())){
                    TopicModel model = null;
                    model = new TopicModel(tempModels.get(j).getId(),
                            tempModels.get(j).getTopicName(), 1);
                    tempModels.remove(j);
                    tempModels.add(j, model);
                }
            }
        }

        myAdapter = new MyAdapter(OwnProfileScreen.this,tempModels);
        gridView.setAdapter(myAdapter);
        Button btnUpdateTopic = (Button)dialog.findViewById(R.id.btnUpdateTopic);
        btnUpdateTopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 ids = "";
                for (int i = 0; i < Utils.topicModels.size(); i++) {
                    if (Utils.topicModels.get(i).getIsCheck() == 1){
                        ids += Utils.topicModels.get(i).getId() + ",";
                    }
                }
                if (ids.contains(",")) {
                    ids = ids.substring(0,ids.length()-1);
                }

                if(ids.length()==0){
                    Toast.makeText(OwnProfileScreen.this, "Please select atleast one topic.", Toast.LENGTH_SHORT).show();
                    return;
                }
                dialog.dismiss();
                String url = ApiUrl.updateusertopicsUrl+"userid="+userId+
                        "&topics="+URLEncoder.encode(ids);
                updateProfile(url, 2);
            }
        });

        dialog.show();
    }

    class MyAdapter extends BaseAdapter {
        Context context;
        LayoutInflater layoutInflater;
        ArrayList<TopicModel> items;
        public MyAdapter(Context context, ArrayList<TopicModel> items){
            this.context = context;
            this.items = items;
            layoutInflater = LayoutInflater.from(context);
        }
        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = null;
            if (view == null){
                view = layoutInflater.inflate(R.layout.grid_items_topics, parent, false);
            }
            CheckBox cbTopic = (CheckBox)view.findViewById(R.id.cbTopic);
            cbTopic.setText(items.get(position).getTopicName());


            if (items.get(position).getIsCheck() == 1){
                cbTopic.setChecked(true);
            }else{
                cbTopic.setChecked(false);
            }
            cbTopic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TopicModel model = null;
                    if (items.get(position).getIsCheck() == 1){
                        model = new TopicModel(items.get(position).getId(),
                                items.get(position).getTopicName(), 0);
                    }else{
                        model = new TopicModel(items.get(position).getId(),
                                items.get(position).getTopicName(), 1);
                    }
                    items.remove(position);
                    items.add(position, model);
                    notifyDataSetChanged();
                }
            });
            return view;
        }
    }

    void setData(){
        image = sharedPreferences.getString("image", "");
        fname = sharedPreferences.getString("name", "");
        lname = sharedPreferences.getString("sname", "");
        email = sharedPreferences.getString("email", "");
        pwd = sharedPreferences.getString("password", "");
        address = sharedPreferences.getString("address", "");
        country = sharedPreferences.getString("country", "");
        state = sharedPreferences.getString("state", "");
        city = sharedPreferences.getString("city", "");
        dob = sharedPreferences.getString("dob", "");
        mobileno = sharedPreferences.getString("phone", "");
        if (image.length()>0){
            String url = "https://ammhu.com/images/"+image;
            url=url.replaceAll(" ", "%20");
            Picasso.with(OwnProfileScreen.this)
                    .load(url)
                    .placeholder(R.mipmap.photo)
                    .into(ivUsrPic);
        }
        setText(fname, etFName);
        setText(lname, etLName);
        setText(email, etEmail);
        setText(pwd, etPwd);
        setText(address, etAddress);
        setText(country, etCountry);
        setText(state, etState);
        setText(city, etCity);
        setText(dob, etDOB);
        setText(mobileno, etMobNum);
    }

    void getFriends(){
//       String url = "http://elogic.southindia.cloudapp.azure.com:8083/transtruck/admindashboard/tables/get/childadminshowList.php?pageno=1&limit=10";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getFriendsUrl + "reciver_id=" + userId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
//                        pDialog.hide();
                        try {
                            Utils.friendsModels.clear();
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("friends");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    Utils.friendsModels.add(new FriendsModel(jsonObject.optString("id"),
                                            jsonObject.optString("sender_id"),
                                            jsonObject.optString("reciver_id"),
                                            jsonObject.optString("name"),
                                            jsonObject.optString("image")));
                                }
//                                mAdapter = new FriendsAdapter(ProfileScreen.this, Utils.friendsModels);
//                                mRecyclerView.setAdapter(mAdapter);
                                tvFriendsCount.setText(Utils.friendsModels.size()+"");

                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
//                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    void getFollowers(){
//       String url = "http://elogic.southindia.cloudapp.azure.com:8083/transtruck/admindashboard/tables/get/childadminshowList.php?pageno=1&limit=10";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getFollowersUrl + "userid=" + userId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
//                        pDialog.hide();
                        try {
                            followerModels = new ArrayList<>();
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("followers");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    followerModels.add(new FollowerModel(jsonObject.optString("id"),
                                            jsonObject.optString("user_id"),
                                            jsonObject.optString("followers_id"),
                                            jsonObject.optString("name"),
                                            jsonObject.optString("image")));
                                }
//                                mAdapter = new FriendsAdapter(ProfileScreen.this, Utils.friendsModels);
//                                mRecyclerView.setAdapter(mAdapter);
                                tvfollowersCount.setText(followerModels.size()+"");

                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
//                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }


    void getFollowing(){
//       String url = "http://elogic.southindia.cloudapp.azure.com:8083/transtruck/admindashboard/tables/get/childadminshowList.php?pageno=1&limit=10";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getFollowingUrl + "userid=" + userId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
//                        pDialog.hide();
                        try {
                            followingModels = new ArrayList<>();
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("following");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    followingModels.add(new FollowingModel(jsonObject.optString("id"),
                                            jsonObject.optString("user_id"),
                                            jsonObject.optString("followers_id"),
                                            jsonObject.optString("name"),
                                            jsonObject.optString("image")));
                                }
//                                mAdapter = new FriendsAdapter(ProfileScreen.this, Utils.friendsModels);
//                                mRecyclerView.setAdapter(mAdapter);
                                tvFollowingCount.setText(followingModels.size()+"");

                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
//                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }


    void updateImage(){
//       String url = "http://elogic.southindia.cloudapp.azure.com:8083/transtruck/admindashboard/tables/get/childadminshowList.php?pageno=1&limit=10";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.updateimageUrl + "userid=" + userId +"&imagename="+TEMP_PROFILE_PIC_FILE, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
//                        pDialog.hide();
                            if (jsonObj.optString("status").equals("success")){
                                sharedPreferences.edit().putString("image", TEMP_PROFILE_PIC_FILE).apply();
                                setData();
                                Toast.makeText(OwnProfileScreen.this, jsonObj.optString("message"), Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(OwnProfileScreen.this, jsonObj.optString("message"), Toast.LENGTH_SHORT).show();
                            }
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
//                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    void updateProfile(String url, final int flag){
        Log.d("url", url);
         pDialog = new ProgressDialog(OwnProfileScreen.this);
        pDialog.setTitle("Please wait ...");
        pDialog.setMessage("Updating data ...");
        pDialog.setCancelable(false);
        pDialog.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        pDialog.hide();
                        if (jsonObj.optString("status").equals("success")){
                            if (flag==0) {
                                sharedPreferences.edit().putString("name", etFName.getText().toString()).apply();
                                sharedPreferences.edit().putString("dob",etDOB.getText().toString()).apply();
                                sharedPreferences.edit().putString("address", etAddress.getText().toString()).apply();
                                sharedPreferences.edit().putString("phone", etMobNum.getText().toString()).apply();
                                sharedPreferences.edit().putString("country", etCountry.getText().toString()).apply();
                                sharedPreferences.edit().putString("state", etState.getText().toString()).apply();
                                sharedPreferences.edit().putString("city", etCity.getText().toString()).apply();
                                sharedPreferences.edit().putString("sname", etLName.getText().toString()).apply();
                                setData();
                            }else if(flag==2){
                                sharedPreferences.edit().putString("topics", ids).apply();
                                sharedPreferences.edit().putInt("topicslengthnew", ids.length()).apply();
                            }else{
                                sharedPreferences.edit().putString("email", etEmail.getText().toString()).apply();
                                sharedPreferences.edit().putString("password", etPwd.getText().toString()).apply();
                                setData();
                            }

                            Toast.makeText(OwnProfileScreen.this, jsonObj.optString("message"), Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(OwnProfileScreen.this, jsonObj.optString("message"), Toast.LENGTH_SHORT).show();
                        }
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }
    private void showDialog() {
        final Dialog dialog = new Dialog(OwnProfileScreen.this,
                android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.custom);
        ((TextView) dialog.findViewById(R.id.text_camera))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        takePic(CAPTURE_REQUEST_IMAGE);
                    }
                });
        ((TextView) dialog.findViewById(R.id.text_gallery))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        takePic(PICK_REQUEST_IMAG);
                    }
                });
        ((ImageView) dialog.findViewById(R.id.iVCamCancel))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
        dialog.show();
    }

    private void takePic(int action) {
        if (isSDCARDMounted()) {
            switch (action) {
                case PICK_REQUEST_IMAG:
                    Intent i = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    i.setType("image/*");
                    i.putExtra("crop", "true");
                    i.putExtra("aspectX", 1.5);
                    i.putExtra("aspectY", 1);
                    i.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri());
                    i.putExtra("outputFormat",
                            Bitmap.CompressFormat.JPEG.toString());
                    startActivityForResult(i, PIC_CROP);
                    break;
                case CAPTURE_REQUEST_IMAGE:
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    uri = getOutputMediaFileUri(); // create a file to save the
                    // image
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    startActivityForResult(intent, CAPTURE_REQUEST_IMAGE);
                    break;

                default:
                    break;
            }
        }
    }

    private Uri getTempUri() {
        Uri uri = null;
        if (getTempFile() != null) {
            uri = Uri.fromFile(getTempFile());
        }
        return uri;
    }

    private File getTempFile() {
        if (isSDCARDMounted()) {
            File f = new File(Environment.getExternalStorageDirectory(),
                    TEMP_PROFILE_PIC_FILE);
            try {
                f.createNewFile();
            } catch (IOException e) {

            }
            return f;
        } else {
            return null;
        }
    }

    /** Create a File for saving an image or video */
    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "Ammhu");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("", "failed to create directory");
                return null;
            }
        }

        File mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_DEMO.jpeg");
        if (!mediaFile.exists())
            try {
                mediaFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        return mediaFile;
    }

    /** Create a file Uri for saving an image or video */
    private static Uri getOutputMediaFileUri() {
        return Uri.fromFile(getOutputMediaFile());
    }

    static boolean isSDCARDMounted() {
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED))
            return true;
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CAPTURE_REQUEST_IMAGE: {
                    cropImage(uri);
                }
                break;
                case PIC_CROP: {
                    File tempFile = getTempFile();
                    String name = "";
                    filePath = Environment.getExternalStorageDirectory() + "/"
                            + TEMP_PROFILE_PIC_FILE;
                    imgArray = imageToByteArray(filePath);
                }
                break;
                default:
                    break;
            }
        }
    }

    private String imageToByteArray(String path) {
        Bitmap bm = decodeSampledBitmapFromResource(path, 150, 50);
        ivUsrPic.setImageBitmap(bm);
        new UploadImageAsync().execute();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
        } catch (Exception e) {
            filePath = null;
            return null;
        }
    }

    public Bitmap RotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        Bitmap bitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
        return bitmap;
    }
    private void cropImage(Uri picUri) {
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.setDataAndType(picUri, "image/*");
        cropIntent.putExtra("crop", "true");
        cropIntent.putExtra("aspectX", 1.5);
        cropIntent.putExtra("aspectY", 1);
        cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri());
        cropIntent.putExtra("outputFormat",
                Bitmap.CompressFormat.JPEG.toString());
        startActivityForResult(cropIntent, PIC_CROP);
    }



    public static Bitmap decodeSampledBitmapFromResource(String filepath,
                                                         int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filepath, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filepath, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public String uploadFile(String sourceFileUri) {

        String fileName = sourceFileUri;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        File uploadFile1 = new File("e:/Test/images.jpg");
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);

        if (sourceFile != null && !sourceFile.isFile()) {
            Log.e("uploadFile", "Source File not exist :"
                    +filePath + "" + uploadFileName);

            runOnUiThread(new Runnable() {
                public void run() {
                }
            });

            return "fail";
        }
        else
        {
            String charset = "UTF-8";
            File file = new File(fileName);

            try {
                MultipartUtility multipart = new MultipartUtility("https://ammhu.com/android/uploadScript.php", charset);

                multipart.addHeaderField("User-Agent", "CodeJava");
                multipart.addHeaderField("Test-Header", "Ammhu");
//                multipart.addFormField("userid", userId);
                multipart.addFilePart("imageFile[]", file);

                List<String> response = multipart.finish();
                System.out.println("SERVER REPLIED:");

                for (String line : response) {
                    System.out.println(line);
                    result = line;
                }
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.optString("status").equalsIgnoreCase("success")) {
                        result = "success";
                        boolean delete = file.delete();
                    }else {
                        if (jsonObject.has("errors")) {
                            error = jsonObject.getString("errors");
                        }
                        boolean delete = file.delete();
                        result = "Failure";
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    boolean delete = file.delete();
                }
            } catch (IOException ex) {
                System.err.println(ex);
                boolean delete = file.delete();
            }
            return result;

        } // End else block 
    }

    public class UploadImageAsync extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(OwnProfileScreen.this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Please wait...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return uploadFile(filePath);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("success")) {
                updateImage();
            }else if (result.equalsIgnoreCase("Failure")) {
                Toast.makeText(OwnProfileScreen.this, "Oops.. An error occurred. Please try again", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(OwnProfileScreen.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
