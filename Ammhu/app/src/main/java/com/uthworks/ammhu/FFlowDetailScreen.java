package com.uthworks.ammhu;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import RestInteraction.RESTInteraction;
import Utils.Utils;
import Utils.ApiUrl;

/**
 * Created by VARIABLE on 11/26/2016.
 */
public class FFlowDetailScreen extends AppCompatActivity {

    ImageView ivFullImage, ivLike, ivUnlike;
    TextView tvFullImageName, tvViewComments;
    EditText etComment;
    RESTInteraction restInteraction;
    Button btnPostCmnt;
    int position;
    TextView tvUnlike, tvLike, tvPostDesc ;
    SharedPreferences sharedPreferences;
    String userId = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_detail_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Posts");
        setSupportActionBar(toolbar);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        userId = sharedPreferences.getString("userid", "");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        restInteraction = RESTInteraction.getInstance(this);
        position = getIntent().getIntExtra("pos", -1);
        tvLike = (TextView)findViewById(R.id.tvLike);
        tvUnlike = (TextView)findViewById(R.id.tvUnlike);
        tvPostDesc = (TextView)findViewById(R.id.tvPostDesc);
//        new GetLikeCount(Utils.imageModels.get(position).getId()).execute();
//        new GetUnlikeCount(Utils.imageModels.get(position).getId()).execute();
        ivFullImage = (ImageView)findViewById(R.id.ivFullImage);
        ivLike = (ImageView)findViewById(R.id.ivLike);
        ivUnlike = (ImageView)findViewById(R.id.ivUnlike);

        ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userId.length()>0) {
                    new VoteImage(Utils.fFlowModels.get(position).getId(), userId, "1").execute();
                }else{
                    startActivity(new Intent(FFlowDetailScreen.this, LaunchingActivity.class));
                }

            }
        });

        ivUnlike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userId.length()>0) {
                    new VoteImage(Utils.fFlowModels.get(position).getId(), userId, "0").execute();
                }else{
                    startActivity(new Intent(FFlowDetailScreen.this, LaunchingActivity.class));
                }

            }
        });
        tvFullImageName = (TextView)findViewById(R.id.tvFullImageName);
        tvViewComments = (TextView)findViewById(R.id.tvViewComments);
        tvViewComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FFlowDetailScreen.this, CommentScreen.class).putExtra("imageid", Utils.fFlowModels.get(position).getId()));
            }
        });
        etComment = (EditText)findViewById(R.id.etComment);
        btnPostCmnt = (Button)findViewById(R.id.btnPostCmnt);
        if (position != -1){
            String url = "https://ammhu.com/uploads/"+Utils.fFlowModels.get(position).getFeature_img();
            url=url.replaceAll(" ", "%20");
//            Picasso.with(this)
//                    .load(url)
//                    .into(ivFullImage);
//            UrlImageViewHelper.setUrlDrawable(ivFullImage, url);
            if (Utils.fFlowModels.get(position).getFeature_img().length()>0) {
                Picasso.with(this)
                        .load(url)
                        .placeholder(R.mipmap.placeholder) // optional
                        .error(R.mipmap.placeholder)         // optional
                        .into(ivFullImage);
            }else{
                ivFullImage.setImageResource(R.mipmap.placeholder);
            }
            tvFullImageName.setText(Html.fromHtml(Utils.fFlowModels.get(position).getPost_title()));
            tvPostDesc.setText(Html.fromHtml(Utils.fFlowModels.get(position).getPost_des()));
        }
//        btnPostCmnt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (etComment.getText().toString().trim().length()>0){
//                    new SendComment(Utils.imageModels.get(position).getId(),"289", URLEncoder.encode(etComment.getText().toString().trim()),
//                            System.currentTimeMillis()+"").execute();
//                }else{
//                    Toast.makeText(FFlowDetailScreen.this, "Please enter your comment.", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
    }

    public class GetLikeCount extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String imageid;

        public GetLikeCount(String imageid){
            this.imageid = imageid;
        }


        @Override
        protected String doInBackground(String... params) {
            return restInteraction.getlikeCount(ApiUrl.getLikeCountUrl + "gallery_image_id=" + imageid);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//            progressDialog.dismiss();
            if (result.equals("success")) {
                tvLike.setText(Utils.likeCount);
            }else if (result.equalsIgnoreCase("fail")) {
                tvLike.setText(Utils.likeCount);
//                Toast.makeText(LaunchingActivity.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
//                Toast.makeText(LaunchingActivity.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }
    public class GetUnlikeCount extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String imageid;

        public GetUnlikeCount(String imageid){
            this.imageid = imageid;
        }


        @Override
        protected String doInBackground(String... params) {
            return restInteraction.getunlikeCount(ApiUrl.getUnlikeCountUrl + "gallery_image_id=" + imageid);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//            progressDialog.dismiss();
            if (result.equals("success")) {
                tvUnlike.setText(Utils.unlikeCount);
            }else if (result.equalsIgnoreCase("fail")) {
                tvUnlike.setText(Utils.unlikeCount);
//                Toast.makeText(LaunchingActivity.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
//                Toast.makeText(LaunchingActivity.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class SendComment extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String catid;
        String topicid;
        String videoLink;
        String videoDesc;

        public SendComment(String catid, String topicid, String videoLink, String videoDesc){
            this.catid = catid;
            this.topicid = topicid;
            this.videoLink = videoLink;
            this.videoDesc = videoDesc;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(FFlowDetailScreen.this);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Please wait ...");
            progressDialog.setMessage("Posting comment ...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.common(ApiUrl.sendCommentUrl + "c_item_id=" + catid + "&user_id=" + topicid
                    + "&c_text=" + videoLink + "&c_when=" + videoDesc);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("success")) {
                etComment.setText("");
                Toast.makeText(FFlowDetailScreen.this, "Comment post succesfully.", Toast.LENGTH_SHORT).show();
            }else if (result.equalsIgnoreCase("fail")) {
//                Toast.makeText(LaunchingActivity.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
//                Toast.makeText(LaunchingActivity.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public class VoteImage extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String catid;
        String topicid;
        String videoLink;
        String videoDesc;

        public VoteImage(String catid, String topicid, String videoLink){
            this.catid = catid;
            this.topicid = topicid;
            this.videoLink = videoLink;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(FFlowDetailScreen.this);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Please wait ...");
            progressDialog.setMessage("Voting ...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.common(ApiUrl.galleryvoteUrl + "gallery_image_id=" + catid + "&user_id=" + topicid
                    + "&vote=" + videoLink);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("success")) {
                new GetLikeCount(Utils.imageModels.get(position).getId()).execute();
                new GetUnlikeCount(Utils.imageModels.get(position).getId()).execute();
//                Toast.makeText(FFlowDetailScreen.this, "Comment post succesfully.", Toast.LENGTH_SHORT).show();
            }else if (result.equalsIgnoreCase("fail")) {
                Toast.makeText(FFlowDetailScreen.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
//                Toast.makeText(LaunchingActivity.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

