package com.uthworks.ammhu;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import DB.DBConstants;
import DB.DBOperations;
import Utils.Utils;

/**
 * Created by VARIABLE on 3/11/2016.
 */
public class PushNotificationService extends GcmListenerService {
    SharedPreferences sharedPreferences;
    DBOperations dbOperations;
    static String toId, groupId;
    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.e("Message Received", data.toString());
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(PushNotificationService.this);
        dbOperations = new DBOperations(PushNotificationService.this);
        if (data.containsKey("flag")){
            if (data.getString("flag").equals("message")){
                ContentValues contentValues = new ContentValues();
                toId = data.getString("user_id");
                contentValues.put(DBConstants.COLUMN_SENDER_ID, data.getString("user_id"));
                contentValues.put(DBConstants.COLUMN_RECEIVER_ID, sharedPreferences.getString("userid", ""));
                contentValues.put(DBConstants.COLUMN_MESSAGE, data.getString("msg"));
                contentValues.put(DBConstants.COLUMN_TIME, data.getString("time"));
                contentValues.put(DBConstants.COLUMN_STATUS, Utils.status);
                contentValues.put(DBConstants.COLUMN_BEEP, Utils.beep);

                dbOperations.addChatToDB(contentValues);
            }else if (data.getString("flag").equals("groupmessage")){
                ContentValues contentValues = new ContentValues();
                groupId = data.getString("group_id");
                contentValues.put(DBConstants.COLUMN_GROUP_ID, data.getString("group_id"));
                contentValues.put(DBConstants.COLUMN_FRIEND_ID, data.getString("user_id"));
                contentValues.put(DBConstants.COLUMN_RECIVER_ID, sharedPreferences.getString("userid", ""));
                contentValues.put(DBConstants.COLUMN_MESSAGE, data.getString("msg"));
                contentValues.put(DBConstants.COLUMN_TIME, data.getString("time"));
                contentValues.put(DBConstants.COLUMN_STATUS, Utils.status);
                contentValues.put(DBConstants.COLUMN_BEEP, Utils.beep);
                dbOperations.addGroupChatToDB(contentValues);
            }
        }
        generateNotification(PushNotificationService.this, data.getString("price"), data.getString("flag"));
        updateMyActivity(PushNotificationService.this, "1");
    }

    private static void generateNotification(Context context, String message, String flag) {
        Intent intent = null;
        if (flag.equals("message")){
            intent = new Intent(context, ChatScreen.class);
            intent.putExtra("toid", toId);
        }else if (flag.equals("request")) {
            intent = new Intent(context, FriendRequestScreen.class);
        }else if (flag.equals("groupmessage")) {
            intent = new Intent(context, GroupChatScreen.class);
            intent.putExtra("groupid", groupId);
        }else{
            intent = new Intent(context, FriendsScreen.class);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        int icon = R.mipmap.logo;
        int mNotificationId = 001;
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        context,
                        0,
                        intent,
                        PendingIntent.FLAG_CANCEL_CURRENT
                );

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                context);
        Notification notification = mBuilder.setSmallIcon(icon).setTicker("Ammhu").setWhen(0)
                .setAutoCancel(true)
                .setContentTitle("Ammhu")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentIntent(resultPendingIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.logo))
                .setContentText(message).build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(mNotificationId, notification);

    }

    static void updateMyActivity(Context context, String message) {
        Intent intent = new Intent("com.uthworks.ammhu");
        //put whatever data you want to send, if any
        intent.putExtra("message", message);
        //send broadcast
        context.sendBroadcast(intent);
    }

}
