package com.uthworks.ammhu;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import Model.FriendsModel;
import RestInteraction.RESTInteraction;
import Utils.ApiUrl;
import Utils.Utils;
import adapter.FriendsAdapter;

/**
 * Created by VARIABLE on 12/19/2016.
 */
public class FriendsScreen extends AppCompatActivity {
    RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;
    RESTInteraction restInteraction;
    String userId;
    SharedPreferences sharedPreferences;
    String tag_json_obj = "json_obj_req";
    ProgressDialog pDialog;
    String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.firend_request_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Friends");
        setSupportActionBar(toolbar);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        userId = sharedPreferences.getString("userid", "");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        restInteraction = RESTInteraction.getInstance(this);
        mRecyclerView = (RecyclerView)findViewById(R.id.recyclerView_listing);
        mRecyclerView.setHasFixedSize(true);
//        llBottom = (LinearLayout)view.findViewById(R.id.llBottom);
//        llBottom.setVisibility(View.VISIBLE);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        if (Utils.friendsModels.size()>0){
            mAdapter = new FriendsAdapter(FriendsScreen.this, Utils.friendsModels);
            mRecyclerView.setAdapter(mAdapter);
        }else{
            getFriends();
        }
    }



    void getFriends(){
        pDialog = new ProgressDialog(FriendsScreen.this);
        pDialog.setCancelable(false);
        pDialog.setTitle("Please wait ...");
        pDialog.setMessage("Getting friends ...");
        pDialog.show();
//       String url = "http://elogic.southindia.cloudapp.azure.com:8083/transtruck/admindashboard/tables/get/childadminshowList.php?pageno=1&limit=10";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getFriendsUrl + "reciver_id=" + userId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        pDialog.hide();
                        try {
                            Utils.friendsModels.clear();
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("friends");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    Utils.friendsModels.add(new FriendsModel(jsonObject.optString("id"),
                                            jsonObject.optString("sender_id"),
                                            jsonObject.optString("reciver_id"),
                                            jsonObject.optString("name"),
                                            jsonObject.optString("image")));
                                }
                                mAdapter = new FriendsAdapter(FriendsScreen.this, Utils.friendsModels);
                                mRecyclerView.setAdapter(mAdapter);

                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }
}
