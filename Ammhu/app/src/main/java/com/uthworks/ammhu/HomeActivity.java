package com.uthworks.ammhu;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.plus.Plus;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import Fragment.EntranceExamFragment;
import Fragment.ExamPaperFragment;
import Fragment.GalleryFragment;
import Fragment.HomeFragment;
import Fragment.ProjextFragment;
import Fragment.QuizFragment;
import Fragment.StudyMaterialFragment;
import Fragment.WebResourceFragment;
import Model.EntranceExamTopicModel;
import Model.TopicModel;
import Model.URCModel;
import RestInteraction.RESTInteraction;
import Utils.ApiUrl;
import Utils.Utils;
import adapter.ViewPagerAdapter;
import com.facebook.login.LoginManager;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    RESTInteraction restInteraction;
    ArrayList<MenuItem> menuItems = new ArrayList<>();
    Menu menu;
    String tag_json_obj = "json_obj_req";
    ProgressDialog pDialog;
    String TAG = "MainActivity";
    Toolbar toolbar;
    SharedPreferences sharedPreferences;

    private static final int REQUEST_READ_CONTACTS = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        AppEventsLogger.activateApp(this);
        setContentView(R.layout.activity_home);
        getTopics();
        getentranceExam();
        restInteraction = RESTInteraction.getInstance(this);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

         toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(8);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        populateAutoComplete();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
         menu = navigationView.getMenu();
        View headerView=navigationView.getHeaderView(0);
        RelativeLayout nav_header_container = (RelativeLayout)headerView.findViewById(R.id.nav_header_container);
        nav_header_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.getString("userid", "").length()>0){
                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.START);
                    Utils.isFriends = true;
                    startActivity(new Intent(HomeActivity.this, OwnProfileScreen.class));
                }else{
                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.START);
                    startActivity(new Intent(HomeActivity.this, LaunchingActivity.class));
                }
            }
        });
        TextView tvName = (TextView)headerView.findViewById(R.id.tvName);
        ImageView ic_profile = (ImageView)headerView.findViewById(R.id.ivProfilePic);

        if (sharedPreferences.getString("userid", "").length()>0){
            tvName.setText(sharedPreferences.getString("name", "")+" "+sharedPreferences.getString("sname", ""));
            if (sharedPreferences.getString("image", "").length()>0){
                String url = "https://ammhu.com/images/"+sharedPreferences.getString("image", "");
                url=url.replaceAll(" ", "%20");
                Picasso.with(HomeActivity.this)
                        .load(url)
                        .placeholder(R.mipmap.photo)
                        .into(ic_profile);
            }
        }


       // new GetAllTopics().execute();
       // new GetEntranceExam().execute();

    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            Log.d("permission granted", "no");
            return;
        }else{
            Log.d("permission granted", "yes");
        }

    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
            Snackbar.make(tabLayout, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Log.d("permission granted", "yes");
                populateAutoComplete();
            }else{
                // Log.d("permission granted", "no");

            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(sharedPreferences.getString("userid", "").length()>0){
            getUserTopics();
        }
    }

    void getTopics(){
//       String url = "http://elogic.southindia.cloudapp.azure.com:8083/transtruck/admindashboard/tables/get/childadminshowList.php?pageno=1&limit=10";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getAllTopicUrl, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
//                        pDialog.hide();
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("topic");
                                Utils.topicModels.clear();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    Utils.topicModels.add(new TopicModel(jsonObject.optString("id"),
                                            jsonObject.optString("name"), 0));
                                }
                                setSideBar();
                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }catch(IndexOutOfBoundsException  exception) {
                           Log.d("error : ", exception.getMessage());
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
//                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    void setSideBar(){
        List<String> items = Arrays.asList(sharedPreferences.getString("topics", "").split("\\s*,\\s*"));
        HashSet<String> hashSet = new HashSet<String>();
        hashSet.addAll(items);
        items = new ArrayList<>();
        items.addAll(hashSet);
        Log.d("user size : ", items.size()+"");
        Log.d("topic size : ", Utils.topicModels.size()+"");
        menu.clear();
        menuItems.clear();
        if (items != null && items.size()>1){

            for (int i = 0; i < Utils.topicModels.size(); i++) {
                for (int j = 0; j < items.size(); j++) {


                    if (items.get(j).replaceAll("'", "").trim().equalsIgnoreCase(Utils.topicModels.get(i).getId().trim())){
                        Log.d("items : ", items.get(j).replaceAll("'", ""));
                        Log.d("topics : ", Utils.topicModels.get(i).getId());
                        Log.d("item id : ", j+"");
                        Log.d("topic id : ", i+"");
                        menu.add(Utils.topicModels.get(i).getTopicName());


                    }
                }
            }
            Log.d("menu size : ", menu.size()+"");
            for (int i = 0; i < menu.size(); i++) {
                menuItems.add(menu.getItem(i));
            }

        }else {
            for (int i = 0; i < 20; i++) {
                menu.add(Utils.topicModels.get(i).getTopicName());
                menuItems.add(menu.getItem(i));
            }
        }
    }

    void getUserTopics(){
        Log.d("url", ApiUrl.getUserTopicUrl);
//       String url = "http://elogic.southindia.cloudapp.azure.com:8083/transtruck/admindashboard/tables/get/childadminshowList.php?pageno=1&limit=10";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getUserTopicUrl+"user_id="+sharedPreferences.getString("userid", ""), null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
//                        pDialog.hide();
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                sharedPreferences.edit().putString("topics", "").apply();
                                if (jsonObj.optString("topics").length()>0) {
                                    String temp = "81,"+jsonObj.optString("topics");
                                    sharedPreferences.edit().putString("topics", temp).apply();
                                }else{
                                    sharedPreferences.edit().putString("topics", jsonObj.optString("topics")).apply();
                                }
                                setSideBar();
                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }catch(IndexOutOfBoundsException  exception) {
                            Log.d("error : ", exception.getMessage());
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
//                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    void getentranceExam(){
//       String url = "http://elogic.southindia.cloudapp.azure.com:8083/transtruck/admindashboard/tables/get/childadminshowList.php?pageno=1&limit=10";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getEntranceExamUrl, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
//                        pDialog.hide();
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("exam");
                                Utils.entranceExamTopicModels.clear();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    Utils.entranceExamTopicModels.add(new EntranceExamTopicModel(jsonObject.optString("id"),
                                            jsonObject.optString("name")));
                                }
                            }else {
                                Utils.message = jsonObj.getString("message");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
//                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }


    public class GetAllTopics extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String catid;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            progressDialog = new ProgressDialog(CategoryScreen.this);
//            progressDialog.setCancelable(false);
//            progressDialog.setTitle("Please wait ...");
//            progressDialog.setMessage("Signin ...");
//            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.getAllTopics(ApiUrl.getAllTopicUrl);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//            progressDialog.dismiss();
            if (result.equals("success")) {

                for (int i = 0; i <Utils.topicModels.size(); i++) {
//                    menu.add(Utils.topicModels.get(i).getTopicName());
                    menu.add(0,Integer.parseInt(Utils.topicModels.get(i).getId()), i, Utils.topicModels.get(i).getTopicName());
                    menuItems.add( menu.getItem(i));

                }

            }else if (result.equalsIgnoreCase("fail")) {
//                Toast.makeText(LaunchingActivity.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
//                Toast.makeText(LaunchingActivity.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class GetEntranceExam extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            return restInteraction.getEntranceExamTopics(ApiUrl.getEntranceExamUrl);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        if (getSupportFragmentManager().getFragments() != null){
            getSupportFragmentManager().getFragments().clear();
        }
        adapter.addFragment(new HomeFragment(), "Home");
        adapter.addFragment(new WebResourceFragment(), "Web Resources");
        adapter.addFragment(new StudyMaterialFragment(), "Study Material");
        adapter.addFragment(new GalleryFragment(), "Gallery");
            adapter.addFragment(new QuizFragment(), "Quiz");
            adapter.addFragment(new ExamPaperFragment(), "Exam Papers");
            adapter.addFragment(new ProjextFragment(), "Projects");
            adapter.addFragment(new EntranceExamFragment(), "Entrance Exams");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        String s = item.getTitle()+"";
        String id = "";
        for (int i = 0; i < Utils.topicModels.size(); i++) {
            if (s.equalsIgnoreCase(Utils.topicModels.get(i).getTopicName())){
                id = Utils.topicModels.get(i).getId();
            }
        }
        HomeFragment.getpo(id);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        MenuItem item = menu.findItem(R.id.action_logout);
        if (sharedPreferences.getString("userid", "").length()>0) {
            item.setVisible(true);
        }else{
            item.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            startActivity(new Intent(HomeActivity.this, SearchMemberScreen.class));
            return true;
        }if (id == R.id.action_settings) {
            if (sharedPreferences.getString("userid", "").length()>0) {
                startActivity(new Intent(HomeActivity.this, FriendRequestScreen.class));
            }else{
                startActivity(new Intent(HomeActivity.this, LaunchingActivity.class));
            }
            return true;
        }if (id == R.id.action_noti) {
            if (sharedPreferences.getString("userid", "").length()>0) {
                startActivity(new Intent(HomeActivity.this, NotificationScreen.class));
            }else{
                startActivity(new Intent(HomeActivity.this, LaunchingActivity.class));
            }
            return true;
        } if (id == R.id.action_fflow) {
            startActivity(new Intent(HomeActivity.this, FFlowScreen.class));
            return true;
        }
        if (id == R.id.action_new) {
            startActivity(new Intent(HomeActivity.this, WebViewScreen.class).putExtra("url", "https://ammhu.com/whatsnew"));
            return true;
        }if (id == R.id.action_feedback) {
            startActivity(new Intent(HomeActivity.this, WebViewScreen.class).putExtra("url", "https://ammhu.com/testimonial"));
            return true;
        }if (id == R.id.action_searchmain) {
            startActivity(new Intent(HomeActivity.this, SearchAllScreen.class));
            return true;
        }if (id == R.id.action_ebook) {
            Utils.isEbook = false;
            startActivity(new Intent(HomeActivity.this, EbookScreen.class));
            return true;
        }if (id == R.id.action_ebook2) {
            Utils.isEbook = true;
            startActivity(new Intent(HomeActivity.this, EbookScreen.class));
            return true;
        }if (id == R.id.action_contactus) {
            startActivity(new Intent(HomeActivity.this, ContactUsScreen.class));
            return true;
        }if (id == R.id.action_group) {
            if (sharedPreferences.getString("userid", "").length()>0) {
                startActivity(new Intent(HomeActivity.this, AllGroupScreen.class));
            }else{
                startActivity(new Intent(HomeActivity.this, LaunchingActivity.class));
            }
            return true;
        }if (id == R.id.action_logout) {
            sharedPreferences.edit().putString("email", "").apply();
            sharedPreferences.edit().putString("password", "").apply();
            sharedPreferences.edit().putString("userid", "").apply();
            sharedPreferences.edit().putString("name", "").apply();
            sharedPreferences.edit().putString("image", "").apply();
            sharedPreferences.edit().putString("email", "").apply();
            sharedPreferences.edit().putString("dob", "").apply();
            sharedPreferences.edit().putString("address", "").apply();
            sharedPreferences.edit().putString("phone", "").apply();
            sharedPreferences.edit().putString("country", "").apply();
            sharedPreferences.edit().putString("state", "").apply();
            sharedPreferences.edit().putString("city", "").apply();
            sharedPreferences.edit().putString("sname", "").apply();
            sharedPreferences.edit().putString("topics", "").apply();
            if (Utils.mGoogleApiClient != null && Utils.mGoogleApiClient.isConnected()) {
                Plus.AccountApi.clearDefaultAccount(Utils.mGoogleApiClient);
                Utils.mGoogleApiClient.disconnect();
                Utils.mGoogleApiClient.connect();
            }
            LoginManager.getInstance().logOut();
            Intent intent = new Intent(HomeActivity.this, LaunchingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
