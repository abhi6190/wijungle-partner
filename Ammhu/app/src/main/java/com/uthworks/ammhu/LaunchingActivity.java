package com.uthworks.ammhu;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import RestInteraction.RESTInteraction;
import Utils.Utils;
import Utils.ApiUrl;

public class LaunchingActivity extends AppCompatActivity implements  GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    CallbackManager callbackManager;
    Button gplusLogin;
    private boolean mIntentInProgress;
    private boolean mSignInClicked;
    private ConnectionResult mConnectionResult;
    private static final int RC_SIGN_IN = 0;
    TextView tvSkip, tvNewUserClick;
    RESTInteraction restInteraction;
    SharedPreferences sharedPreferences;
    String tag_json_obj = "json_obj_req";
    String TAG = "OwnProfileScreen";
    ProgressDialog pDialog;
    String password = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        AppEventsLogger.activateApp(this);
        setContentView(R.layout.activity_launching);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        restInteraction = RESTInteraction.getInstance(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        callbackManager = CallbackManager.Factory.create();
        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        tvSkip = (TextView)findViewById(R.id.tvSkip);
        tvNewUserClick = (TextView)findViewById(R.id.tvNewUserClick);
        LoginButton loginButton = (LoginButton)findViewById(R.id.login_button);
        loginButton.setReadPermissions("email");
        getLoginDetails(loginButton);


        Utils.mGoogleApiClient = new GoogleApiClient.Builder(LaunchingActivity.this)
                .addConnectionCallbacks(LaunchingActivity.this)
                .addOnConnectionFailedListener(LaunchingActivity.this).addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();

        gplusLogin = (Button) findViewById(R.id.gplusLogin);
        gplusLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInWithGplus();
            }
        });

        String htmlString="<u>Skip</u>";
        tvSkip.setText(Html.fromHtml(htmlString));
        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LaunchingActivity.this, HomeActivity.class));
            }
        });
        tvNewUserClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LaunchingActivity.this, SignupScreen.class));
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });


    }

    protected void onStart() {
        super.onStart();
        Utils.mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this, 0).show();
            return;
        }

        if (!mIntentInProgress) {

            mConnectionResult = result;

            if (mSignInClicked) {

                resolveSignInError();
            }
        }

    }

    @Override
    public void onConnected(Bundle arg0) {
        mSignInClicked = false;
        getProfileInformation();
        //		startActivity(new Intent(MainActivity.this, Menunew.class));
        //		finish();

    }

    @Override
    public void onConnectionSuspended(int arg0) {
        Utils.mGoogleApiClient.connect();
        //	    updateUI(false);
    }

    /**
     * Sign-in into google
     * */
    private void signInWithGplus() {
        if (!Utils.mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            resolveSignInError();
        }
    }

    /**
     * Method to resolve any signin errors
     * */
    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                Utils.mGoogleApiClient.connect();
            }
        }
    }

    private void getProfileInformation() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(Utils.mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi.getCurrentPerson(Utils.mGoogleApiClient);
                String checkId = currentPerson.getId();
                String name = currentPerson.getDisplayName();
                String personPhotoUrl = currentPerson.getImage().getUrl();
                String personGooglePlusProfile = currentPerson.getUrl();
                String email = Plus.AccountApi.getAccountName(Utils.mGoogleApiClient);
                String image = "";
                if (personPhotoUrl != null){
                    image = personPhotoUrl;
                }else if(personGooglePlusProfile != null){
                    image = personGooglePlusProfile;
                }
                String url = ApiUrl.socialLoginUrl + "email=" + URLEncoder.encode(email)
                        + "&name=" + URLEncoder.encode(name) + "&gcmid="+Utils.gcmId
                        +"&type=gplus&image="+URLEncoder.encode(image)+"&social_user_id="+checkId;
                SignUp(url);
//                new UpdateNewUsersToDBAsync(checkId, name, email, "gplus").execute();
//                startActivity(new Intent(LoginActivity.this, GetNumberScreen.class).putExtra("profileId", checkId)
//                        .putExtra("name", name).putExtra("email", email));
               // startActivity(new Intent(LaunchingActivity.this, HomeActivity.class));
                Log.e("Restaurant", "Name: " + name + ", plusProfile: "
                        + personGooglePlusProfile + ", email: " + email
                        + ", Image: " + personPhotoUrl);

            } else {
//                Toast.makeText(getApplicationContext(),
//                        "Person information is null", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void signOutFromGplus() {
        if (Utils.mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(Utils.mGoogleApiClient);
            Utils.mGoogleApiClient.disconnect();
            Utils.mGoogleApiClient.connect();
//            Utils.check = 0;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        super.onActivityResult(requestCode, responseCode, intent);
        callbackManager.onActivityResult(requestCode, responseCode, intent);
        if (requestCode == RC_SIGN_IN) {
            if (responseCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (Utils.mGoogleApiClient != null && !Utils.mGoogleApiClient.isConnecting()) {
                Utils.mGoogleApiClient.connect();
            }
        }
    }

    protected void getLoginDetails(LoginButton login_button){
        login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult login_result) {
                getUserInfo(login_result);
                Log.d("success", "hahaha");
            }

            @Override
            public void onCancel() {
                Log.d("cancel", "hahaha");
            }

            @Override
            public void onError(FacebookException exception) {
                Log.d("error", "hahaha");
            }
        });
    }

    protected void getUserInfo(LoginResult login_result){

        GraphRequest data_request = GraphRequest.newMeRequest(
                login_result.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject json_object,
                            GraphResponse response) {

                        try {
//                            profile_pic_data = json_object.get("picture").toString();
                            String profile_pic_url = json_object.getJSONObject("picture").getJSONObject("data").getString("url");
                            Log.d("email", json_object.opt("email").toString());
                            Log.d("name", json_object.opt("name").toString());
                            String url = ApiUrl.socialLoginUrl + "email=" + URLEncoder.encode(json_object.opt("email").toString())
                                    + "&name=" + URLEncoder.encode(json_object.opt("name").toString()) + "&gcmid="+Utils.gcmId
                                    +"&type=fb&image="+URLEncoder.encode(profile_pic_url)+"&social_user_id="+json_object.opt("id").toString();
                            SignUp(url);
                            //startActivity(new Intent(LaunchingActivity.this, HomeActivity.class));
//                            sharedPreferences.edit().putString("image", profile_pic_url).commit();
//                            new LoginnAsync(json_object.get("email").toString(), "",
//                                    gcmId, LoginActivity.this, "2").execute();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "id,name,email,picture.width(120).height(120)");
        data_request.setParameters(permission_param);
        data_request.executeAsync();
    }

    private void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
         password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }
        if (!cancel){

//            new LoginAsync(email, password).execute();
            String url = ApiUrl.loginUrl + "email=" + URLEncoder.encode(email)
                    + "&password=" + URLEncoder.encode(password) + "&gcmid="+Utils.gcmId;
            SignUp(url);
//            startActivity(new Intent(LaunchingActivity.this, HomeActivity.class));
//            finish();
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            Log.d("pass", hexString.toString());
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    void SignUp(String url){
        Log.d("url", url);
        pDialog = new ProgressDialog(LaunchingActivity.this);
        pDialog.setTitle("Please wait ...");
        pDialog.setMessage("Signin ...");
        pDialog.setCancelable(false);
        pDialog.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        pDialog.hide();
                        if (jsonObj.optString("status").equals("success")){
                            sharedPreferences.edit().putString("userid", jsonObj.optString("id")).apply();
                            sharedPreferences.edit().putString("name", jsonObj.optString("name")).apply();
                            sharedPreferences.edit().putString("image", jsonObj.optString("image")).apply();
                            sharedPreferences.edit().putString("email", jsonObj.optString("email")).apply();
                            sharedPreferences.edit().putString("dob", jsonObj.optString("dob")).apply();
                            sharedPreferences.edit().putString("address", jsonObj.optString("address")).apply();
                            sharedPreferences.edit().putString("phone", jsonObj.optString("phone")).apply();
                            sharedPreferences.edit().putString("country", jsonObj.optString("country")).apply();
                            sharedPreferences.edit().putString("state", jsonObj.optString("state")).apply();
                            sharedPreferences.edit().putString("city", jsonObj.optString("city")).apply();
                            sharedPreferences.edit().putString("sname", jsonObj.optString("sname")).apply();
                            if (jsonObj.optString("topics").length()>0) {
                                String temp = "81,"+jsonObj.optString("topics");
                                sharedPreferences.edit().putString("topics", temp).apply();
                                sharedPreferences.edit().putInt("topicslengthold", temp.length()).apply();
                            }else{
                                sharedPreferences.edit().putString("topics", jsonObj.optString("topics")).apply();
                                sharedPreferences.edit().putInt("topicslengthold", jsonObj.optString("topics").length()).apply();
                            }

                            sharedPreferences.edit().putString("password",  password).apply();
                            Intent intent = new Intent(LaunchingActivity.this, HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }else{
                            Toast.makeText(LaunchingActivity.this, jsonObj.optString("message"), Toast.LENGTH_SHORT).show();
                        }
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public class LoginAsync extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String email; String pass;
        public LoginAsync(String email, String pass) {
            this.email =email;
            this.pass = pass;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(LaunchingActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Please wait ...");
            progressDialog.setMessage("Signin ...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.loginuser(ApiUrl.loginUrl + "email=" + URLEncoder.encode(email)
                    + "&password=" + URLEncoder.encode(pass) + "&gcmid="+Utils.gcmId);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("success")) {
                sharedPreferences.edit().putString("email", email).apply();
                sharedPreferences.edit().putString("password", pass).apply();
                Intent intent = new Intent(LaunchingActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }else if (result.equalsIgnoreCase("fail")) {
                Toast.makeText(LaunchingActivity.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(LaunchingActivity.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }


}
