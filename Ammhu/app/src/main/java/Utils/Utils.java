package Utils;

import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.List;

import Model.ChapterModel;
import Model.ClassModel;
import Model.EntranceExamTopicModel;
import Model.EntrancePaperModel;
import Model.ExamCategoryModel;
import Model.ExamPaperModel;
import Model.FFlowModel;
import Model.FollowerModel;
import Model.FollowingModel;
import Model.FriendRequestModel;
import Model.FriendsModel;
import Model.GalleryCommentModel;
import Model.ImageModel;
import Model.LinkModel;
import Model.PDFModel;
import Model.PostModel;
import Model.QuizModel;
import Model.TopicModel;
import Model.URCModel;
import Model.UserModel;
import Model.VideoModel;

/**
 * Created by VARIABLE on 9/24/2016.
 */
public class Utils {

    public static String message = "";
    public static String likeCount = "0";
    public static String unlikeCount = "0";
    public static boolean isEbook = false;
    public static boolean isFriends = false;
    public static String urcId = "";
    public static String topicId = "";
    public static String status = "1";
    public static String beep = "1";
    public static String gcmId = "";
    public static ArrayList<TopicModel> topicModels = new ArrayList<>();
    public static List<UserModel> userModels = new ArrayList<>();
    public static ArrayList<FriendRequestModel> friendRequestModels = new ArrayList<>();
    public static ArrayList<ExamPaperModel> examPaperModels = new ArrayList<>();
    public static ArrayList<FriendsModel> friendsModels = new ArrayList<>();
    public static ArrayList<QuizModel> quizModels = new ArrayList<>();
    public static ArrayList<FFlowModel> fFlowModels = new ArrayList<>();
    public static List<ExamCategoryModel> examCategoryModels = new ArrayList<>();
    public static ArrayList<EntrancePaperModel> entrancePaperModels = new ArrayList<>();
    public static ArrayList<PostModel> postModels = new ArrayList<>();
    public static ArrayList<ClassModel> classModels = new ArrayList<>();
    public static ArrayList<EntranceExamTopicModel> entranceExamTopicModels = new ArrayList<>();
    public static ArrayList<GalleryCommentModel> galleryCommentModels = new ArrayList<>();
    public static ArrayList<URCModel> urcModels = new ArrayList<>();
    public static ArrayList<VideoModel> videoModels = new ArrayList<>();
    public static ArrayList<LinkModel> linkModels = new ArrayList<>();
    public static ArrayList<ChapterModel> chapterModels = new ArrayList<>();
    public static ArrayList<PDFModel> pdfModels = new ArrayList<>();
    public static ArrayList<ImageModel> imageModels = new ArrayList<>();
    public static ArrayList<FollowerModel> followerModels = new ArrayList<>();
    public static ArrayList<FollowingModel> followingModels = new ArrayList<>();
    public static GoogleApiClient mGoogleApiClient;
}
