package Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.squareup.picasso.Picasso;
import com.uthworks.ammhu.FullImageScreen;
import com.uthworks.ammhu.R;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import Model.ImageModel;
import RestInteraction.RESTInteraction;
import Utils.Utils;
import Utils.ApiUrl;

/**
 * Created by VARIABLE on 10/11/2016.
 */
public class GalleryFragment extends Fragment {

    MyAdapter myAdapter;
    RESTInteraction restInteraction;
    Spinner  spinner1;
    List<String> categories1 = new ArrayList<String>();
    View view;
    GridView gridGallery;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.gallery_layout, container, false);
        restInteraction = RESTInteraction.getInstance(getActivity());

        gridGallery = (GridView)view.findViewById(R.id.gridGallery);
        gridGallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(getActivity(), FullImageScreen.class).putExtra("pos", Utils.imageModels.get(position)));
            }
        });
        spinner1 = (Spinner) view.findViewById(R.id.spinner1);


        for (int i = 0; i < Utils.urcModels.size(); i++) {
            categories1.add(Utils.urcModels.get(i).getName());
        }

        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories1);

        // Drop down layout style - list view with radio button
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner1.setAdapter(dataAdapter1);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Utils.urcId = Utils.urcModels.get(position).getId();
                new GetAllImages(Utils.urcId, "0").execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return  view;
    }

    public class GetAllImages extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String catid;
        String topicId;

        public GetAllImages(String catid, String topicId){
            this.catid = catid;
            this.topicId = topicId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            progressDialog = new ProgressDialog(CategoryScreen.this);
//            progressDialog.setCancelable(false);
//            progressDialog.setTitle("Please wait ...");
//            progressDialog.setMessage("Signin ...");
//            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.getAllImages(ApiUrl.getAllImagesUrl + "cat_id=" + catid + "&topicid=" +topicId);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//            progressDialog.dismiss();
            if (result.equals("success")) {
                if(getActivity() !=null) {
                    myAdapter = new MyAdapter(getActivity(), Utils.imageModels);
                    gridGallery.setAdapter(myAdapter);
                }
            }else if (result.equalsIgnoreCase("fail")) {
//                Toast.makeText(LaunchingActivity.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
//                Toast.makeText(LaunchingActivity.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    class MyAdapter extends BaseAdapter {
        LayoutInflater layoutInflater;
        Context context;
        ArrayList<ImageModel> items;
        public MyAdapter(Context context, ArrayList<ImageModel> imageModels){
            this.context = context;
            items = imageModels;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View mView = null;
            if (mView == null){
                mView = layoutInflater.inflate(R.layout.grid_list_items, parent, false);
            }
            ImageView ivgalleryImage = (ImageView)mView.findViewById(R.id.ivgalleryImage);
            TextView ivgalleryImageName = (TextView)mView.findViewById(R.id.ivgalleryImageName);
            String url = "https://ammhu.com/uploads/"+items.get(position).getSrc();
            url=url.replaceAll(" ", "%20");
            Log.d("imageurl", url);
            Picasso.with(getActivity())
                    .load(url.trim())
                    .into(ivgalleryImage);
//            UrlImageViewHelper.setUrlDrawable(ivgalleryImage, url);
            ivgalleryImageName.setText(items.get(position).getTitle());

            return mView;
        }
    }


}
