package Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.uthworks.ammhu.R;
import com.uthworks.ammhu.ShowVideos;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import Model.LinkModel;
import RestInteraction.RESTInteraction;
import Utils.ApiUrl;
import Utils.Utils;

/**
 * Created by VARIABLE on 10/11/2016.
 */
public class WebResourceFragment extends Fragment {

    View view;
    ListView listView;
    MyAdapter myAdapter;
    String strSpinner = "";
    Spinner spinner, spinner1;
    List<String> categories = new ArrayList<String>();
    List<String> categories1 = new ArrayList<String>();
    RESTInteraction restInteraction;
    EditText etVideoLink, etVideoDesc;
    Button btnSubmitVideo, btnShowVideos;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.webresources, container, false);
        listView = (ListView)view.findViewById(R.id.listViewOnlineRes);

        restInteraction = RESTInteraction.getInstance(getActivity());
        spinner = (Spinner)view.findViewById(R.id.spinner);
        spinner1 = (Spinner)view.findViewById(R.id.spinner1);
        etVideoLink = (EditText)view.findViewById(R.id.etVideoLink);
        etVideoDesc = (EditText)view.findViewById(R.id.etVideoDesc);
        btnSubmitVideo = (Button)view.findViewById(R.id.btnSubmitVideo);
        btnShowVideos = (Button)view.findViewById(R.id.btnShowVideos);
        btnSubmitVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etVideoLink.getText().toString().trim().length() == 0) {
                    Toast.makeText(getActivity(), "Please enter Video link.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (etVideoDesc.getText().toString().trim().length() == 0) {
                    Toast.makeText(getActivity(), "Please enter description.", Toast.LENGTH_SHORT).show();
                    return;
                }
                new SendVideo(Utils.urcId, Utils.topicId, URLEncoder.encode(etVideoLink.getText().toString().trim()),
                        URLEncoder.encode(etVideoDesc.getText().toString().trim())).execute();
            }
        });

        btnShowVideos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            startActivity(new Intent(getActivity(), ShowVideos.class).putExtra("catid", Utils.urcId));

            }
        });
//        new GetAllLinks(Utils.urcModels.get(0).getId()).execute();

        new GetAllTopics().execute();




        for (int i = 0; i < Utils.urcModels.size(); i++) {
            categories1.add(Utils.urcModels.get(i).getName());
        }


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Utils.topicId = Utils.topicModels.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories1);

        // Drop down layout style - list view with radio button
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner1.setAdapter(dataAdapter1);

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Utils.urcId = Utils.urcModels.get(position).getId();
                new GetAllLinks(Utils.urcModels.get(position).getId()).execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return view;
    }

    public class GetAllTopics extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String catid;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            progressDialog = new ProgressDialog(CategoryScreen.this);
//            progressDialog.setCancelable(false);
//            progressDialog.setTitle("Please wait ...");
//            progressDialog.setMessage("Signin ...");
//            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.getAllTopics(ApiUrl.getAllTopicUrl);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//            progressDialog.dismiss();
            if (result.equals("success")) {

                if(getActivity() != null) {
                    for (int i = 0; i < Utils.topicModels.size(); i++) {
                        categories.add(Utils.topicModels.get(i).getTopicName());
                    }
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);

                    // Drop down layout style - list view with radio button
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    // attaching data adapter to spinner
                    spinner.setAdapter(dataAdapter);
                }

            }else if (result.equalsIgnoreCase("fail")) {
//                Toast.makeText(LaunchingActivity.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
//                Toast.makeText(LaunchingActivity.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class GetAllLinks extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String catid;

        public GetAllLinks(String catid){
            this.catid = catid;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            progressDialog = new ProgressDialog(CategoryScreen.this);
//            progressDialog.setCancelable(false);
//            progressDialog.setTitle("Please wait ...");
//            progressDialog.setMessage("Signin ...");
//            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.getAllLink(ApiUrl.getAllLinkUrl + "category_id=" + catid);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//            progressDialog.dismiss();
            if (result.equals("success")) {
                if (getActivity() !=null) {
                    myAdapter = new MyAdapter(getActivity(), Utils.linkModels);
                    listView.setAdapter(myAdapter);
                }
            }else if (result.equalsIgnoreCase("fail")) {
//                Toast.makeText(LaunchingActivity.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
//                Toast.makeText(LaunchingActivity.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public class SendVideo extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String catid;
        String topicid;
        String videoLink;
        String videoDesc;

        public SendVideo(String catid, String topicid, String videoLink, String videoDesc){
            this.catid = catid;
            this.topicid = topicid;
            this.videoLink = videoLink;
            this.videoDesc = videoDesc;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Please wait ...");
            progressDialog.setMessage("Sending Video link ...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.common(ApiUrl.sendVideoUrl + "cat_id=" + catid + "&topic_id=" + topicid+"&user_id=41"
                    + "&video_link=" + videoLink + "&description=" + videoDesc);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("success")) {
                Toast.makeText(getActivity(), "Video link sent succesfully.", Toast.LENGTH_SHORT).show();
            }else if (result.equalsIgnoreCase("fail")) {
//                Toast.makeText(LaunchingActivity.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
//                Toast.makeText(LaunchingActivity.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }



    class MyAdapter extends BaseAdapter{
        LayoutInflater layoutInflater;
        Context context;
        ArrayList<LinkModel> linkModels;
        public MyAdapter(Context context, ArrayList<LinkModel> linkModels){
            this.context = context;
            this.linkModels = linkModels;
            if (context!=null) {
                layoutInflater = LayoutInflater.from(context);
            }
        }

        @Override
        public int getCount() {
            return linkModels.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View mView = null;
            if (mView == null){
                mView = layoutInflater.inflate(R.layout.webresource_list_items, parent, false);
            }
            TextView tvLink = (TextView)mView.findViewById(R.id.tvLink);
            tvLink.setText(Html.fromHtml(linkModels.get(position).getName()));
            tvLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(linkModels.get(position).getUrl()));
                    startActivity(browserIntent);
                }
            });
            return mView;
        }
    }
}
