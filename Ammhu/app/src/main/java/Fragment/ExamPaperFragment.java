package Fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.Spinner;

import com.uthworks.ammhu.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import RestInteraction.RESTInteraction;
import Utils.Utils;
import Utils.ApiUrl;
import adapter.ExamPaperAdapter;
import adapter.PastOrderAdapter;
import adapter.ProjectAdapter;

/**
 * Created by VARIABLE on 10/11/2016.
 */
public class ExamPaperFragment extends Fragment {
    View view;
    Spinner spinner1;
    List<String> categories1 = new ArrayList<String>();
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    ExamPaperAdapter listAdapter;
    RESTInteraction restInteraction;
    HashMap<String, List<String>> listDataChildId;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.study_material_layout, container, false);
        expListView = (ExpandableListView) view.findViewById(R.id.lvExp);
        restInteraction = RESTInteraction.getInstance(getActivity());
        //prepareListData();
//        listAdapter = new PastOrderAdapter(getActivity(), listDataHeader, listDataChild);
////       // setting list adapter
//        expListView.setAdapter(listAdapter);
        spinner1 = (Spinner)view.findViewById(R.id.spinner1);

        for (int i = 0; i < Utils.urcModels.size(); i++) {
            categories1.add(Utils.urcModels.get(i).getName());
        }
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories1);

        // Drop down layout style - list view with radio button
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner1.setAdapter(dataAdapter1);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Utils.urcId = Utils.urcModels.get(position).getId();
                Utils.chapterModels.clear();
                for (int i = 0; i < Utils.classModels.size(); i++) {
                    Log.d("iiiiii", i+"");
                    new GetAllExamPaper(Utils.urcModels.get(position).getId(), Utils.classModels.get(i).getId()).execute();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return view;
    }

    public class GetAllExamPaper extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String classid, categoryid;

        public GetAllExamPaper(String categoryid, String classid){
            this.categoryid = categoryid;
            this.classid = classid;
        }


        @Override
        protected String doInBackground(String... params) {
            return restInteraction.getExamPaper(ApiUrl.getExamPaperUrl + "category_id=" + categoryid + "&class_id=" + classid);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//            progressDialog.dismiss();
            if (result.equals("success")) {
//                if (i == Utils.classModels.size()-1) {
                prepareListData();
//                }
            }else if (result.equalsIgnoreCase("fail")) {
//                Toast.makeText(LaunchingActivity.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
//                Toast.makeText(LaunchingActivity.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        listDataChildId = new HashMap<String, List<String>>();
        for (int i = 0; i < Utils.classModels.size(); i++) {
            List<String> nine1 = new ArrayList<String>();
            nine1.clear();
            List<String> ids = new ArrayList<String>();
            ids.clear();
            listDataHeader.add(Utils.classModels.get(i).getName());
            for (int j = 0; j < Utils.examPaperModels.size(); j++) {
                if (Utils.classModels.get(i).getId().equals(Utils.examPaperModels.get(j).getClass_id())){
                    if(!nine1.contains(Utils.examPaperModels.get(j).getType())) {
                        nine1.add(Utils.examPaperModels.get(j).getType());
                        ids.add(Utils.examPaperModels.get(j).getClass_id());
                    }
                }
            }
            listDataChild.put(listDataHeader.get(i), nine1);
            listDataChildId.put(listDataHeader.get(i), ids);
        }
        if (getActivity() !=null) {
            listAdapter = new ExamPaperAdapter(getActivity(), listDataHeader, listDataChild, listDataChildId);
////       // setting list adapter
            expListView.setAdapter(listAdapter);
        }
    }

}
