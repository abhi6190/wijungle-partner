package Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.uthworks.ammhu.R;
import com.uthworks.ammhu.ShowVideos;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import Model.EntrancePaperModel;
import Model.QuizModel;
import RestInteraction.RESTInteraction;
import Utils.Utils;
import Utils.ApiUrl;

/**
 * Created by VARIABLE on 10/11/2016.
 */
public class QuizFragment extends Fragment {

    View view;
    ListView listView;
    MyAdapter myAdapter;
    String strSpinner = "";
    Spinner spinner1;
    List<String> categories = new ArrayList<String>();
    RESTInteraction restInteraction;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.quiz_fragment, container, false);
        listView = (ListView)view.findViewById(R.id.listViewOnlineRes);

        restInteraction = RESTInteraction.getInstance(getActivity());
        spinner1 = (Spinner)view.findViewById(R.id.spinner1);

        for (int i = 0; i < Utils.urcModels.size(); i++) {
            categories.add(Utils.urcModels.get(i).getName());
        }
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner1.setAdapter(dataAdapter1);

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Utils.urcId = Utils.urcModels.get(position).getId();
                new GetAllQuiz(Utils.urcModels.get(position).getId()).execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return view;
    }

    public class GetAllQuiz extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String catid;

        public GetAllQuiz(String catid){
            this.catid = catid;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            progressDialog = new ProgressDialog(CategoryScreen.this);
//            progressDialog.setCancelable(false);
//            progressDialog.setTitle("Please wait ...");
//            progressDialog.setMessage("Signin ...");
//            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.getAllQuiz(ApiUrl.getQuizUrl + "category_id=" + catid);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//            progressDialog.dismiss();
            if (result.equals("success")) {
                if (getActivity() !=null) {
                    myAdapter = new MyAdapter(getActivity(), Utils.quizModels);
                    listView.setAdapter(myAdapter);
                }
            }else if (result.equalsIgnoreCase("fail")) {
                if (getActivity() !=null) {
                    myAdapter = new MyAdapter(getActivity(), Utils.quizModels);
                    listView.setAdapter(myAdapter);
                }
//                Toast.makeText(LaunchingActivity.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
//                Toast.makeText(LaunchingActivity.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    class MyAdapter extends BaseAdapter {
        Context context;
        LayoutInflater layoutInflater;
        ArrayList<QuizModel> pdfModels;
        public MyAdapter(Context context, ArrayList<QuizModel> pdfModels){
            this.context = context;
            this.pdfModels = pdfModels;
            layoutInflater = LayoutInflater.from(context);
        }
        @Override
        public int getCount() {
            return pdfModels.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = null;
            if (view == null){
                view = layoutInflater.inflate(R.layout.quiz_list_items, parent, false);
            }
            TextView tvQuestion = (TextView)view.findViewById(R.id.tvQuestion);
            TextView tvDate = (TextView)view.findViewById(R.id.tvDate);
            final TextView tvViewAns = (TextView)view.findViewById(R.id.tvViewAns);
            final TextView tvAnswer = (TextView)view.findViewById(R.id.tvAnswer);
            final View view1 = (View)view.findViewById(R.id.tvAnswer);
            tvViewAns.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (v.getTag().toString().equals("1")){
                        view1.setVisibility(View.VISIBLE);
                        tvAnswer.setVisibility(View.VISIBLE);
                        tvViewAns.setTag("2");
                    }else{
                        view1.setVisibility(View.GONE);
                        tvAnswer.setVisibility(View.GONE);
                        tvViewAns.setTag("1");
                    }
                }
            });
//            tvViewAns.setText(pdfModels.get(position).getDownloads());
            tvQuestion.setText("Q : "+Html.fromHtml(pdfModels.get(position).getQuestion()));
            tvDate.setText(pdfModels.get(position).getPost_date());
            tvAnswer.setText("Ans : "+Html.fromHtml(pdfModels.get(position).getAnswer()));
            return view;
        }
    }
}
