package Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.uthworks.ammhu.AppController;
import com.uthworks.ammhu.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import Model.PostModel;
import Model.URCModel;
import RestInteraction.RESTInteraction;
import Utils.ApiUrl;
import Utils.Utils;
import adapter.FullMenuAdapter;

/**
 * Created by VARIABLE on 10/11/2016.
 */
public class HomeFragment extends Fragment{

    static RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    static RecyclerView.Adapter mAdapter;
    View view;
    RESTInteraction restInteraction;
    static String tag_json_obj = "json_obj_req";
    ProgressDialog pDialog;
    String TAG = "MainActivity";
    static Context context;
    //static LinearLayout llMain;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d("Start", "fragment");
        context = getActivity();
        view= inflater.inflate(R.layout.home_layout, container, false);

        restInteraction = RESTInteraction.getInstance(getActivity());
       // llMain = (LinearLayout)view.findViewById(R.id.llMain);
        mRecyclerView = (RecyclerView)view.findViewById(R.id.recyclerView_listing);
        mRecyclerView.setHasFixedSize(true);
//        llBottom = (LinearLayout)view.findViewById(R.id.llBottom);
//        llBottom.setVisibility(View.VISIBLE);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
//        new GetAllPosts("81").execute();
        getAllPosts("81");

        return view;
    }

    public static void getpo(String id){
        getAllPosts(id);
    }

   static  void getAllPosts(String catId){
        Log.d("url", ApiUrl.getAllPostUrl + "topicid=" + catId);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.getAllPostUrl+"topicid="+catId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
//                        pDialog.hide();
                        try {
                            Utils.postModels.clear();
                            if (jsonObj.getString("status").equals("success")) {
                                JSONArray jsonArray = jsonObj.optJSONArray("posts");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    Utils.postModels.add(new PostModel(jsonObject.optString("id"),
                                            jsonObject.optString("user_id"),
                                            jsonObject.optString("post_title"),
                                            jsonObject.optString("post_des"),
                                            jsonObject.optString("topic_id"),
                                            jsonObject.optString("dd"),
                                            jsonObject.optString("feature_img"),
                                            jsonObject.optString("slug"),
                                            jsonObject.optString("answer_request"),
                                            jsonObject.optString("count")));
                                }
                                if(context !=null) {
                                    //llMain.setVisibility(View.GONE);
                                    mRecyclerView.setVisibility(View.VISIBLE);
                                    mAdapter = new FullMenuAdapter(context, Utils.postModels);
                                    mRecyclerView.setAdapter(mAdapter);
                                }

                            }else {
                                if(context !=null) {
                                    //llMain.setVisibility(View.GONE);
                                    mRecyclerView.setVisibility(View.VISIBLE);
                                    mAdapter = new FullMenuAdapter(context, Utils.postModels);
                                    mRecyclerView.setAdapter(mAdapter);
                                }
                                Utils.message = jsonObj.getString("message");
                                Toast.makeText(context, Utils.message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // hide the progress dialog
//                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }


}
