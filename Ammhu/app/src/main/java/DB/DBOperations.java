package DB;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

public class DBOperations {
	MySQLiteHelper mySQLiteHelper;
	Context context;
	SQLiteDatabase database;
	SharedPreferences sharedPrefs;

	public DBOperations(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
		mySQLiteHelper = MySQLiteHelper.getInstance(context);
		mySQLiteHelper.openDataBase();
		database = mySQLiteHelper.getDatabaseInstance();
	}

//	public long addBloodPressureToDB(String card_id, ContentValues values) {
//		Cursor cursor = database.query(DBConstants.TABLE_LATEST,
//				new String[] { DBConstants.KEY_ID },
//				DBConstants.KEY_ID + " =? ", new String[] { card_id }, null, null, null);
//		if (cursor.getCount() > 0 && cursor.moveToFirst()) {
//			return database.update(DBConstants.TABLE_LATEST, values,
//					DBConstants.KEY_ID + " =? ", new String[] { card_id });
//		} else if (cursor.getCount() == 0) {
//			// If not found then create new one//
//			return database.insert(DBConstants.TABLE_LATEST, null, values);
//		} else {
//			return -1;
//		}
//	}


	public long addChatToDB(ContentValues contentValues){
		return  database.insert(DBConstants.TABLE_CHAT_TABLE, null, contentValues);
//		String query = "SELECT * FROM "
//				+DBConstants.TABLE_CHAT_TABLE
//				+" WHERE ("+DBConstants.COLUMN_SENDER_ID + " = "+ fromId + " AND "+ DBConstants.COLUMN_RECEIVER_ID + " = "+ toId
//				+ " OR "+ DBConstants.COLUMN_SENDER_ID + " = "+ toId + " AND " + DBConstants.COLUMN_RECEIVER_ID + " = "+ fromId+") AND "
//				+ DBConstants.COLUMN_SESSION_FINISHED + " = 0 ORDER BY "+DBConstants.KEY_ID + " DESC";
//
//		Cursor c =  database.rawQuery(query, null);
//		if (c != null && c.getCount()>0 && c.moveToFirst()){
//			String updateQuerry = null;
//			if (c.getString(c.getColumnIndexOrThrow(DBConstants.COLUMN_SENDER_ID)).equals(fromId)){
////				String concatString = "SELECT CONCAT("+c.getString(c.getColumnIndexOrThrow(DBConstants.COLUMN_MESSAGE))+ " , " + message+") WHERE "+DBConstants.;
////				updateQuerry = "UPDATE "+ DBConstants.TABLE_CHAT_TABLE + " SET "+
////						DBConstants.COLUMN_MESSAGE + " = ('"+c.getString(c.getColumnIndexOrThrow(DBConstants.COLUMN_MESSAGE))+ "'|| '"+"<br/>"+"<hr/>"+"<br/>"+message+"'), "+DBConstants.COLUMN_TIME
////						+ " = '"+ time+"'  WHERE "
////						+ DBConstants.KEY_ID + " = "+ c.getString(c.getColumnIndexOrThrow(DBConstants.KEY_ID));
////				Cursor cursor =  database.rawQuery(updateQuerry, null);
//				ContentValues contentValues1 = new ContentValues();
//				contentValues1.put(DBConstants.COLUMN_CHAT_ID, c.getString(c.getColumnIndexOrThrow(DBConstants.KEY_ID)));
//				contentValues1.put(DBConstants.COLUMN_MESSAGE, message);
//				contentValues1.put(DBConstants.COLUMN_TIME, time);
//
//				return database.insert(DBConstants.TABLE_MESSAGE_TABLE, null, contentValues1);
//			}else{
//				long i = database.insert(DBConstants.TABLE_CHAT_TABLE, null, contentValues);
//				String query1 = "SELECT * FROM "
//						+DBConstants.TABLE_CHAT_TABLE
//						+" WHERE ("+DBConstants.COLUMN_SENDER_ID + " = "+ fromId + " AND "+ DBConstants.COLUMN_RECEIVER_ID + " = "+ toId
//						+ " OR "+ DBConstants.COLUMN_SENDER_ID + " = "+ toId + " AND " + DBConstants.COLUMN_RECEIVER_ID + " = "+ fromId+") AND "
//						+ DBConstants.COLUMN_SESSION_FINISHED + " = 0 ORDER BY "+DBConstants.KEY_ID + " DESC";
//				return  i;
//			}
//		}else if (c.getCount() == 0) {
//			// If not found then create new one//
//			long i =  database.insert(DBConstants.TABLE_CHAT_TABLE, null, contentValues);
//			String query1 = "SELECT * FROM "
//					+DBConstants.TABLE_CHAT_TABLE
//					+" WHERE ("+DBConstants.COLUMN_SENDER_ID + " = "+ fromId + " AND "+ DBConstants.COLUMN_RECEIVER_ID + " = "+ toId
//					+ " OR "+ DBConstants.COLUMN_SENDER_ID + " = "+ toId + " AND " + DBConstants.COLUMN_RECEIVER_ID + " = "+ fromId+") AND "
//					+ DBConstants.COLUMN_SESSION_FINISHED + " = 0 ORDER BY "+DBConstants.KEY_ID + " DESC";
//			return  i;
//		} else {
//			return -1;
//		}

	}

	public long addGroupChatToDB(ContentValues contentValues){
		return  database.insert(DBConstants.TABLE_GROUP_CHAT_TABLE, null, contentValues);
	}
	public long addCountriesToDB(ContentValues contentValues){
		return  database.insert(DBConstants.TABLE_COUNTRY_TABLE, null, contentValues);
	}
	public long addStatesToDB(ContentValues contentValues){
		return  database.insert(DBConstants.TABLE_STATES_TABLE, null, contentValues);
	}

	public Cursor getCountry(){
		return database.query(DBConstants.TABLE_COUNTRY_TABLE, null, null, null, null, null, null);
	}

	public Cursor getStates(String countryName){
		Cursor getCountryId = database.query(DBConstants.TABLE_COUNTRY_TABLE, null, DBConstants.COLUMN_NAME+" =? ", new String[]{countryName}, null, null, null);
		Cursor cursor = null;
		if (getCountryId != null && getCountryId.getCount()>0 && getCountryId.moveToFirst()){
			cursor=  database.query(DBConstants.TABLE_STATES_TABLE, null, DBConstants.COLUMN_COUNTRY_ID+" =? ",
					new String[]{getCountryId.getString(getCountryId.getColumnIndexOrThrow(DBConstants.KEY_ID))}, null, null, null);
		}
		return cursor;
	}


	public Cursor getChat(String fromId, String toId){
		String query = "SELECT * FROM "+DBConstants.TABLE_CHAT_TABLE + " WHERE ("+ DBConstants.COLUMN_SENDER_ID + " = " + fromId +
				" AND " + DBConstants.COLUMN_RECEIVER_ID + " = "+toId + " OR " + DBConstants.COLUMN_SENDER_ID + " = "+toId+
				" AND " + DBConstants.COLUMN_RECEIVER_ID + " = "+fromId+")";
		return database.rawQuery(query, null);
	}

	public Cursor getGroupChat(String groupId){
		String query = "SELECT * FROM "+DBConstants.TABLE_GROUP_CHAT_TABLE + " WHERE "+DBConstants.COLUMN_GROUP_ID+" = "+ groupId;
		return database.rawQuery(query, null);
	}

}
