package DB;

public class DBConstants {

	// Shared Preference Keys
	public static final String KEY_ID = "id";

	// Tables
	public static final String TABLE_CHAT_TABLE    = "chat_table";
	public static final String TABLE_GROUP_CHAT_TABLE    = "group_chat";
	public static final String TABLE_COUNTRY_TABLE    = "countries";
	public static final String TABLE_STATES_TABLE    = "states";

    // Columns for chat table
    public static final String COLUMN_SENDER_ID = "user_id";
    public static final String COLUMN_RECEIVER_ID= "friend_id";
    public static final String COLUMN_TIME = "timestamp";
    public static final String COLUMN_MESSAGE = "msg";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_BEEP = "beep";


    // Columns for group chat table
    public static final String COLUMN_GROUP_ID = "group_id";
    public static final String COLUMN_RECIVER_ID = "reciver_id";
    public static final String COLUMN_FRIEND_ID= "friend_id";


    public static final String COLUMN_NAME= "name";
    public static final String COLUMN_COUNTRY_ID= "country_id";


}