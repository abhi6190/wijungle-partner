package Model;

import java.io.Serializable;

/**
 * Created by VARIABLE on 11/25/2016.
 */
public class QuizModel implements Serializable{

    String id;
    String post_date;
    String category_id;
    String user_id;
    String topic_id;
    String quiz_type_id;
    String question;
    String answer;
    String feature_img;

    public QuizModel(String id, String post_date, String category_id, String user_id, String topic_id, String quiz_type_id, String question, String answer, String feature_img) {
        this.id = id;
        this.post_date = post_date;
        this.category_id = category_id;
        this.user_id = user_id;
        this.topic_id = topic_id;
        this.quiz_type_id = quiz_type_id;
        this.question = question;
        this.answer = answer;
        this.feature_img = feature_img;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }

    public String getQuiz_type_id() {
        return quiz_type_id;
    }

    public void setQuiz_type_id(String quiz_type_id) {
        this.quiz_type_id = quiz_type_id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getFeature_img() {
        return feature_img;
    }

    public void setFeature_img(String feature_img) {
        this.feature_img = feature_img;
    }
}
