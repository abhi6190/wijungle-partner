package Model;

/**
 * Created by VARIABLE on 3/7/2017.
 */
public class GroupModel {
    String grop_id;
    String owner_id;
    String group_title;
    String group_img;
    String date;

    public GroupModel(String grop_id, String owner_id, String group_title, String group_img, String date) {
        this.grop_id = grop_id;
        this.owner_id = owner_id;
        this.group_title = group_title;
        this.group_img = group_img;
        this.date = date;
    }

    public String getGrop_id() {
        return grop_id;
    }

    public void setGrop_id(String grop_id) {
        this.grop_id = grop_id;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public String getGroup_title() {
        return group_title;
    }

    public void setGroup_title(String group_title) {
        this.group_title = group_title;
    }

    public String getGroup_img() {
        return group_img;
    }

    public void setGroup_img(String group_img) {
        this.group_img = group_img;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
