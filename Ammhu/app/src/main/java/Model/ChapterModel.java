package Model;

/**
 * Created by VARIABLE on 11/7/2016.
 */
public class ChapterModel {
    String id;
    String class_id;
    String chapter_name;
    String name;
    String category_id;

    public ChapterModel(String id, String class_id, String chapter_name, String name, String category_id) {
        this.id = id;
        this.class_id = class_id;
        this.chapter_name = chapter_name;
        this.name = name;
        this.category_id = category_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getChapter_name() {
        return chapter_name;
    }

    public void setChapter_name(String chapter_name) {
        this.chapter_name = chapter_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }
}
