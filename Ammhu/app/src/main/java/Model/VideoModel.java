package Model;

import java.io.Serializable;

/**
 * Created by VARIABLE on 11/21/2016.
 */
public class VideoModel implements Serializable{

    String id;
    String user_id;
    String cat_id;
    String topic_id;
    String video_link;
    String description;
    String status;

    public VideoModel(String id, String user_id, String cat_id, String topic_id, String video_link, String description, String status) {
        this.id = id;
        this.user_id = user_id;
        this.cat_id = cat_id;
        this.topic_id = topic_id;
        this.video_link = video_link;
        this.description = description;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }

    public String getVideo_link() {
        return video_link;
    }

    public void setVideo_link(String video_link) {
        this.video_link = video_link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
