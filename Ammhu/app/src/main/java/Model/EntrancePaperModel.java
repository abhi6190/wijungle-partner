package Model;

/**
 * Created by VARIABLE on 11/25/2016.
 */
public class EntrancePaperModel {

    String id;
    String exam_id;
    String user_id;
    String year;
    String title;
    String path;
    String post_date;
    String downloads;
    String paper;

    public EntrancePaperModel(String id, String exam_id, String user_id, String year, String title, String path, String post_date, String downloads, String paper) {
        this.id = id;
        this.exam_id = exam_id;
        this.user_id = user_id;
        this.year = year;
        this.title = title;
        this.path = path;
        this.post_date = post_date;
        this.downloads = downloads;
        this.paper = paper;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExam_id() {
        return exam_id;
    }

    public void setExam_id(String exam_id) {
        this.exam_id = exam_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getDownloads() {
        return downloads;
    }

    public void setDownloads(String downloads) {
        this.downloads = downloads;
    }

    public String getPaper() {
        return paper;
    }

    public void setPaper(String paper) {
        this.paper = paper;
    }
}
