package Model;

/**
 * Created by VARIABLE on 11/26/2016.
 */
public class FFlowModel {
    String id;
    String user_id;
    String post_title;
    String post_des;
    String topic_id;
    String dd;
    String feature_img;
    String slug;
    String answer_request;
    String count;

    public FFlowModel(String id, String user_id, String post_title, String post_des, String topic_id, String dd, String feature_img, String slug, String answer_request, String count) {
        this.id = id;
        this.user_id = user_id;
        this.post_title = post_title;
        this.post_des = post_des;
        this.topic_id = topic_id;
        this.dd = dd;
        this.feature_img = feature_img;
        this.slug = slug;
        this.answer_request = answer_request;
        this.count = count;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_des() {
        return post_des;
    }

    public void setPost_des(String post_des) {
        this.post_des = post_des;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }

    public String getDd() {
        return dd;
    }

    public void setDd(String dd) {
        this.dd = dd;
    }

    public String getFeature_img() {
        return feature_img;
    }

    public void setFeature_img(String feature_img) {
        this.feature_img = feature_img;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getAnswer_request() {
        return answer_request;
    }

    public void setAnswer_request(String answer_request) {
        this.answer_request = answer_request;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}

