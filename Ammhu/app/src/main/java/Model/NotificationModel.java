package Model;

/**
 * Created by VARIABLE on 1/19/2017.
 */
public class NotificationModel {
    String user_id;
    String from;
    String name;
    String type;
    String message;
    String post_id;

    public NotificationModel(String user_id, String from, String name, String type, String message, String post_id) {
        this.user_id = user_id;
        this.from = from;
        this.name = name;
        this.type = type;
        this.message = message;
        this.post_id = post_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }
}
