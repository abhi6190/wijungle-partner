package Model;

/**
 * Created by VARIABLE on 6/9/2017.
 */
public class ExamPaperModel {
    String id;
    String class_id;
    String path;
    String year;
    String category_id;
    String title;
    String type;
    String post_date;
    String downloads;

    public ExamPaperModel(String id, String class_id, String path, String year, String category_id, String title, String type, String post_date, String downloads) {
        this.id = id;
        this.class_id = class_id;
        this.path = path;
        this.year = year;
        this.category_id = category_id;
        this.title = title;
        this.type = type;
        this.post_date = post_date;
        this.downloads = downloads;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getDownloads() {
        return downloads;
    }

    public void setDownloads(String downloads) {
        this.downloads = downloads;
    }
}
