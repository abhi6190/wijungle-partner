package Model;

/**
 * Created by VARIABLE on 1/19/2017.
 */
public class EBookSubCatModel {
    String id;
    String subcat;
    String pcat_id;

    public EBookSubCatModel(String id, String subcat, String pcat_id) {
        this.id = id;
        this.subcat = subcat;
        this.pcat_id = pcat_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubcat() {
        return subcat;
    }

    public void setSubcat(String subcat) {
        this.subcat = subcat;
    }

    public String getPcat_id() {
        return pcat_id;
    }

    public void setPcat_id(String pcat_id) {
        this.pcat_id = pcat_id;
    }
}
