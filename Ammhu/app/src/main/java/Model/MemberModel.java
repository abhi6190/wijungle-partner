package Model;

/**
 * Created by VARIABLE on 3/9/2017.
 */
public class MemberModel {
    String id;
    String group_id;
    String user_id;
    String group_owner;
    String date;
    String name;
    String image;

    public MemberModel(String id, String group_id, String user_id, String group_owner, String date, String name, String image) {
        this.id = id;
        this.group_id = group_id;
        this.user_id = user_id;
        this.group_owner = group_owner;
        this.date = date;
        this.name = name;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getGroup_owner() {
        return group_owner;
    }

    public void setGroup_owner(String group_owner) {
        this.group_owner = group_owner;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
