package Model;

/**
 * Created by VARIABLE on 11/6/2016.
 */
public class LinkModel {
    String id;
    String name;
    String url;
    String category_id;

    public LinkModel(String id, String name, String url, String category_id) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.category_id = category_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }
}
