package Model;

/**
 * Created by VARIABLE on 12/18/2016.
 */
public class FriendRequestModel {
    String id;
    String sender_id;
    String reciver_id;
    String name;

    public FriendRequestModel(String id, String sender_id, String reciver_id, String name) {
        this.id = id;
        this.sender_id = sender_id;
        this.reciver_id = reciver_id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getReciver_id() {
        return reciver_id;
    }

    public void setReciver_id(String reciver_id) {
        this.reciver_id = reciver_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
