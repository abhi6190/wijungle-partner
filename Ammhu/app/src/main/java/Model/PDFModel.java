package Model;

import java.io.Serializable;

/**
 * Created by VARIABLE on 11/7/2016.
 */
public class PDFModel implements Serializable{
    String id;
    String class_id;
    String user_id;
    String chapter_id;
    String title;
    String path;
    String post_date;
    String downloads;
    String category_id;

    public PDFModel(String id, String class_id, String user_id, String chapter_id, String title, String path, String post_date, String downloads, String category_id) {
        this.id = id;
        this.class_id = class_id;
        this.user_id = user_id;
        this.chapter_id = chapter_id;
        this.title = title;
        this.path = path;
        this.post_date = post_date;
        this.downloads = downloads;
        this.category_id = category_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getChapter_id() {
        return chapter_id;
    }

    public void setChapter_id(String chapter_id) {
        this.chapter_id = chapter_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getDownloads() {
        return downloads;
    }

    public void setDownloads(String downloads) {
        this.downloads = downloads;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }
}
