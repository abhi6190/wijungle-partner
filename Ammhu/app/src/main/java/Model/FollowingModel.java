package Model;

import java.io.Serializable;

/**
 * Created by VARIABLE on 1/7/2017.
 */
public class FollowingModel implements Serializable{

    String id;
    String user_id;
    String followers_id;
    String name;
    String image;

    public FollowingModel(String id, String user_id, String followers_id,String name,  String image) {
        this.id = id;
        this.user_id = user_id;
        this.followers_id = followers_id;
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFollowers_id() {
        return followers_id;
    }

    public void setFollowers_id(String followers_id) {
        this.followers_id = followers_id;
    }
}
