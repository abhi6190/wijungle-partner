package Model;

/**
 * Created by VARIABLE on 11/8/2016.
 */
public class GalleryCommentModel {
    String c_id;
    String user_id;
    String c_item_id;
    String c_text;
    String c_when;

    public GalleryCommentModel(String c_id, String user_id, String c_item_id, String c_text, String c_when) {
        this.c_id = c_id;
        this.user_id = user_id;
        this.c_item_id = c_item_id;
        this.c_text = c_text;
        this.c_when = c_when;
    }

    public String getC_id() {
        return c_id;
    }

    public void setC_id(String c_id) {
        this.c_id = c_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getC_item_id() {
        return c_item_id;
    }

    public void setC_item_id(String c_item_id) {
        this.c_item_id = c_item_id;
    }

    public String getC_text() {
        return c_text;
    }

    public void setC_text(String c_text) {
        this.c_text = c_text;
    }

    public String getC_when() {
        return c_when;
    }

    public void setC_when(String c_when) {
        this.c_when = c_when;
    }
}
