package Model;

import java.io.Serializable;

/**
 * Created by VARIABLE on 11/24/2016.
 */
public class UserModel implements Serializable{

    String id;
    String name;
    String sname;
    String email;
    String dd;
    String mm;
    String yyyy;
    String gender;
    String address;
    String phone;
    String image;
    String country;
    String state;
    String city;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDd() {
        return dd;
    }

    public void setDd(String dd) {
        this.dd = dd;
    }

    public String getMm() {
        return mm;
    }

    public void setMm(String mm) {
        this.mm = mm;
    }

    public String getYyyy() {
        return yyyy;
    }

    public void setYyyy(String yyyy) {
        this.yyyy = yyyy;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public UserModel(String id, String name, String sname, String email, String dd, String mm, String yyyy, String gender, String address, String phone, String image, String country, String state, String city) {
        this.id = id;
        this.name = name;
        this.sname = sname;
        this.email = email;
        this.dd = dd;
        this.mm = mm;
        this.yyyy = yyyy;
        this.gender = gender;
        this.address = address;
        this.phone = phone;
        this.image = image;
        this.country = country;
        this.state = state;
        this.city = city;
    }
}
