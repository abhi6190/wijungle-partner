package Model;

/**
 * Created by VARIABLE on 11/5/2016.
 */
public class TopicModel {
    String id;
    String topicName;
    int isCheck;

    public TopicModel(String id, String topicName, int isCheck) {
        this.id = id;
        this.topicName = topicName;
        this.isCheck = isCheck;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public int getIsCheck() {
        return isCheck;
    }

    public void setIsCheck(int isCheck) {
        this.isCheck = isCheck;
    }
}
