package Model;

/**
 * Created by VARIABLE on 2/2/2017.
 */
public class PostCommentModel {

    String post_id;
    String user_id;
    String content;
    String post_date;
    String feature_img;

    public PostCommentModel(String post_id, String user_id, String content, String post_date, String feature_img) {
        this.post_id = post_id;
        this.user_id = user_id;
        this.content = content;
        this.post_date = post_date;
        this.feature_img = feature_img;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getFeature_img() {
        return feature_img;
    }

    public void setFeature_img(String feature_img) {
        this.feature_img = feature_img;
    }
}
