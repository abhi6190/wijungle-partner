package Model;

/**
 * Created by VARIABLE on 3/9/2017.
 */
public class ParticipantModel {

    String id;
    String sender_id;
    String reciver_id;
    String name;
    String image;
    String isCheck;

    public ParticipantModel(String id, String sender_id, String reciver_id, String name, String image, String isCheck) {
        this.id = id;
        this.sender_id = sender_id;
        this.reciver_id = reciver_id;
        this.name = name;
        this.image = image;
        this.isCheck = isCheck;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getReciver_id() {
        return reciver_id;
    }

    public void setReciver_id(String reciver_id) {
        this.reciver_id = reciver_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIsCheck() {
        return isCheck;
    }

    public void setIsCheck(String isCheck) {
        this.isCheck = isCheck;
    }
}
