package Model;

/**
 * Created by VARIABLE on 11/6/2016.
 */
public class URCModel {
    String id;
    String name;

    public URCModel(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
