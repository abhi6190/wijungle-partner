package Model;

import java.io.Serializable;

/**
 * Created by VARIABLE on 11/7/2016.
 */
public class ImageModel implements Serializable{
    String id;
    String user_id;
    String cat_id;
    String topic_id;
    String title;
    String src;
    String comments_count;

    public ImageModel(String id, String user_id, String cat_id, String topic_id, String title, String src, String comments_count) {
        this.id = id;
        this.user_id = user_id;
        this.cat_id = cat_id;
        this.topic_id = topic_id;
        this.title = title;
        this.src = src;
        this.comments_count = comments_count;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getComments_count() {
        return comments_count;
    }

    public void setComments_count(String comments_count) {
        this.comments_count = comments_count;
    }
}
