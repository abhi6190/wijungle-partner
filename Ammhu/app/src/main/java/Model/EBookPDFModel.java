package Model;

/**
 * Created by VARIABLE on 1/21/2017.
 */
public class EBookPDFModel {
    String id;
    String name;
    String category_id;
    String sub_category_id;
    String pdf;
    String download;

    public EBookPDFModel(String id, String name, String category_id, String sub_category_id, String pdf, String download) {
        this.id = id;
        this.name = name;
        this.category_id = category_id;
        this.sub_category_id = sub_category_id;
        this.pdf = pdf;
        this.download = download;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getSub_category_id() {
        return sub_category_id;
    }

    public void setSub_category_id(String sub_category_id) {
        this.sub_category_id = sub_category_id;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getDownload() {
        return download;
    }

    public void setDownload(String download) {
        this.download = download;
    }
}
