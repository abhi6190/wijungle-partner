package Model;

/**
 * Created by VARIABLE on 11/7/2016.
 */
public class ClassModel {
    String id;
    String name;

    public ClassModel(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
