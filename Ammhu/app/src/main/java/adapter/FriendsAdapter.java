package adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.uthworks.ammhu.ChatScreen;
import com.uthworks.ammhu.FFlowDetailScreen;
import com.uthworks.ammhu.R;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import Model.FFlowModel;
import Model.FriendRequestModel;
import Model.FriendsModel;
import RestInteraction.RESTInteraction;
import Utils.ApiUrl;
import Utils.Utils;

/**
 * Created by VARIABLE on 12/18/2016.
 */
public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.ViewHolder>{


    Context context;
    ArrayList<String> titles;
    int flag;
    ArrayList<FriendsModel> postModels;
    RESTInteraction restInteraction;
    String desc = "";
    SharedPreferences sharedPreferences;
    public FriendsAdapter(Context context, ArrayList<FriendsModel> postModels) {
        // TODO Auto-generated constructor stub
        this.context = context;
        restInteraction = RESTInteraction.getInstance(context);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.postModels = postModels;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.friends_list_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        holder.dishname_yourmenu.setText(fullMenuModels.get(position).getDish_name());

//        holder.view1.setMinimumHeight(1);

        if (postModels.get(position).getImage().length()>0){
            String url = "https://ammhu.com/images/"+postModels.get(position).getImage();
            url=url.replaceAll(" ", "%20");
            Picasso.with(context)
                    .load(url)
                    .placeholder(R.mipmap.photo)
                    .into(holder.ivUsrPic);
        }

        holder.tvName.setText(Html.fromHtml(postModels.get(position).getName()));
        holder.tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isFriends) {
                    if (sharedPreferences.getString("userid", "").equals(postModels.get(position).getSender_id())) {
                        context.startActivity(new Intent(context, ChatScreen.class).putExtra("toid", postModels.get(position).getReciver_id()));
                    } else {
                        context.startActivity(new Intent(context, ChatScreen.class).putExtra("toid", postModels.get(position).getSender_id()));
                    }
                }
            }
        });

    }



    @Override
    public int getItemCount() {
        return postModels.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvName;
        public ImageView ivUsrPic;
        public Button btnCnfmReq;
        public View view1;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView)itemView.findViewById(R.id.tvName);
            ivUsrPic = (ImageView)itemView.findViewById(R.id.ivUsrPic);
            btnCnfmReq = (Button)itemView.findViewById(R.id.btnCnfmReq);
            btnCnfmReq.setVisibility(View.INVISIBLE);
//            ivRemoveItem = (ImageView)itemView.findViewById(R.id.ivRemoveItem);
//            ivAddItem = (ImageView)itemView.findViewById(R.id.ivAddItem);
//            view1 = (View)itemView.findViewById(R.id.view1);

        }
    }
}
