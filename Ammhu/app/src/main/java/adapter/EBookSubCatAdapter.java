package adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uthworks.ammhu.EBookPDFS;
import com.uthworks.ammhu.EBookSubCatScreen;
import com.uthworks.ammhu.R;

import java.util.ArrayList;

import Model.EBookSubCatModel;
import Model.EbookCategoryModel;
import RestInteraction.RESTInteraction;

/**
 * Created by VARIABLE on 1/19/2017.
 */
public class EBookSubCatAdapter extends RecyclerView.Adapter<EBookSubCatAdapter.ViewHolder>{


    Context context;
    ArrayList<String> titles;
    int flag;
    ArrayList<EBookSubCatModel> postModels;
    RESTInteraction restInteraction;
    String desc = "";
    SharedPreferences sharedPreferences;
    public EBookSubCatAdapter(Context context, ArrayList<EBookSubCatModel> postModels) {
        // TODO Auto-generated constructor stub
        this.context = context;
        restInteraction = RESTInteraction.getInstance(context);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.postModels = postModels;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ebook_list_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.tvEbookName.setText(Html.fromHtml(postModels.get(position).getSubcat()));
        holder.llMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, EBookPDFS.class).putExtra("catid", postModels.get(position).getPcat_id())
                .putExtra("subcatid", postModels.get(position).getId()));
            }
        });

    }



    @Override
    public int getItemCount() {
        return postModels.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvEbookName;
        public LinearLayout llMain;

        public ViewHolder(View itemView) {
            super(itemView);
            tvEbookName = (TextView)itemView.findViewById(R.id.tvEbookName);
            llMain = (LinearLayout)itemView.findViewById(R.id.llMain);
        }
    }
}
