package adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.uthworks.ammhu.ChatScreen;
import com.uthworks.ammhu.ProfileScreen;
import com.uthworks.ammhu.R;

import java.util.ArrayList;

import Model.FriendsModel;
import Model.UserModel;
import RestInteraction.RESTInteraction;
import Utils.Utils;

/**
 * Created by VARIABLE on 1/18/2017.
 */
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder>{


    Context context;
    ArrayList<String> titles;
    int flag;
    ArrayList<UserModel> postModels;
    RESTInteraction restInteraction;
    String desc = "";
    SharedPreferences sharedPreferences;
    public UserAdapter(Context context, ArrayList<UserModel> postModels) {
        // TODO Auto-generated constructor stub
        this.context = context;
        restInteraction = RESTInteraction.getInstance(context);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.postModels = postModels;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.friends_list_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.tvName.setText(Html.fromHtml(postModels.get(position).getName()+" "+postModels.get(position).getSname()));
//        holder.tvName.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (sharedPreferences.getString("userid", "").equals(postModels.get(position).getSender_id())) {
//                    context.startActivity(new Intent(context, ChatScreen.class).putExtra("toid", postModels.get(position).getReciver_id()));
//                }else{
//                    context.startActivity(new Intent(context, ChatScreen.class).putExtra("toid", postModels.get(position).getSender_id()));
//                }
//            }
//        });

        if (postModels.get(position).getImage().length()>0){
            String url = "https://ammhu.com/images/"+postModels.get(position).getImage();
            url=url.replaceAll(" ", "%20");
            Picasso.with(context)
                    .load(url)
                    .placeholder(R.mipmap.photo)
                    .into(holder.ivUsrPic);
        }
        holder.llMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.isFriends = false;
                context.startActivity(new Intent(context, ProfileScreen.class).putExtra("users", postModels.get(position)));
            }
        });

    }



    @Override
    public int getItemCount() {
        return postModels.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvName;
        public ImageView ivUsrPic;
        public Button btnCnfmReq;
        public View view1;
        public LinearLayout llMain;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView)itemView.findViewById(R.id.tvName);
            ivUsrPic = (ImageView)itemView.findViewById(R.id.ivUsrPic);
            btnCnfmReq = (Button)itemView.findViewById(R.id.btnCnfmReq);
            llMain = (LinearLayout)itemView.findViewById(R.id.llMain);
            btnCnfmReq.setVisibility(View.GONE);

        }
    }
}
