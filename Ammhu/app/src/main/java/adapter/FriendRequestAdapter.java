package adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.uthworks.ammhu.R;

import java.net.URLEncoder;
import java.util.ArrayList;

import Model.FriendRequestModel;
import RestInteraction.RESTInteraction;
import Utils.ApiUrl;
import Utils.Utils;

/**
 * Created by VARIABLE on 12/19/2016.
 */
public class FriendRequestAdapter extends RecyclerView.Adapter<FriendRequestAdapter.ViewHolder>{


    Context context;
    ArrayList<String> titles;
    int flag;
    ArrayList<FriendRequestModel> postModels;
    RESTInteraction restInteraction;
    String desc = "";
    SharedPreferences sharedPreferences;
    public FriendRequestAdapter(Context context, ArrayList<FriendRequestModel> postModels) {
        // TODO Auto-generated constructor stub
        this.context = context;
        restInteraction = RESTInteraction.getInstance(context);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.postModels = postModels;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.friends_list_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        holder.dishname_yourmenu.setText(fullMenuModels.get(position).getDish_name());

//        holder.view1.setMinimumHeight(1);


        holder.tvName.setText(Html.fromHtml(postModels.get(position).getName()));
        holder.tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                context.startActivity(new Intent(context, FFlowDetailScreen.class).putExtra("pos", position));
            }
        });
//        if (postModels.get(position).get)
//        String url = "https://ammhu.com/uploads/"+imageModels.getSrc();
//        url=url.replaceAll(" ", "%20");
//        Picasso.with(context)
//                .load(url)
//                .into(holder.ivUsrPic);

        holder.btnCnfmReq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                context.startActivity(new Intent(context, FFlowDetailScreen.class).putExtra("pos", position));
                new SendFriendRequest(postModels.get(position).getSender_id(), sharedPreferences.getString("userid", "")).execute();
            }
        });
    }

    public class SendFriendRequest extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String fromId; String toId;
        public SendFriendRequest(String fromId, String toId) {
            this.fromId =fromId;
            this.toId = toId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Please wait ...");
            progressDialog.setMessage("Confirming request ...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.common(ApiUrl.confirmFriendRequestUrl + "sender_id=" + URLEncoder.encode(fromId)
                    + "&reciver_id=" + URLEncoder.encode(toId) + "&status=" + "1");
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("success")) {
                notifyDataSetChanged();
                Toast.makeText(context, "Request Confirmed.", Toast.LENGTH_SHORT).show();
            }else if (result.equalsIgnoreCase("fail")) {
                Toast.makeText(context, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(context, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public int getItemCount() {
        return postModels.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvName;
        public ImageView ivUsrPic;
        public Button btnCnfmReq;
        public View view1;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView)itemView.findViewById(R.id.tvName);
            ivUsrPic = (ImageView)itemView.findViewById(R.id.ivUsrPic);
            btnCnfmReq = (Button)itemView.findViewById(R.id.btnCnfmReq);
//            ivRemoveItem = (ImageView)itemView.findViewById(R.id.ivRemoveItem);
//            ivAddItem = (ImageView)itemView.findViewById(R.id.ivAddItem);
//            view1 = (View)itemView.findViewById(R.id.view1);

        }
    }
}
