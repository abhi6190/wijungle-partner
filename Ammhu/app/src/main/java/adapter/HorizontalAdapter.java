package adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.squareup.picasso.Picasso;
import com.uthworks.ammhu.R;

import java.util.ArrayList;
import java.util.List;


public class HorizontalAdapter extends PagerAdapter {

    ArrayList<String> mArray;
    Context mContext;
    LayoutInflater mLayoutInflater;
    List<String> images;

    public HorizontalAdapter(Context context,ArrayList<String> array) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mArray = array;
    }
    
    public HorizontalAdapter(Context context,List<String> images) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.images = images;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((FrameLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.activity_autocomplete_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        FrameLayout flMain = (FrameLayout)itemView.findViewById(R.id.flMain);

//        imageView.setImageResource(mResources[position]);
//        Picasso.with(mContext).load(mArray.get(position)).into(imageView);
        if (images.get(position).length()>0) {
            String url = "https://ammhu.com/uploads/"+images.get(position);
            url=url.replaceAll(" ", "%20");
            Picasso.with(mContext)
               .load(url)
                .placeholder(R.mipmap.placeholder) // optional
                .error(R.mipmap.placeholder)         // optional
                .into(imageView);
		}else {
			imageView.setImageResource(R.mipmap.placeholder);
		}
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }
}