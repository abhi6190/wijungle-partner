package adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;
import com.uthworks.ammhu.AppController;
import com.uthworks.ammhu.ChatScreen;
import com.uthworks.ammhu.GroupChatScreen;
import com.uthworks.ammhu.GroupMemberScreen;
import com.uthworks.ammhu.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Model.FriendsModel;
import Model.GroupModel;
import Model.ParticipantModel;
import RestInteraction.RESTInteraction;
import Utils.Utils;
import Utils.ApiUrl;

/**
 * Created by VARIABLE on 3/7/2017.
 */
public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.ViewHolder>{


    Context context;
    ArrayList<String> titles;
    int flag;
    ArrayList<GroupModel> postModels;
    RESTInteraction restInteraction;
    String desc = "";
    SharedPreferences sharedPreferences;
    String tag_json_obj = "json_obj_req";
    ProgressDialog pDialog;
    String TAG = "MainActivity";
    String userId;
    public GroupAdapter(Context context, ArrayList<GroupModel> postModels) {
        // TODO Auto-generated constructor stub
        this.context = context;
        restInteraction = RESTInteraction.getInstance(context);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        userId = sharedPreferences.getString("userid", "");
        this.postModels = postModels;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.group_list_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        holder.dishname_yourmenu.setText(fullMenuModels.get(position).getDish_name());

//        holder.view1.setMinimumHeight(1);

        if (postModels.get(position).getGroup_img().length()>0){
            String url = "https://ammhu.com/group/"+postModels.get(position).getGroup_img();
            url=url.replaceAll(" ", "%20");
            Picasso.with(context)
                    .load(url)
                    .placeholder(R.mipmap.photo)
                    .into(holder.ivUsrPic);
        }

        if (!userId.equalsIgnoreCase(postModels.get(position).getOwner_id())){
            holder.btnDelete.setVisibility(View.GONE);
        }

        holder.tvName.setText(Html.fromHtml(postModels.get(position).getGroup_title()));
        holder.tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userId.equalsIgnoreCase(postModels.get(position).getOwner_id())){
                    context.startActivity(new Intent(context, GroupMemberScreen.class).putExtra("groupid", postModels.get(position).getGrop_id()));
                }
            }
        });

        holder.btnStartChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, GroupChatScreen.class).putExtra("groupid", postModels.get(position).getGrop_id()));
            }
        });

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteGroup(postModels.get(position).getGrop_id());
            }
        });

    }

    void deleteGroup(String groupId){
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setTitle("Please wait ...");
        pDialog.setMessage("Delete group ...");
        pDialog.show();
//       String url = "http://elogic.southindia.cloudapp.azure.com:8083/transtruck/admindashboard/tables/get/childadminshowList.php?pageno=1&limit=10";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ApiUrl.deletegroupUrl + "groupid=" + groupId, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObj) {
                        Log.d(TAG, jsonObj.toString());
                        pDialog.hide();
                        try {
                            if (jsonObj.getString("status").equals("success")) {
                                notifyDataSetChanged();
                                Toast.makeText(context, "Group deleted successfully", Toast.LENGTH_SHORT).show();
                            }else {
                                Utils.message = jsonObj.getString("message");
                                Toast.makeText(context, Utils.message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }



    @Override
    public int getItemCount() {
        return postModels.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvName;
        public ImageView ivUsrPic;
        public Button btnStartChat;
        public Button btnDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView)itemView.findViewById(R.id.tvGrpName);
            ivUsrPic = (ImageView)itemView.findViewById(R.id.ivGrpImage);
            btnStartChat = (Button)itemView.findViewById(R.id.btnStartChat);
            btnDelete = (Button)itemView.findViewById(R.id.btnDelete);
        }
    }
}
