package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uthworks.ammhu.FFlowDetailScreen;
import com.uthworks.ammhu.PostDetailScreen;
import com.uthworks.ammhu.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import Model.FFlowModel;
import Model.PostModel;

/**
 * Created by VARIABLE on 11/26/2016.
 */
public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder>{


    Context context;
    ArrayList<String> titles;
    int flag;
    ArrayList<FFlowModel> postModels;
    String desc = "";
    public PostAdapter(Context context, ArrayList<FFlowModel> postModels) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.postModels = postModels;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        holder.dishname_yourmenu.setText(fullMenuModels.get(position).getDish_name());

//        holder.view1.setMinimumHeight(1);

        DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        fromFormat.setLenient(false);
        DateFormat toFormat = new SimpleDateFormat("MMM dd, yyyy");
        toFormat.setLenient(false);
        Date date = null;
        try {
            date = fromFormat.parse(postModels.get(position).getDd());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.tvDate.setText(toFormat.format(date));
        holder.tvDesc.setText(Html.fromHtml(postModels.get(position).getPost_des()));
        holder.tvTitle.setText(Html.fromHtml(postModels.get(position).getPost_title()));
        holder.llMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, FFlowDetailScreen.class).putExtra("pos", position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return postModels.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvDesc;
        public TextView tvDate;
        public TextView tvTitle;
        public ImageView ivRemoveItem;
        public ImageView ivAddItem;
        public View view1;
        private LinearLayout llMain;

        public ViewHolder(View itemView) {
            super(itemView);
            tvDesc = (TextView)itemView.findViewById(R.id.tvDesc);
            tvDate = (TextView)itemView.findViewById(R.id.tvDate);
            tvTitle = (TextView)itemView.findViewById(R.id.tvTitle);
            llMain = (LinearLayout)itemView.findViewById(R.id.llMain);
//            ivRemoveItem = (ImageView)itemView.findViewById(R.id.ivRemoveItem);
//            ivAddItem = (ImageView)itemView.findViewById(R.id.ivAddItem);
//            view1 = (View)itemView.findViewById(R.id.view1);

        }
    }


}
