package adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.uthworks.ammhu.R;

import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import Model.UserModel;
import RestInteraction.RESTInteraction;
import Utils.ApiUrl;
import Utils.Utils;

/**
 * Created by VARIABLE on 11/24/2016.
 */
public class BookAutoCompleteAdapter extends BaseAdapter implements Filterable {

    private static final int MAX_RESULTS = 10;
    private Context mContext;
    private List<UserModel> resultList = new ArrayList<UserModel>();
    RESTInteraction restInteraction;
    public BookAutoCompleteAdapter(Context context) {
        mContext = context;
        restInteraction = RESTInteraction.getInstance(mContext);
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public UserModel getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.search_member_list_items, parent, false);
        }
        ((TextView) convertView.findViewById(R.id.text1)).setText(getItem(position).getName()+ " "+getItem(position).getSname());
        ((TextView) convertView.findViewById(R.id.text2)).setText("From : "+getItem(position).getCity()+ ", "+getItem(position).getState()
                + ", "+getItem(position).getCountry());
        ImageView imageView =(ImageView)convertView.findViewById(R.id.ivUsrPic);

        if (getItem(position).getImage().length()>0){
            String url = "https://ammhu.com/images/"+getItem(position).getImage();
            url=url.replaceAll(" ", "%20");
            Picasso.with(mContext)
                    .load(url)
                    .placeholder(R.mipmap.photo)
                    .into(imageView);
        }
        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    List<UserModel> books = findBooks(mContext, constraint.toString());

                    // Assign the data to the FilterResults
                    filterResults.values = books;
                    filterResults.count = books.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, Filter.FilterResults results) {
                if (results != null && results.count > 0) {
                    resultList = (List<UserModel>) results.values;
//                    notifyDataSetChanged();
                } else {
//                    notifyDataSetInvalidated();
                }
            }};
        return filter;
    }

    /**
     * Returns a search result for the given book title.
     */
    private List<UserModel> findBooks(Context context, String bookTitle) {
        // GoogleBooksProtocol is a wrapper for the Google Books API
        new GetAllUsers(URLEncoder.encode(bookTitle)).execute();
        return Utils.userModels;
    }

    public class GetAllUsers extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String Title;

        public GetAllUsers(String Title) {
            this.Title = Title;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            progressDialog = new ProgressDialog(CategoryScreen.this);
//            progressDialog.setCancelable(false);
//            progressDialog.setTitle("Please wait ...");
//            progressDialog.setMessage("Signin ...");
//            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return restInteraction.getAllUsers(ApiUrl.getAllUsersUrl+"name="+Title);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//            progressDialog.dismiss();
            if (result.equals("success")) {
                notifyDataSetChanged();

            }else if (result.equalsIgnoreCase("fail")) {
//                Toast.makeText(LaunchingActivity.this, Utils.message, Toast.LENGTH_SHORT).show();
            }
            else {
//                Toast.makeText(LaunchingActivity.this, "Network error..Please try again!!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

